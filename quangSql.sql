-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mylaravel_intern.contact
CREATE TABLE IF NOT EXISTS `contact` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_id_unique` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.contact: ~16 rows (approximately)
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` (`id`, `name`, `email`, `message`, `created_at`, `updated_at`) VALUES
	(1, 'DO VAN QUANG', 'quangdv@iit.com.vn', 'i am developer', '2020-08-20 12:05:39', '2020-08-20 12:05:39'),
	(2, 'Hải Băng', 'vituan98@gmail.com', 'cửa hàng rất tuyệt vời! làm ăn uy tín! rất chuyên nghệp!', '2020-08-21 05:21:32', '2020-08-21 05:21:32'),
	(3, 'Obama', 'obamaamerica.dev@gmail.com', 'Conragatution!', '2020-08-21 05:29:37', '2020-08-21 05:29:37'),
	(4, 'Phạm Nhật Vượng', 'nhatvuong67@gmail.com', 'tuyệt vời', '2020-08-21 05:34:35', '2020-08-21 05:34:35'),
	(5, 'sàgHTE', 'quhfdb@gmail.com', 'jkdshgukraghadkjnbkjhathoihd goihi gs gherio tẻo', '2020-08-21 05:39:27', '2020-08-21 05:39:27'),
	(6, 'i ét', 'iet@gmail.com', 'i am i ét!', '2020-08-23 16:48:06', '2020-08-23 16:48:06'),
	(7, 'DO VAN QUANG 1998', 'vituan98@gmail.com', 'sfabadgnhwmsndgnsf', '2020-08-23 17:18:31', '2020-08-23 17:18:31'),
	(8, 'sfDVSDVG', 'quangdv@iit.com.vn', 'ADvsbrshtar', '2020-08-23 17:19:57', '2020-08-23 17:19:57'),
	(9, 'Steve Jobs', 'vituan98@gmail.com', 'sfasbdgngmnrsh', '2020-08-23 17:22:36', '2020-08-23 17:22:36'),
	(10, 'sfsdgfr', 'dvq.dev@gmail.com', 'ầdg', '2020-08-23 17:27:45', '2020-08-23 17:27:45'),
	(11, 'adFAEF', 'quangdv@iit.com.vn', 'ứadbgrdbanst', '2020-08-23 17:29:55', '2020-08-23 17:29:55'),
	(12, 'DO VAN QUANG', 'dvq.dev@gmail.com', 'aDwRB', '2020-08-23 17:31:28', '2020-08-23 17:31:28'),
	(13, 'DO VAN QUANG', 'dvq.dev@gmail.com', 'cÁ', '2020-08-23 17:31:43', '2020-08-23 17:31:43'),
	(14, 'DO VAN QUANG', 'dvq.dev@gmail.com', 'sekgeSH;GIERHA', '2020-08-25 06:44:26', '2020-08-25 06:44:26');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_id_unique` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.customer: ~58 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`id`, `name`, `address`, `phone`, `email`, `total`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '83000', 1, '2020-08-17 06:59:19', '2020-08-20 14:29:43'),
	(4, 'Henry John', 'New York', '0245634789', 'henryjohn.dev@gmail.com', '40000000', 1, '2020-08-17 07:51:50', '2020-08-20 03:53:21'),
	(5, 'Hà Minh Quang', 'no.26 Bach Dang streets, Ha Noi City, VietNam', '0245775325774', 'quangdve@gmail.com', '40000000', 1, '2020-08-17 07:53:23', '2020-08-20 03:53:20'),
	(6, 'quang', 'nam định', '0969030421', 'dovanquang260719988@gmail.com', '606000', 1, '2020-08-20 02:49:44', '2020-08-20 03:53:19'),
	(7, 'quang', 'nam định', '0969030421', 'dovanquang260719988@gmail.com', '606000', 1, '2020-08-20 02:52:20', '2020-08-20 03:53:16'),
	(8, 'quang dev', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '625500', 1, '2020-08-20 07:12:17', '2020-08-20 17:51:06'),
	(9, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '0', 1, '2020-08-20 07:16:30', '2020-08-20 14:44:15'),
	(10, 'Mạnh', 'hoàn kiếm, hà nội', '09563231315', 'duymanh97@gmail.com', '570000', 0, '2020-08-20 16:01:18', '2020-08-20 16:01:18'),
	(11, 'Minh Hải', 'kiến xương, thái bình', '0355135497', 'minhhai89@gmail.com', '48000', 0, '2020-08-20 16:05:35', '2020-08-20 16:05:35'),
	(12, 'Thao Minh', 'Hải hậu, Nam định', '0956724242', 'minhthao95@gmail.com', '19500', 0, '2020-08-21 02:37:20', '2020-08-21 02:37:20'),
	(13, 'aaaaa', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '18000', 1, '2020-08-21 02:52:26', '2020-08-23 18:20:25'),
	(14, 'aaaaa', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '18000', 0, '2020-08-21 02:53:00', '2020-08-21 02:53:00'),
	(15, 'aaaaa', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '18000', 1, '2020-08-21 02:53:30', '2020-08-23 18:20:24'),
	(16, 'aaaaa', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '18000', 0, '2020-08-21 02:55:51', '2020-08-21 02:55:51'),
	(17, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '0', 0, '2020-08-21 03:02:03', '2020-08-21 03:02:03'),
	(18, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '0', 0, '2020-08-21 03:02:42', '2020-08-21 03:02:42'),
	(19, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '0', 0, '2020-08-21 03:03:09', '2020-08-21 03:03:09'),
	(20, 'Thảo', 'Hải hậu, Nam định', '113', 'minhthao.dev95@gmail.com', '118818000', 0, '2020-08-21 03:15:28', '2020-08-21 03:15:28'),
	(21, 'Thảo', 'Hải hậu, Nam định', '113', 'minhthao.dev95@gmail.com', '118818000', 0, '2020-08-21 03:16:39', '2020-08-21 03:16:39'),
	(22, 'Thảo', 'Hải hậu, Nam định', '113', 'minhthao.dev95@gmail.com', '118818000', 0, '2020-08-21 03:19:00', '2020-08-21 03:19:00'),
	(23, 'Thảo', 'Hải hậu, Nam định', '113', 'minhthao.dev95@gmail.com', '118818000', 0, '2020-08-21 03:19:35', '2020-08-21 03:19:35'),
	(24, 'Thảo', 'Hải hậu, Nam định', '113', 'minhthao.dev95@gmail.com', '118818000', 0, '2020-08-21 03:23:18', '2020-08-21 03:23:18'),
	(25, 'Anh Hải', 'sfjdshgjfdhg', '09797967967', 'haivm10@gmail.com', '34500', 1, '2020-08-21 07:51:43', '2020-08-21 07:57:42'),
	(26, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '19500', 0, '2020-08-23 16:45:18', '2020-08-23 16:45:18'),
	(27, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '119770000', 1, '2020-08-24 05:24:57', '2020-08-24 07:35:54'),
	(28, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '0', 1, '2020-08-24 05:26:02', '2020-08-24 07:36:02'),
	(29, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:26:36', '2020-08-24 05:26:36'),
	(30, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:31:34', '2020-08-24 05:31:34'),
	(31, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:32:15', '2020-08-24 05:32:15'),
	(32, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:32:21', '2020-08-24 05:32:21'),
	(33, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:33:17', '2020-08-24 05:33:17'),
	(34, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:33:51', '2020-08-24 05:33:51'),
	(35, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:34:04', '2020-08-24 05:34:04'),
	(36, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:34:54', '2020-08-24 05:34:54'),
	(37, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:35:07', '2020-08-24 05:35:07'),
	(38, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:35:59', '2020-08-24 05:35:59'),
	(39, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:39:06', '2020-08-24 05:39:06'),
	(40, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:43:53', '2020-08-24 05:43:53'),
	(41, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:47:56', '2020-08-24 05:47:56'),
	(42, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:48:11', '2020-08-24 05:48:11'),
	(43, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:48:33', '2020-08-24 05:48:33'),
	(44, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:49:54', '2020-08-24 05:49:54'),
	(45, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:50:42', '2020-08-24 05:50:42'),
	(46, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:50:45', '2020-08-24 05:50:45'),
	(47, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 05:51:23', '2020-08-24 05:51:23'),
	(48, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 06:01:46', '2020-08-24 06:01:46'),
	(49, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 06:04:26', '2020-08-24 06:04:26'),
	(50, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 06:04:51', '2020-08-24 06:04:51'),
	(51, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 06:05:19', '2020-08-24 06:05:19'),
	(52, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 06:05:43', '2020-08-24 06:05:43'),
	(53, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 06:06:18', '2020-08-24 06:06:18'),
	(54, 'sdsegad', 'dẻgaf', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 06:06:39', '2020-08-24 06:06:39'),
	(55, 'dfadgdf21', 'ưewfed', '0969030421', 'dvq.dev@gmail.com', '119750000', 0, '2020-08-24 06:08:31', '2020-08-24 06:08:31'),
	(56, 'dfadgdf21', 'ưewfed', '0969030421', 'dvq.dev@gmail.com', '119750000', 0, '2020-08-24 06:09:21', '2020-08-24 06:09:21'),
	(57, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 06:11:58', '2020-08-24 06:11:58'),
	(58, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '992000', 0, '2020-08-24 06:13:01', '2020-08-24 06:13:01'),
	(59, 'Long', 'hòa hình', '094543249353', 'longhb2000@gmail.com', '118815000', 0, '2020-08-24 06:17:25', '2020-08-24 06:17:25'),
	(60, 'Long', 'hòa hình', '094543249353', 'longhb2000@gmail.com', '118815000', 0, '2020-08-24 06:22:58', '2020-08-24 06:22:58'),
	(61, 'Long', 'hòa hình', '094543249353', 'longhb2000@gmail.com', '118815000', 0, '2020-08-24 06:23:16', '2020-08-24 06:23:16'),
	(62, 'Long', 'hòa bình', '0935464543743', 'dvq.dev@gmail.com', '120740000', 0, '2020-08-24 06:24:29', '2020-08-24 06:24:29'),
	(63, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '2088500', 0, '2020-08-24 06:27:01', '2020-08-24 06:27:01'),
	(64, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '125000', 0, '2020-08-24 06:32:33', '2020-08-24 06:32:33'),
	(65, 'DO VAN QUANG', 'THON TON THANH .', '0969030421', 'dvq.dev@gmail.com', '41880000', 0, '2020-08-24 06:43:47', '2020-08-24 06:43:47');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.images_product
CREATE TABLE IF NOT EXISTS `images_product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `images_product_product_id_index` (`product_id`),
  CONSTRAINT `images_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.images_product: ~23 rows (approximately)
/*!40000 ALTER TABLE `images_product` DISABLE KEYS */;
INSERT INTO `images_product` (`id`, `link`, `product_id`, `created_at`, `updated_at`) VALUES
	(1, 'images.jpg', 12, '2020-08-10 10:01:10', '2020-08-10 10:01:10'),
	(2, 'tải xuống.jpg', 12, '2020-08-10 10:01:10', '2020-08-10 10:01:10'),
	(3, 'pham-nhat-vuong-giau-tu-dau-giau-nhu-the-nao-bytuong-com.jpg', 1, '2020-08-10 14:10:17', '2020-08-10 14:10:17'),
	(4, 'r6.png', 1, '2020-08-10 14:10:17', '2020-08-10 14:10:17'),
	(5, 'tải xuống.jpg', 1, '2020-08-10 14:10:17', '2020-08-10 14:10:17'),
	(6, 'tanh5002-15611295041881237222821.jpg', 1, '2020-08-10 14:10:17', '2020-08-10 14:10:17'),
	(7, 'tanh5009-1561128721680305074690.jpg', 1, '2020-08-10 14:10:17', '2020-08-10 14:10:17'),
	(8, 'unnamed.jpg', 1, '2020-08-10 14:10:17', '2020-08-10 14:10:17'),
	(9, 'antd_vn-vai_thieu-0914.jpg', 2, '2020-08-10 14:14:10', '2020-08-10 14:14:10'),
	(10, 'e69b76decc01604b186ba264b503dfec.jpg', 2, '2020-08-10 14:14:10', '2020-08-10 14:14:10'),
	(11, 'img-3415_odbx.jpg', 2, '2020-08-10 14:14:10', '2020-08-10 14:14:10'),
	(12, 'tải xuống.jfif', 2, '2020-08-10 14:14:10', '2020-08-10 14:14:10'),
	(13, 'gia-may-cay-lua-kubota-2.jpg', 3, '2020-08-10 14:21:31', '2020-08-10 14:21:31'),
	(14, 'images.jfif', 3, '2020-08-10 14:21:31', '2020-08-10 14:21:31'),
	(15, 'images.jpg', 3, '2020-08-10 14:21:31', '2020-08-10 14:21:31'),
	(16, 'maxresdefault (1).jpg', 3, '2020-08-10 14:21:31', '2020-08-10 14:21:31'),
	(17, 'maxresdefault (2).jpg', 3, '2020-08-10 14:21:31', '2020-08-10 14:21:31'),
	(18, 'maxresdefault.jpg', 3, '2020-08-10 14:21:31', '2020-08-10 14:21:31'),
	(19, 'mua-ban-may-gat-lua-gia-bao-nhieu-gia-may-gat-lua-xep-day-va-cam-tay.jpg', 3, '2020-08-10 14:21:31', '2020-08-10 14:21:31'),
	(20, 'tải xuống.jpg', 3, '2020-08-10 14:21:31', '2020-08-10 14:21:31'),
	(21, 'images946477_sapoche.jpg', 13, '2020-08-19 16:47:52', '2020-08-19 16:47:52'),
	(22, 'sapoche_tien_giang.jpg', 13, '2020-08-19 16:47:52', '2020-08-19 16:47:52'),
	(23, 'sapoche.jpeg', 13, '2020-08-19 16:47:52', '2020-08-19 16:47:52'),
	(24, '11-xoai_tait.jpg', 18, '2020-08-25 06:50:51', '2020-08-25 06:50:51'),
	(25, 'bug.jpg', 18, '2020-08-25 06:50:51', '2020-08-25 06:50:51');
/*!40000 ALTER TABLE `images_product` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.info
CREATE TABLE IF NOT EXISTS `info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1:trụ sở chính, 2:trụ sở phụ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `info_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.info: ~2 rows (approximately)
/*!40000 ALTER TABLE `info` DISABLE KEYS */;
INSERT INTO `info` (`id`, `address`, `phone`, `email`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Số 1, Nguyễn Chí Thanh, Hà Nội', '0969030421', 'dvq.dev@gmail.com', 1, '2020-08-10 04:27:16', '2020-08-09 22:40:35'),
	(2, 'Số 26, Đường Hồ Tùng Mậu, Hà Nội', '0943434651', 'henryjohn.dev@gmail.com', 2, '2020-08-09 21:51:07', '2020-08-09 22:40:47');
/*!40000 ALTER TABLE `info` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.migrations: ~12 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2019_08_19_000000_create_failed_jobs_table', 1),
	(2, '2020_07_27_071107_create_users_table', 1),
	(3, '2020_07_29_081640_create_theloai_table', 1),
	(4, '2020_07_30_033008_create_slide_table', 1),
	(5, '2020_07_30_151240_create_product_table', 1),
	(6, '2020_07_30_153243_create_images_product_table', 1),
	(7, '2020_07_31_023205_create_type_post_table', 1),
	(8, '2020_07_31_023334_create_post_table', 1),
	(9, '2020_08_09_210219_create_info_table', 2),
	(10, '2020_08_09_212520_create_info_table', 3),
	(11, '2020_08_13_090409_create_customer_table', 4),
	(12, '2020_08_13_090423_create_order_table', 4),
	(13, '2020_08_20_052502_create_contact_table', 5),
	(14, '2020_08_21_040909_update_post_table', 6),
	(15, '2020_08_22_165916_create_opinion_table', 7);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.opinion
CREATE TABLE IF NOT EXISTS `opinion` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user.png',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cate` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.opinion: ~1 rows (approximately)
/*!40000 ALTER TABLE `opinion` DISABLE KEYS */;
INSERT INTO `opinion` (`id`, `image`, `name`, `content`, `cate`, `created_at`, `updated_at`) VALUES
	(1, 'user.png', 'Quang Dev', 'Nsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna alierqua Ut enierim ad minim veniam, quis nostrud exercitation ullamco laboris nissdi ut aliquip exeriommodo consequate Duis aute irure dolor in reprehenderit in voluptate.', 'Chuyên gia Nông nghiệp', '2020-08-23 00:22:51', '2020-08-24 13:12:38'),
	(4, 'IMAGE-opinion-1598123305IMAGE-AUTHOR-1596949117pham-nhat-vuong-giau-tu-dau-giau-nhu-the-nao-bytuong-com.jpg', 'Phạm Nhật Vượng', 'This error appear when this don\'t get the file object, at first try to find out the file object inside controller method then try execute this function', 'Nông lân ngư nghiệp', '2020-08-22 19:08:25', '2020-08-22 19:08:25'),
	(5, 'IMAGE-opinion-1598274794IMAGE-SLIDE-1596948955im-105325.jpg', 'Bill Gates', 'Chuyên gia lâm nghiệp', 'Chuyên gia lâm nghiệp', '2020-08-24 13:13:14', '2020-08-24 13:13:14');
/*!40000 ALTER TABLE `opinion` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.order
CREATE TABLE IF NOT EXISTS `order` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id_unique` (`id`),
  KEY `order_customer_id_foreign` (`customer_id`),
  CONSTRAINT `order_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.order: ~106 rows (approximately)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` (`id`, `name`, `price`, `quantity`, `image`, `customer_id`) VALUES
	(1, 'Vải thiều Hưng Yên', 19500, 2, NULL, 1),
	(2, 'Nhãn nồng Hưng Yên', 22000, 2, NULL, 1),
	(4, 'Máy Cấy lúa tự động', 20000000, 2, NULL, 4),
	(5, 'Máy Cấy lúa tự động', 20000000, 2, NULL, 5),
	(6, 'Xoài An Giang', 18000, 2, 'xoai.jpg', 6),
	(7, 'Tôm hùm Alaska', 285000, 2, 'IMAGE-PRODUCT1596419492tom-hum-1.jpg', 6),
	(8, 'Xoài An Giang', 18000, 2, 'xoai.jpg', 7),
	(9, 'Tôm hùm Alaska', 285000, 2, 'IMAGE-PRODUCT1596419492tom-hum-1.jpg', 7),
	(10, 'Xoài An Giang', 18000, 2, 'xoai.jpg', 8),
	(11, 'Tôm hùm Alaska', 285000, 2, 'IMAGE-PRODUCT1596419492tom-hum-1.jpg', 8),
	(12, 'Vải thiều Hưng Yên', 19500, 1, 'IMAGE-PRODUCT1596371208vai-thieu-tuoi-1.png', 8),
	(13, 'Tôm hùm Alaska', 285000, 2, 'IMAGE-PRODUCT1596419492tom-hum-1.jpg', 10),
	(14, 'Xoài An Giang', 18000, 2, 'xoai.jpg', 11),
	(15, 'Bưởi Năm Roi', 12000, 1, 'IMAGE-PRODUCT1596446463buoi-nam-roi.jpg', 11),
	(16, 'Vải thiều Hưng Yên', 19500, 1, 'IMAGE-PRODUCT1596371208vai-thieu-tuoi-1.png', 12),
	(17, 'Xoài An Giang', 18000, 1, 'xoai.jpg', 13),
	(18, 'Xoài An Giang', 18000, 1, 'xoai.jpg', 14),
	(19, 'Xoài An Giang', 18000, 1, 'xoai.jpg', 15),
	(20, 'Xoài An Giang', 18000, 1, 'xoai.jpg', 16),
	(21, 'Xoài An Giang', 18000, 1, 'xoai.jpg', 20),
	(22, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 20),
	(23, 'Xoài An Giang', 18000, 1, 'xoai.jpg', 21),
	(24, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 21),
	(25, 'Xoài An Giang', 18000, 1, 'xoai.jpg', 22),
	(26, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 22),
	(27, 'Xoài An Giang', 18000, 1, 'xoai.jpg', 23),
	(28, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 23),
	(29, 'Xoài An Giang', 18000, 1, 'xoai.jpg', 24),
	(30, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 24),
	(31, 'Vải thiều Hưng Yên', 19500, 1, 'IMAGE-PRODUCT1596371208vai-thieu-tuoi-1.png', 25),
	(32, 'Hồng cát', 15000, 1, 'sapo.jpg', 25),
	(33, 'Vải thiều Hưng Yên', 19500, 1, 'IMAGE-PRODUCT1596371208vai-thieu-tuoi-1.png', 26),
	(34, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 27),
	(35, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 27),
	(36, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 29),
	(37, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 29),
	(38, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 30),
	(39, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 30),
	(40, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 31),
	(41, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 31),
	(42, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 32),
	(43, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 32),
	(44, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 33),
	(45, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 33),
	(46, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 34),
	(47, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 34),
	(48, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 35),
	(49, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 35),
	(50, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 36),
	(51, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 36),
	(52, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 37),
	(53, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 37),
	(54, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 38),
	(55, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 38),
	(56, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 39),
	(57, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 39),
	(58, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 40),
	(59, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 40),
	(60, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 41),
	(61, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 41),
	(62, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 42),
	(63, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 42),
	(64, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 43),
	(65, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 43),
	(66, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 44),
	(67, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 44),
	(68, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 45),
	(69, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 45),
	(70, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 46),
	(71, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 46),
	(72, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 47),
	(73, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 47),
	(74, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 48),
	(75, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 48),
	(76, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 49),
	(77, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 49),
	(78, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 50),
	(79, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 50),
	(80, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 51),
	(81, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 51),
	(82, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 52),
	(83, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 52),
	(84, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 53),
	(85, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 53),
	(86, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 54),
	(87, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 54),
	(88, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 55),
	(89, 'demo1', 950000, 1, 'IMAGE-PRODUCT1597004331hq720.png', 55),
	(90, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 56),
	(91, 'demo1', 950000, 1, 'IMAGE-PRODUCT1597004331hq720.png', 56),
	(92, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 57),
	(93, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 57),
	(94, 'Cá Ngừ', 970000, 1, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 58),
	(95, 'Nhãn nồng Hưng Yên', 22000, 1, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 58),
	(96, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 59),
	(97, 'Hồng cát', 15000, 1, 'sapo.jpg', 59),
	(98, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 60),
	(99, 'Hồng cát', 15000, 1, 'sapo.jpg', 60),
	(100, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 61),
	(101, 'Hồng cát', 15000, 1, 'sapo.jpg', 61),
	(102, 'Máy gặt đập liên hợp mini', 118800000, 1, 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 62),
	(103, 'Cá Ngừ', 970000, 2, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 62),
	(104, 'Cá Ngừ', 970000, 2, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 63),
	(105, 'Bồ câu', 49500, 3, 'IMAGE-PRODUCT1596447133bocau.jpg', 63),
	(106, 'Chuối Ngự', 15000, 1, 'IMAGE-PRODUCT1596446678chuoi.jpg', 64),
	(107, 'Nhãn nồng Hưng Yên', 22000, 5, 'IMAGE-PRODUCT1596468986nhanNong.jpg', 64),
	(108, 'Cá Ngừ', 970000, 4, 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 65),
	(109, 'Máy Cấy lúa tự động', 19000000, 2, 'gia-may-cay-lua-kubota-2.jpg', 65);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.post
CREATE TABLE IF NOT EXISTS `post` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contentHot` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'thời gian tạo bài viết',
  `tag` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci COMMENT 'nội dung bài viết dạng html',
  `view-count` int(11) NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `typePost_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `post_id_index` (`id`),
  KEY `post_tag_index` (`tag`),
  KEY `post_user_id_index` (`user_id`),
  KEY `post_typepost_id_index` (`typePost_id`),
  CONSTRAINT `post_typepost_id_foreign` FOREIGN KEY (`typePost_id`) REFERENCES `type_post` (`id`) ON DELETE CASCADE,
  CONSTRAINT `post_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.post: ~5 rows (approximately)
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` (`id`, `title`, `contentHot`, `slug`, `image`, `postDate`, `tag`, `content`, `view-count`, `user_id`, `typePost_id`, `created_at`, `updated_at`) VALUES
	(1, 'France Bans Metam-Sodium Sprays 1', 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloreme laudantiuem totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemreeo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.', 'france-bans-metam-sodium-sprays-1-1', 'IMAGE-SLIDE-1596815742blog.jpg', '2020-08-03 00:00:00', 'France Bans Metam-Sodium Sprays, quangdev, fance', '<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sed ut perspiciatis unde.</p>\r\n<p class="mt-3">omnis iste natus error sit voluptatem accusantium doloreme laudantiuem totam rem aperiam, eaque ipsa quereae ab illreyio inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemreeo enim ipsam voluptatem quia voluptartrs sit aspernatur aut odit aut fugit architecto beatae vitae dicta sunt explicabo. Nemfhgrw Nemreeo enim ipsam voluptat Nemreeo</p>\r\n<div class="single_content_wrapper"><img src="https://via.placeholder.com/240x199" alt="image" />\r\n<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et doloertfire magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco labories nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatatderesf igd noern proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sedrersfsare perspiciatis unde omnis iste natus error sit voluptatem accusantium doloreer sfeme ut laudantiuem totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et qsriuasi architecto beatae vitae dicta sunt explicabo. Nemreeo enim ipsam voluptatreem qeuia voluptas sit aspernatur aut odit aut fugit architecto beatae vitae dicta sunt explicaebo.</p>\r\n</div>\r\n<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p class="mt-3">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloreme laudantiuem totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemreeo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit architecto beatae vitae dicta sunt explicabo. Nemfhgrw</p>\r\n<div class="blog_message">\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore etdolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo consequat. Duis aute irure dolor in reprehenderit in</p>\r\n<img src="images/message_quotes.png" alt="image" /></div>\r\n<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sed ut perspiciatis unde.</p>', 0, 1, 2, '2020-08-01 03:47:49', '2020-08-21 06:49:43'),
	(3, 'France Bans Metam-Sodium Sprays2', 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloreme laudantiuem totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemreeo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.', 'france-bans-metam-sodium-sprays2-3', 'IMAGE-SLIDE-1596816436China.jpg', '2020-07-26 00:00:00', 'France Bans Metam-Sodium Sprays', '<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloreme laudantiuem totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemreeo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>', 0, 1, 1, '2020-08-03 03:47:33', '2020-08-08 17:15:52'),
	(4, 'France Bans Metam-Sodium Sprays 3', 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloreme laudantiuem totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemreeo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.', 'france-bans-metam-sodium-sprays-3-4', 'IMAGE-SLIDE-1596817530p3.jpeg', '2020-08-07 00:00:00', 'dfds, fgfrd', '<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloreme laudantiuem totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemreeo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>', 0, 1, 1, '2020-08-07 16:25:30', '2020-08-08 17:16:11'),
	(5, 'Bof italy a dcds', 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloreme laudantiuem totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemreeo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.', 'bof-italy-a-dcds-5', 'IMAGE-SLIDE-1596817599unnamed.jpg', '2020-08-07 00:00:00', 'assss', '<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim venia quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit iluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui ofa deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloreme laudantiuem totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemreeo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>', 0, 1, 1, '2020-08-07 16:26:39', '2020-08-08 17:16:20'),
	(7, 'Bỗng dưng bị cấm xuất khẩu xoài sang Trung Quốc', 'Tổng cục Hải quan Trung Quốc (TQ) vừa thông báo trong tháng 6 có 220 lô xoài của Việt Nam vi phạm quy định về kiểm dịch thực vật với nhiều nguyên nhân khác nhau...', 'bong-dung-bi-cam-xuat-khau-xoai-sang-trung-quoc-7', 'IMAGE-SLIDE-159829263311-xoai_tait.jpg', '2020-08-24 00:00:00', 'xoài, trung quốc, hải quan', '<p><a href="https://plo.vn/tags/VOG7lW5nIGPhu6VjIEjhuqNpIHF1YW4gVHJ1bmcgUXXhu5Fj/tong-cuc-hai-quan-trung-quoc.html">Tổng cục Hải quan Trung Quốc</a>&nbsp;(TQ) vừa th&ocirc;ng b&aacute;o trong th&aacute;ng 6 c&oacute; 220 l&ocirc;&nbsp;<a href="https://plo.vn/tags/eG_DoGkgY-G7p2EgVmnhu4d0IE5hbQ==/xoai-cua-viet-nam.html">xo&agrave;i của Việt Nam</a>&nbsp;<a href="https://plo.vn/tags/dmkgcGjhuqFtIHF1eSDEkeG7i25o/vi-pham-quy-dinh.html">vi phạm quy định</a>&nbsp;về kiểm dịch thực vật với nhiều nguy&ecirc;n nh&acirc;n kh&aacute;c nhau. Do đ&oacute;, ph&iacute;a TQ y&ecirc;u cầu tạm ngưng xuất khẩu xo&agrave;i từ c&aacute;c v&ugrave;ng trồng v&agrave; cơ sở đ&oacute;ng g&oacute;i c&oacute; li&ecirc;n quan để phối hợp tiến h&agrave;nh điều tra nguy&ecirc;n nh&acirc;n, đề xuất biện ph&aacute;p khắc phục v&agrave; n&acirc;ng cao c&ocirc;ng t&aacute;c quản l&yacute;.</p>\r\n<p>Đ&aacute;ng lưu &yacute;, trong c&aacute;c v&ugrave;ng trồng v&agrave; cơ sở đ&oacute;ng g&oacute;i n&agrave;y c&oacute; 2/82 v&ugrave;ng trồng xo&agrave;i v&agrave; 1/12 cơ sở đ&oacute;ng g&oacute;i của Đồng Th&aacute;p trong danh s&aacute;ch vi phạm. Thế nhưng c&aacute;c cơ sở n&agrave;y cho biết họ đ&atilde; bị mạo danh m&atilde; số v&ugrave;ng trồng v&agrave; m&atilde; số đ&oacute;ng g&oacute;i.</p>\r\n<p><strong>Bức x&uacute;c v&igrave; m&atilde; số ri&ecirc;ng bị &ldquo;x&agrave;i ch&ugrave;a&rdquo;</strong></p>\r\n<p>Trao đổi với&nbsp;<em>Ph&aacute;p Luật TP.HCM</em>, b&agrave; Đinh Thị Mỹ Nhung, Ph&oacute; Gi&aacute;m đốc C&ocirc;ng ty TNHH Kim Nhung Đồng Th&aacute;p, cho biết: C&ocirc;ng ty được cấp m&atilde; số nh&agrave; đ&oacute;ng g&oacute;i xuất khẩu sang TQ từ năm 2018. Tuy nhi&ecirc;n, đến năm 2019, b&agrave; đ&atilde; ph&aacute;t hiện m&atilde; số đ&oacute;ng g&oacute;i của c&ocirc;ng ty m&igrave;nh được sử dụng rất nhiều lần cho h&agrave;ng loạt l&ocirc; xo&agrave;i xuất khẩu của những doanh nghiệp kh&aacute;c nhưng kh&ocirc;ng c&oacute; c&aacute;ch n&agrave;o ngăn chặn được.</p>\r\n<div id="div-gpt-ad-1578026033787-0_container">\r\n<div id="div-gpt-ad-1578026033787-0" data-google-query-id="CNLPoMa3tOsCFUV-vQodQ-oJnA">\r\n<div id="google_ads_iframe_/21622890900/VN_plo.vn_pc_article_mid1_300x250//336x280_0__container__"><iframe id="google_ads_iframe_/21622890900/VN_plo.vn_pc_article_mid1_300x250//336x280_0" title="3rd party ad content" name="google_ads_iframe_/21622890900/VN_plo.vn_pc_article_mid1_300x250//336x280_0" width="336" height="280" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" data-google-container-id="1" data-load-complete="true" data-mce-fragment="1"></iframe></div>\r\n</div>\r\n</div>\r\n<p>&ldquo;Khi t&ocirc;i hỏi tại sao lại lấy m&atilde; số n&agrave;y để sử dụng th&igrave; họ trả lời l&agrave; do ph&iacute;a thương nh&acirc;n TQ y&ecirc;u cầu. Ph&iacute;a TQ đưa m&atilde; số n&agrave;o th&igrave; họ lấy m&atilde; số đ&oacute; đ&oacute;ng l&ecirc;n, chứ kh&ocirc;ng biết m&atilde; số đ&oacute; l&agrave; của ai v&agrave; ở đ&acirc;u. T&ocirc;i n&oacute;i đ&acirc;y l&agrave; m&atilde; số của c&ocirc;ng ty m&igrave;nh, y&ecirc;u cầu c&aacute;c c&ocirc;ng ty kh&aacute;c kh&ocirc;ng được sử dụng nhưng họ kh&ocirc;ng quan t&acirc;m&rdquo; - b&agrave; Nhung cho biết.</p>\r\n<p>Qu&aacute; bức x&uacute;c, trong c&aacute;c hội nghị của địa phương, b&agrave; Nhung phản &aacute;nh t&igrave;nh trạng của m&igrave;nh để mong được giải quyết. Thế nhưng đến nay m&atilde; số đ&oacute;ng g&oacute;i của C&ocirc;ng ty Kim Nhung lại bị loại ra khỏi danh s&aacute;ch được c&ocirc;ng nhận, kh&ocirc;ng được xuất khẩu xo&agrave;i qua TQ nữa.</p>\r\n<p>B&agrave; Nhung cho rằng qua sự việc tr&ecirc;n cho thấy c&oacute; nhiều lỗ hổng. Bởi th&ocirc;ng thường, với c&aacute;c l&ocirc; h&agrave;ng xuất khẩu qua c&aacute;c thị trường như Mỹ, ch&acirc;u &Acirc;u... th&igrave; khi đăng k&yacute; kiểm dịch, cơ quan chức năng x&aacute;c nhận m&atilde; v&ugrave;ng trồng v&agrave; m&atilde; nh&agrave; đ&oacute;ng g&oacute;i đ&oacute; c&oacute; đ&uacute;ng hay kh&ocirc;ng. Nếu đ&uacute;ng m&atilde; số v&ugrave;ng trồng, m&atilde; số của nh&agrave; đ&oacute;ng g&oacute;i đ&oacute; th&igrave; l&ocirc; h&agrave;ng mới được xuất khẩu, c&ograve;n kh&ocirc;ng sẽ bị trả lại. C&ograve;n thị trường TQ th&igrave; kh&ocirc;ng x&aacute;c nhận chủ của m&atilde; số v&ugrave;ng trồng, nh&agrave; đ&oacute;ng g&oacute;i m&agrave; chỉ đăng k&yacute; v&agrave; đưa h&agrave;ng sang n&ecirc;n ai cũng đi được.</p>\r\n<p>Nhiều đơn vị kh&aacute;c cũng rơi v&agrave;o t&igrave;nh cảnh tương tự như C&ocirc;ng ty Kim Nhung. Điển h&igrave;nh l&agrave; Hợp t&aacute;c x&atilde; Mỹ Xương cũng bị mạo danh m&atilde; số v&ugrave;ng trồng v&agrave; bị TQ tạm dừng xuất khẩu.</p>\r\n<div id="sas_44269_container">\r\n<div id="sas_44269"></div>\r\n</div>\r\n<p>&Ocirc;ng V&otilde; Việt Hưng, Gi&aacute;m đốc hợp t&aacute;c x&atilde; n&agrave;y, cho biết: &ldquo;Nhiều c&ocirc;ng ty tự in m&atilde; code v&ugrave;ng trồng của hợp t&aacute;c x&atilde; để xuất khẩu sang TQ. Việc bị l&agrave;m giả xuất xứ khiến ch&uacute;ng t&ocirc;i mang tiếng. Ch&uacute;ng t&ocirc;i cũng chẳng biết g&igrave;, đến khi Chi cục Bảo vệ thực vật của tỉnh th&ocirc;ng b&aacute;o mới nắm được th&ocirc;ng tin&rdquo;.</p>\r\n<p class="item-photo" align="center"><a class="photo" href="https://image.plo.vn/w800/Uploaded/2020/cqjwqcdwp/2020_08_20/11-xoai_tait.jpg" data-desc="Trung Quốc y&ecirc;u cầu phải cấp m&atilde; số cho c&aacute;c v&ugrave;ng trồng v&agrave; cơ sở đ&oacute;ng g&oacute;i quả tươi xuất khẩu ch&iacute;nh ngạch sang thị trường n&agrave;y. Ảnh: ANH Đ&Agrave;O" data-index="1"><img class="cms-photo" src="https://image.plo.vn/w800/Uploaded/2020/cqjwqcdwp/2020_08_20/11-xoai_tait.jpg" alt="Bỗng dưng bị cấm xuất khẩu xo&agrave;i sang Trung Quốc - ảnh 1" data-photo-original-src="https://image.plo.vn/w800/Uploaded/2020/cqjwqcdwp/2020_08_20/11-xoai_tait.jpg" /></a><br /><em class="image_caption">Trung Quốc y&ecirc;u cầu phải cấp m&atilde; số cho c&aacute;c v&ugrave;ng trồng v&agrave; cơ sở đ&oacute;ng g&oacute;i quả tươi xuất khẩu ch&iacute;nh ngạch sang thị trường n&agrave;y. Ảnh: ANH Đ&Agrave;O</em></p>\r\n<p><strong>Kh&ocirc;ng cẩn thận sẽ mất thị trường</strong></p>\r\n<p>&Ocirc;ng Đặng Ph&uacute;c Nguy&ecirc;n, Tổng thư k&yacute; Hiệp hội Rau quả Việt Nam, lo ngại nếu t&igrave;nh trạng mạo danh diễn ra nhiều th&igrave; ph&iacute;a TQ c&oacute; thể sẽ cấm nhập, dẫn đến mất thị trường. Như vậy, sau đ&oacute; cơ quan chức năng phải đi l&agrave;m việc lại, mất nhiều thời gian, c&ocirc;ng sức.</p>\r\n<div class="box-script">&nbsp;</div>\r\n<p>&Ocirc;ng Nguy&ecirc;n cũng cho rằng c&oacute; thể do hiện nay số diện t&iacute;ch tr&aacute;i c&acirc;y được cấp m&atilde; số v&ugrave;ng trồng &iacute;t hơn số lượng trồng thực tế, dẫn đến t&igrave;nh trạng mạo danh m&atilde; số v&ugrave;ng trồng. Do vậy, phải quản l&yacute; m&atilde; số nh&agrave; đ&oacute;ng g&oacute;i thật chặt v&igrave; c&aacute;c v&ugrave;ng trồng kh&ocirc;ng trực tiếp xuất khẩu h&agrave;ng sang TQ m&agrave; phải th&ocirc;ng qua cơ sở đ&oacute;ng g&oacute;i.</p>\r\n<div>\r\n<table cellspacing="5" cellpadding="0" align="center">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p>Để hạn chế t&igrave;nh trạng mạo danh m&atilde; số v&ugrave;ng trồng v&agrave; m&atilde; số đ&oacute;ng g&oacute;i, &ocirc;ng Trần Thanh T&acirc;m, Chi cục trưởng Chi cục Trồng trọt v&agrave; Bảo vệ thực vật tỉnh Đồng Th&aacute;p, đề xuất cần c&oacute; sự kiểm so&aacute;t chặt chẽ h&agrave;ng n&ocirc;ng sản xuất khẩu khi tiến h&agrave;nh th&ocirc;ng quan. Cụ thể, phải c&oacute; sự x&aacute;c nhận của chủ sở hữu m&atilde; số v&ugrave;ng trồng, m&atilde; số nh&agrave; đ&oacute;ng g&oacute;i đ&oacute; hoặc hợp đồng thu mua.&nbsp;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n<p>&ldquo;T&ocirc;i thấy c&aacute;c thị trường kh&oacute; t&iacute;nh như Mỹ, ch&acirc;u &Acirc;u... đều quản l&yacute; th&ocirc;ng qua cơ sở đ&oacute;ng g&oacute;i. V&iacute; dụ c&ocirc;ng ty A xuất h&agrave;ng qua Mỹ, nếu chất lượng h&agrave;ng xấu th&igrave; sau đ&oacute; Mỹ sẽ kh&ocirc;ng cho ph&eacute;p c&ocirc;ng ty n&agrave;y xuất khẩu v&agrave;o nước họ nữa hoặc bị kiểm tra rất kỹ&rdquo; - &ocirc;ng Nguy&ecirc;n n&ecirc;u v&iacute; dụ.</p>\r\n<p>&Ocirc;ng Nguyễn Quang Hiếu, Trưởng ph&ograve;ng Hợp t&aacute;c quốc tế v&agrave; Truyền th&ocirc;ng, Cục Bảo vệ thực vật (Bộ NN&amp;PTNT), cho biết: Cục đ&atilde; nhận được phản &aacute;nh của tỉnh Đồng Th&aacute;p về t&igrave;nh trạng c&aacute;c doanh nghiệp sử dụng kh&ocirc;ng đ&uacute;ng m&atilde; số, &ldquo;mượn&rdquo; m&atilde; số của nhau để xuất khẩu. Điều n&agrave;y kh&ocirc;ng chỉ g&acirc;y ảnh hưởng đến uy t&iacute;n của xo&agrave;i Việt Nam xuất khẩu, m&agrave; c&ograve;n trực tiếp ảnh hưởng đến c&aacute;c đơn vị chủ sở hữu m&atilde; số ngay trong vụ xuất khẩu tới đ&acirc;y.</p>\r\n<p>&ldquo;Để tr&aacute;nh lặp lại c&aacute;c vi phạm tương tự, Cục Bảo vệ thực vật sẽ l&agrave;m việc với c&aacute;c đơn vị li&ecirc;n quan v&agrave; tỉnh Đồng Th&aacute;p n&oacute;i ri&ecirc;ng để b&agrave;n bạc cụ thể hơn về việc ph&acirc;n c&ocirc;ng tr&aacute;ch nhiệm; thống nhất về c&aacute;ch thức triển khai thực hiện v&agrave; tăng cường c&ocirc;ng t&aacute;c quản l&yacute; v&ugrave;ng trồng kh&ocirc;ng chỉ với sản phẩm xo&agrave;i&rdquo; - &ocirc;ng Hiếu cho hay.</p>\r\n<div>\r\n<table cellspacing="5" cellpadding="0" align="center">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p><strong>Phải c&oacute; hướng dẫn cụ thể, chế t&agrave;i nặng</strong></p>\r\n<p>&Ocirc;ng Nguyễn Quang Hiếu, Trưởng ph&ograve;ng Hợp t&aacute;c quốc tế v&agrave; Truyền th&ocirc;ng, Cục Bảo vệ thực vật, khẳng định: Việc cấp m&atilde; số cho c&aacute;c v&ugrave;ng trồng v&agrave; cơ sở đ&oacute;ng g&oacute;i xuất ph&aacute;t từ y&ecirc;u cầu của ph&iacute;a Tổng cục Hải quan TQ. Quy định n&agrave;y được&nbsp;<a href="https://plo.vn/tags/dGjDtG5nIGLDoW8=/thong-bao.html">th&ocirc;ng b&aacute;o</a>&nbsp;từ năm 2018 v&agrave; bắt đầu &aacute;p dụng từ năm 2019. Theo đ&oacute;, trước khi c&aacute;c l&ocirc; h&agrave;ng quả tươi xuất khẩu ch&iacute;nh ngạch sang thị trường TQ phải ghi m&atilde; số v&ugrave;ng trồng v&agrave; cơ sở đ&oacute;ng g&oacute;i l&ecirc;n giấy chứng nhận&nbsp;<a href="https://plo.vn/tags/a2nhu4NtIGThu4tjaCB0aOG7sWMgduG6rXQ=/kiem-dich-thuc-vat.html">kiểm dịch thực vật</a>&nbsp;xuất khẩu.</p>\r\n<p>&ldquo;Lượng m&atilde; số lớn trải d&agrave;i ở tất cả địa phương n&ecirc;n trong năm 2019, Cục Bảo vệ thực vật đ&atilde; phối hợp với nhiều địa phương để tiến h&agrave;nh tập huấn, phổ biến th&ocirc;ng tin li&ecirc;n quan đến nội dung quy tr&igrave;nh cấp m&atilde; số cho thị trường TQ. Trong đ&oacute; đặc biệt nhấn mạnh vai tr&ograve; địa phương trong việc x&aacute;c minh th&ocirc;ng tin c&aacute;c v&ugrave;ng trồng c&oacute; y&ecirc;u cầu cấp m&atilde; v&agrave; thực hiện v&ugrave;ng gi&aacute;m s&aacute;t c&aacute;c m&atilde; số v&ugrave;ng trồng đ&atilde; được ph&iacute;a TQ chấp nhận&rdquo; - &ocirc;ng Hiếu cho hay.</p>\r\n<p>Tuy vậy, theo một số c&ocirc;ng ty xuất khẩu tr&aacute;i c&acirc;y, việc cấp m&atilde; số v&ugrave;ng trồng v&agrave; m&atilde; số nh&agrave; đ&oacute;ng g&oacute;i để xuất h&agrave;ng sang TQ kh&aacute; đơn giản. Đặc biệt, m&atilde; số xuất sang TQ được c&ocirc;ng khai chứ kh&ocirc;ng bảo mật như c&aacute;c nước n&ecirc;n dễ bị giả mạo. Ch&iacute;nh v&igrave; vậy, c&aacute;c cơ quan chức năng cần phải c&oacute; quy định, hướng dẫn chi tiết cũng như chế t&agrave;i mạnh đối với những trường hợp giả xuất xứ.&nbsp;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', 0, 1, 1, '2020-08-24 18:10:33', '2020-08-24 18:10:33');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint(20) unsigned NOT NULL DEFAULT '0',
  `contentHot` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sale` bigint(20) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci COMMENT 'nội dung sản phẩm',
  `status` tinyint(4) NOT NULL DEFAULT '2' COMMENT 'trạng thái: 1:Bản nháp, 2:chờ duyệt, 3: đã xuất bản, 4: đã xóa',
  `category_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id_index` (`id`),
  KEY `product_category_id_index` (`category_id`),
  CONSTRAINT `product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `theloai` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.product: ~13 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `name`, `price`, `contentHot`, `image`, `slug`, `sale`, `content`, `status`, `category_id`, `created_at`, `updated_at`) VALUES
	(1, 'Xoài An Giang', 20000, 'Hiệu quả kinh tế của cây xoài tại An Giang đã mang lợi nhuận cao hơn từ 7 - 10 lần so với trồng lúa (tùy giá cả thị trường hàng năm và từng loại giống xoài).', 'xoai.jpg', 'xoai-an-giang-1', 10, '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>', 3, 1, '2020-08-02 17:47:17', '2020-08-24 17:43:41'),
	(2, 'Vải thiều Hưng Yên', 19500, 'Vải thiều hưng yên', 'IMAGE-PRODUCT1596371208vai-thieu-tuoi-1.png', 'vai-thieu-hung-yen-2', 0, '<h2 id="article_sapo" class="ctTp tuht_show">D&ugrave; chỉ l&agrave; những h&igrave;nh ảnh chụp th&ocirc;ng thường kh&ocirc;ng qua chỉnh sửa nhưng v&oacute;c d&aacute;ng của hoa hậu H&agrave;n Quốc vẫn được thể hiện.</h2>\r\n<div class="sbNws">Sự kiện:&nbsp;<a class="sbevt clrGr fs12" title="L&agrave;m đẹp c&ugrave;ng SAO" href="https://www.24h.com.vn/lam-dep-cung-sao-c78e711.html">L&agrave;m đẹp c&ugrave;ng SAO</a>,&nbsp;<a class="sbevt clrGr" title="Giảm c&acirc;n" href="https://www.24h.com.vn/giam-can-c78e5605.html">Giảm c&acirc;n</a></div>\r\n<p align="center"><img class="news-image initial loading" src="https://cdn.24h.com.vn/upload/2-2020/images/2020-05-18/Cuu-hoa-hau-Han-Quoc-hon-50-tuoi-len-top-tim-kiem-chi-voi-hai-buc-anh-voc-dang-qua-nuot-5be44e4bf0cc3bfe1a97bd9a2cf38415_7c62d74e-17c4-405-1589795285-117-width520height728.jpg" alt="Cựu hoa hậu H&agrave;n Quốc hơn 50 tuổi l&ecirc;n top t&igrave;m kiếm chỉ với 2 bức ảnh - 1" data-was-processed="true" /></p>\r\n<p class="img_chu_thich_0407">Cựu hoa hậu H&agrave;n Quốc diện quần short, &aacute;o trễ vai khoe v&oacute;c d&aacute;ng nuột n&agrave;.</p>\r\n<p>Mới đ&acirc;y, cộng đồng mạng H&agrave;n Quốc x&ocirc;n xao về hai bức ảnh của cựu hoa hậu, diễn vi&ecirc;n Kim Sung Ryung. Bức ảnh được chụp vội nhưng vẫn đủ thể hiện được được vẻ trẻ trung c&ugrave;ng v&oacute;c d&aacute;ng thon thả của người đẹp sinh năm 1967. Một số người b&igrave;nh luận:&nbsp;<em>"Nh&igrave;n như thế n&agrave;y chỉ&nbsp;giống như 30 tuổi th&ocirc;i"</em>,&nbsp;<em>"Đ&acirc;y l&agrave; ảnh chụp v&agrave;o năm 2017 m&agrave;, đỉnh thật sự"</em>,&nbsp;<em>"Cứ tưởng ảnh l&acirc;u lắm rồi nhưng mới chụp năm 2017 th&ocirc;i n&agrave;y"</em>, "V&oacute;c d&aacute;ng chuẩn thật",... Cư d&acirc;n mạng Việt Nam cũng đồng t&igrave;nh với &yacute; kiến n&agrave;y, khẳng định vẻ đẹp "bất l&atilde;o" của người đẹp xứ sở kim chi.&nbsp;</p>\r\n<p align="center"><img class="news-image loaded" src="https://cdn.24h.com.vn/upload/2-2020/images/2020-05-18/Cuu-hoa-hau-Han-Quoc-hon-50-tuoi-len-top-tim-kiem-chi-voi-hai-buc-anh-voc-dang-qua-nuot-20170419094808-17932086-1664375673576551-570101234-1589795617-94-width620height620.jpg" alt="Cựu hoa hậu H&agrave;n Quốc hơn 50 tuổi l&ecirc;n top t&igrave;m kiếm chỉ với 2 bức ảnh - 2" data-original="https://cdn.24h.com.vn/upload/2-2020/images/2020-05-18/Cuu-hoa-hau-Han-Quoc-hon-50-tuoi-len-top-tim-kiem-chi-voi-hai-buc-anh-voc-dang-qua-nuot-20170419094808-17932086-1664375673576551-570101234-1589795617-94-width620height620.jpg" data-was-processed="true" /></p>\r\n<p class="img_chu_thich_0407">Những tưởng những h&igrave;nh ảnh n&agrave;y chụp đ&atilde; l&acirc;u nhưng thực chất mới chụp năm 2017.</p>\r\n<p>Kim Sung Ryung từng đăng quang ng&ocirc;i vị hoa hậu H&agrave;n Quốc v&agrave;o năm 1988 v&agrave; đại diện quốc gia tham gia đấu trường quốc tế l&agrave; hoa hậu Ho&agrave;n vũ năm 1989. Tiếp đ&oacute; c&ocirc; đảm nhận vai tr&ograve; ph&oacute;ng vi&ecirc;n của đ&agrave;i KBS v&agrave; ph&aacute;t triển sự nghiệp diễn vi&ecirc;n. Kim Sung Ryung vẫn được gọi l&agrave; "mẹ Kim Tan" với phần thể hiện ấn tượng trọng bộ phim "The Heirs". C&ocirc; cũng từng g&acirc;y x&ocirc;n xao khi nhiều lần đến Việt Nam đặc biệt l&agrave; thăm c&ocirc; con g&aacute;i nu&ocirc;i người Việt Nam.</p>\r\n<div id="ADS_159_15s" class="txtCent">\r\n<div id="ADS_159_15stxtBnrHor"></div>\r\n</div>\r\n<p align="center"><img class="news-image loaded" src="https://cdn.24h.com.vn/upload/2-2020/images/2020-05-18/Cuu-hoa-hau-Han-Quoc-hon-50-tuoi-len-top-tim-kiem-chi-voi-hai-buc-anh-voc-dang-qua-nuot-64782841_173172350372146_7823562813425886522_n-1589795739-855-width1080height1350.jpg" alt="Cựu hoa hậu H&agrave;n Quốc hơn 50 tuổi l&ecirc;n top t&igrave;m kiếm chỉ với 2 bức ảnh - 3" data-original="https://cdn.24h.com.vn/upload/2-2020/images/2020-05-18/Cuu-hoa-hau-Han-Quoc-hon-50-tuoi-len-top-tim-kiem-chi-voi-hai-buc-anh-voc-dang-qua-nuot-64782841_173172350372146_7823562813425886522_n-1589795739-855-width1080height1350.jpg" data-was-processed="true" /></p>\r\n<p class="img_chu_thich_0407">Người đẹp từng đăng quang ng&ocirc;i vị hoa hậu H&agrave;n Quốc v&agrave; đại diện đi thi quốc tế.</p>\r\n<p>Hiện tại c&ocirc; đ&atilde; 53&nbsp;tuổi nhưng vẫn được nhận x&eacute;t l&agrave; trẻ trung hơn tuổi. Cụ thể, để g&igrave;n giữ n&eacute;t thanh xu&acirc;n, Kim Sung Ryung đ&atilde; &aacute;p dụng một số c&aacute;ch l&agrave;m đẹp sau:</p>\r\n<p><strong>Bảo vệ da khỏi &aacute;nh nắng</strong></p>\r\n<p>Kim Sung Ryung từng chia sẻ c&ocirc; thường xuy&ecirc;n massage để gi&uacute;p lưu th&ocirc;ng m&aacute;u l&agrave;m đẹp da, chống l&atilde;o h&oacute;a. Đồng thời massage cũng gi&uacute;p hấp thụ tinh chất dưỡng da tốt hơn&nbsp;để việc duy tr&igrave; vẻ đẹp được hiệu quả. B&ecirc;n cạnh đ&oacute;, c&ocirc; cũng ch&uacute; &yacute; đến việc bảo vệ da khỏi những tia s&aacute;ng c&oacute; hại trong &aacute;nh s&aacute;ng mặt trời bằng c&aacute;ch thoa kem chống nắng mỗi ng&agrave;y.&nbsp;</p>\r\n<p align="center"><img class="news-image loaded" src="https://cdn.24h.com.vn/upload/2-2020/images/2020-05-18/Cuu-hoa-hau-Han-Quoc-hon-50-tuoi-len-top-tim-kiem-chi-voi-hai-buc-anh-voc-dang-qua-nuot-47170210_329761321195751_6299779317409059937_n-1589795886-605-width960height960.jpg" alt="Cựu hoa hậu H&agrave;n Quốc hơn 50 tuổi l&ecirc;n top t&igrave;m kiếm chỉ với 2 bức ảnh - 4" data-original="https://cdn.24h.com.vn/upload/2-2020/images/2020-05-18/Cuu-hoa-hau-Han-Quoc-hon-50-tuoi-len-top-tim-kiem-chi-voi-hai-buc-anh-voc-dang-qua-nuot-47170210_329761321195751_6299779317409059937_n-1589795886-605-width960height960.jpg" data-was-processed="true" /></p>\r\n<p class="img_chu_thich_0407" align="center">Sử dụng kem chống nắng kh&ocirc;ng chỉ tr&aacute;nh việc da sạm đen m&agrave; c&ograve;n gi&uacute;p da chống l&atilde;o h&oacute;a.</p>\r\n<p><strong>Kiểm tra c&acirc;n nặng mỗi ng&agrave;y</strong></p>\r\n<p>Để c&oacute; được c&acirc;n nặng đ&uacute;ng chuẩn, Kim Sung Ryung kiểm so&aacute;t rất chặt trọng lượng của m&igrave;nh. Ngay khi thức dậy c&ocirc; sẽ kiểm tra c&acirc;n nặng v&agrave; cố gắng kh&ocirc;ng vượt qu&aacute; mức trung b&igrave;nh. C&acirc;n nặng của cựu hoa hậu H&agrave;n Quốc trong khoảng 53 đến 54kg. Về chế độ ăn, Kim Sung Ryung hạn chế ăn tinh bột v&agrave; kh&ocirc;ng uống đồ uống lạnh. C&ocirc; n&oacute;i th&ecirc;m m&igrave;nh lu&ocirc;n cố gắng đi ngủ sớm v&agrave; dậy sớm, thời gian ăn s&aacute;ng cố định l&agrave; v&agrave;o l&uacute;c 7:30. Người đẹp khẳng định khi đi ngủ sớm c&ocirc; sẽ &iacute;t ăn đ&ecirc;m hơn.&nbsp;</p>', 3, 1, '2020-08-02 12:26:48', '2020-08-24 17:43:54'),
	(3, 'Máy gặt đập liên hợp mini', 120000, 'Công ty mới cho ra mắt sản phẩm máy gặt đập liên hợp mini thế hệ mới model SCM 4LZ-1.3 .Máy có chức năng quạt gió sàng rung nên thóc gặt cực sạch được bà con ưa chuộng tin dùng có thể bán thảnh phẩm cho thương lái luôn tại ruộng.', 'IMAGE-PRODUCT1597069291maxresdefault (1).jpg', 'may-gat-dap-lien-hop-mini-3', 1, '<p>C&ocirc;ng ty mới cho ra mắt sản phẩm m&aacute;y gặt đập li&ecirc;n hợp mini thế hệ mới model SCM 4LZ-1.3 .M&aacute;y c&oacute; chức năng quạt gi&oacute; s&agrave;ng rung n&ecirc;n th&oacute;c gặt cực sạch được b&agrave; con ưa chuộng tin d&ugrave;ng c&oacute; thể b&aacute;n thảnh phẩm cho thương l&aacute;i lu&ocirc;n tại ruộng.Ngo&agrave;i ra m&aacute;y c&ograve;n c&oacute; ưu điểm như c&ocirc;ng suất lớn, h&agrave;m cắt lớn nhưng k&iacute;ch thước lại cực kỳ nhỏ gọn ph&ugrave; hợp với ruộng bậc thang manh m&uacute;n, ruộng nhỏ.M&aacute;y c&oacute; thiết kế cải tiến từ c&aacute;c d&ograve;ng m&aacute;y gặt đập li&ecirc;n hợp kh&aacute;c với bản x&iacute;ch to , gầm cao n&ecirc;n m&aacute;y c&oacute; thể đi được ruộng s&igrave;nh lầy. Một chiếc m&aacute;y với rất nhiều cải tiến b&agrave; con ch&uacute; &yacute; xem hết video để thấy được nhưng ưu điểm của chiếc m&aacute;y gặt l&uacute;a mini thế hệ mới n&agrave;y</p>\r\n<p>&nbsp;C&ocirc;ng suất m&aacute;y 20HP</p>\r\n<p>H&agrave;m cắt 1m3</p>\r\n<p>Trọng lượng 750kg</p>\r\n<p>Bản x&iacute;ch 350x38x90</p>\r\n<p>Năng suất: 1500m2/h</p>\r\n<p>Mức ti&ecirc;u hao nhi&ecirc;n liệu 1,2 - 1,5 lit/h</p>\r\n<p>C&oacute; hệ thống s&agrave;ng rung cho l&uacute;a rất sạch</p>\r\n<p><iframe src="http://www.youtube.com/embed/w4rTOaluPJk" width="800" height="800" data-mce-fragment="1"></iframe></p>', 3, 5, '2020-08-02 17:14:49', '2020-08-10 14:21:43'),
	(4, 'Cá Ngừ', 10000, 'cá ngừ', 'IMAGE-PRODUCT1596419125cangudaiduong_master.jpg', 'ca-ngu-4', 3, '<p>C&aacute; ngừ đại dương (hay c&ograve;n gọi l&agrave; c&aacute; b&ograve; g&ugrave;) l&agrave; loại c&aacute; lớn thuộc họ C&aacute; bạc m&aacute; (Scombridae), chủ yếu thuộc chi Thunnus, sinh sống ở v&ugrave;ng biển ấm, c&aacute;ch bờ độ 185 km trở ra. Ở Việt Nam, C&aacute; ngừ đại dương l&agrave; t&ecirc;n địa phương để chỉ loại c&aacute; ngừ mắt to v&agrave; c&aacute; ngừ v&acirc;y v&agrave;ng[1]. C&aacute; ngừ đại dương l&agrave; loại hải sản đặc biệt thơm ngon, mắt rất bổ (c&aacute; ngừ mắt to), được chế biến th&agrave;nh nhiều loại m&oacute;n ăn ngon v&agrave; tạo nguồn h&agrave;ng xuất khẩu c&oacute; gi&aacute; trị.</p>', 3, 2, '2020-08-02 17:22:13', '2020-08-09 05:02:14'),
	(5, 'Tôm hùm Alaska', 300000, 'Tôm hùm giảm giá chưa từng thấy, dân buôn "khóc dở" vì ôm cả tấn hàng tồn', 'IMAGE-PRODUCT1596419492tom-hum-1.jpg', 'tom-hum-alaska-5', 5, '<p>Chưa khi n&agrave;o gi&aacute; t&ocirc;m h&ugrave;m giảm mạnh như hiện tại. Đặc biệt l&agrave; t&ocirc;m h&ugrave;m ngộp, mỗi kg giảm xuống c&ograve;n 150.000 - 160.000 đồng/kg, rẻ hơn cả thịt lợn.</p>', 1, 2, '2020-08-03 01:51:32', '2020-08-24 13:27:58'),
	(6, 'Bưởi Năm Roi', 12000, 'Nếu miền Bắc có bưởi Đoan Hùng, bưởi Diễn; miền Trung có bưởi Phúc Trạch, bưởi Thanh Trà là những giống ngon nổi tiếng.', 'IMAGE-PRODUCT1596446463buoi-nam-roi.jpg', 'buoi-nam-roi-6', 0, '<p>Bưởi Năm Roi Vĩnh Long l&agrave; một trong những đặc sản Vĩnh Long, xuất ph&aacute;t từ x&atilde; Mỹ H&ograve;a, huyện B&igrave;nh Minh. V&ugrave;ng đất B&igrave;nh Minh được bồi đắp ph&ugrave; sa từ s&ocirc;ng Hậu, s&ocirc;ng Măng Th&iacute;t v&agrave; s&ocirc;ng Tr&agrave; Von. Ch&iacute;nh n&oacute; l&agrave; điều kiện địa h&igrave;nh thuận lợi để tạo ra giống bưởi ngon nổi tiếng như vậy. Th&ecirc;m v&agrave;o đ&oacute;, điều kiện thời tiết nơi đ&acirc;y cũng thuận lợi cho qu&aacute; tr&igrave;nh sinh trưởng, ph&aacute;t triển tốt nhất của giống bưởi Năm Roi. Từ th&aacute;ng 9 đến th&aacute;ng 12 l&agrave; những th&aacute;ng &iacute;t mưa rơi v&agrave;o giai đoạn ra hoa, thu hoạch của c&acirc;y. Trong giai đoạn n&agrave;y, c&acirc;y chỉ cần &iacute;t nước để k&iacute;ch th&iacute;ch hoa nở v&agrave; giữ được h&agrave;m lượng tối đa c&aacute;c dưỡng chất. Những th&aacute;ng mưa nhiều như th&aacute;ng 6, 7 lại ứng với giai đoạn t&iacute;ch lũy đường, vitamin C v&agrave; nước cho quả.</p>', 3, 1, '2020-08-03 09:21:03', '2020-08-09 05:02:31'),
	(7, 'Chuối Ngự', 15000, 'Vốn là đặc sản nông nghiệp nổi tiếng thuộc làng Đại Hoàng (làng Vũ Đại) xã Hòa Hậu, huyện Lý Nhân, tỉnh Hà Nam, chuối ngự được lùng mua bởi các thương lái khắp vùng miền, kể cả xuất khẩu', 'IMAGE-PRODUCT1596446678chuoi.jpg', 'chuoi-ngu-7', 0, '<p>Gọi l&agrave; chuối ngự, v&igrave; n&oacute; được chọn l&agrave; đặc sản tiến vua từ đời nh&agrave; Trần (thế kỷ thứ XIII). Chuối ngự, hay chuối Tiến Vua l&agrave; n&ocirc;ng sản được trồng chủ yếu ở miền Bắc, trong đ&oacute; nổi tiếng nhất v&agrave; ngon nhất l&agrave; ở l&agrave;ng Đại Ho&agrave;ng, nơi c&oacute; thổ nhưỡng, kh&iacute; hậu ho&agrave;n hảo để c&acirc;y chuối ngự ph&aacute;t triển.</p>', 3, 1, '2020-08-03 09:24:38', '2020-08-24 17:44:06'),
	(8, 'Gà đông tảo', 120000, 'Nhiều hộ nuôi gà Đông Tảo trước đây bán mỗi đợt 1.000 - 3.000 con thì nay số lượng mua chỉ ở mức 200 - 300 con một tháng.', 'IMAGE-PRODUCT1596446954gà.jpg', 'ga-dong-tao-8', 1, '<p>&Ocirc;ng Minh, chủ trang trại nu&ocirc;i g&agrave; Đ&ocirc;ng Tảo ở quận 9 (TP HCM) cho biết, nếu 3 năm trước trang trại nh&agrave; &ocirc;ng rộng một ha v&agrave; số lượng nu&ocirc;i l&ecirc;n tới v&agrave;i ngh&igrave;n con th&igrave; nay đ&atilde; thu hẹp diện t&iacute;ch v&agrave; chỉ nu&ocirc;i khoảng hơn 30 con g&agrave; m&aacute;i để l&agrave;m giống. Theo &ocirc;ng Minh, do nhu cầu ti&ecirc;u thụ đi xuống, ngay cả lễ, Tết số lượng đặt mua cũng giảm 30-40%, n&ecirc;n &ocirc;ng buộc phải thu hẹp quy m&ocirc;. Trong khi đ&oacute;, gi&aacute; g&agrave; cũng hạ nhiệt so với c&aacute;ch đ&acirc;y v&agrave;i năm. Nếu 2016, g&agrave; Đ&ocirc;ng Tảo c&oacute; trọng lượng 4-5 kg thường b&aacute;n theo con ở mức 20-30 triệu đồng th&igrave; nay nhiều con gi&aacute; cao lắm chỉ 10-15 triệu đồng. Với loại dưới 3 kg gi&aacute; g&agrave; thịt giảm 20-30% xuống c&ograve;n 200.000 - 300.000 đồng một kg.</p>', 3, 3, '2020-08-03 09:29:14', '2020-08-09 05:02:57'),
	(9, 'Bồ câu', 50000, 'Ông bà ta xưa cũng biết nuôi Bồ Câu từ lâu, nhưng cách nuôi của mình có tính gia đình, lại nuôi thả nên kết quả thu được không đáng là bao, nhiều khi còn phải nếm mùi thất bại nữa.', 'IMAGE-PRODUCT1596447133bocau.jpg', 'bo-cau-9', 1, '<p>Ngo&agrave;i ra, hầu hết c&aacute;c nước cũng đ&atilde; lập ra nhiều trại nu&ocirc;i Bồ C&acirc;u hơn, chẳng hạn như ở Ph&aacute;p c&oacute; trại EURO PIGEON, v&agrave; ngay c&aacute;c nước l&aacute;ng giềng với ta như Th&aacute;i Lan, Singapore&hellip; cũng c&oacute; nhiều trại Bồ C&acirc;u rất lơn. &Ocirc;ng b&agrave; ta xưa cũng biết nu&ocirc;i Bồ C&acirc;u từ l&acirc;u, nhưng c&aacute;ch nu&ocirc;i của m&igrave;nh c&oacute; t&iacute;nh gia đ&igrave;nh, lại nu&ocirc;i thả n&ecirc;n kết quả thu được kh&ocirc;ng đ&aacute;ng l&agrave; bao, nhiều khi c&ograve;n phải nếm m&ugrave;i thất bại nữa. Đến nỗi, c&aacute;c cụ phải khuy&ecirc;n con ch&aacute;u đừng nghĩ đến chuyện l&agrave;m gi&agrave;u bằng c&aacute;ch nu&ocirc;i Bồ C&acirc;u: &ldquo;Muốn gi&agrave;u nu&ocirc;i tr&acirc;u n&aacute;i, Muốn lụn bại nu&ocirc;i bồ c&acirc;u&rdquo;. C&aacute;ch nu&ocirc;i Bồ C&acirc;u ta trước đ&acirc;y l&agrave; nu&ocirc;i thả. M&agrave; nu&ocirc;i thả th&igrave; c&oacute; nhiều l&yacute; do để chim nh&agrave; bỏ chuồng m&agrave; đi. T&iacute;nh &yacute; của Bồ C&acirc;u kh&ocirc;ng bội bạc v&oacute;i chủ, nhưng nếu một khi điều kiện sống của n&oacute; kh&ocirc;ng c&ograve;n th&iacute;ch hợp th&igrave; n&oacute; phải t&igrave;m nơi ở l&yacute; tưởng kh&aacute;c để sinh sống m&agrave; th&ocirc;i. Nếu nu&ocirc;i Bồ C&acirc;u m&agrave; kh&ocirc;ng nắm vững phần kỹ thuật chăn nu&ocirc;i, từ c&aacute;ch l&agrave;m chuồng trại, từ c&aacute;ch chăm s&oacute;c đến c&aacute;ch cho ăn&hellip; th&igrave; ch&uacute;ng rủ nhau bỏ chuồng m&agrave; đi cũng phải. Bồ C&acirc;u th&iacute;ch ở chuồng rộng r&atilde;i v&agrave; cao r&aacute;o. Bạn n&ecirc;n nghĩ đến c&aacute;ch đặt chuồng ở một độ cao th&iacute;ch hợp n&agrave;o đế Bồ C&acirc;u khi bay đi kiếm ăn c&oacute; thể nhận định được hướng chuồng của n&oacute; m&agrave; bay về, nhất l&agrave; đối v&oacute;i những chim mới. Chuồng phải được quay về hướng Nam hoặc Đ&ocirc;ng Nam để hưởng nhiều &aacute;nh nắng mặt trời, v&agrave; đồng thời tr&aacute;nh trực tiếp mưa tạt. Bồ C&acirc;u rất sợ ch&oacute; v&agrave; m&egrave;o, nhất l&agrave; m&egrave;o. M&egrave;o rất th&iacute;ch vồ Bồ C&acirc;u để ăn thịt, v&igrave; vậy nu&ocirc;i Bồ C&acirc;u l&agrave; phải trừ m&egrave;o. M&egrave;o kh&ocirc;ng thể ở chung nh&agrave; hay chung khu vườn với Bồ C&acirc;u. Nếu chuồng Bồ C&acirc;u bị m&egrave;o đến viếng nhiều lần, hoặc Bồ C&acirc;u đang kiếm ăn trong vườn trong s&acirc;n m&agrave; bị m&egrave;o vồ hụt, ch&uacute;ng cũng hoảng hồn bỏ chuồng m&agrave; đi. Một con đi th&igrave; thế n&agrave;o con kh&aacute;c cũng bay theo. Nu&ocirc;i Bồ C&acirc;u thả cũng phải biết c&aacute;ch cho ăn: s&aacute;ng cho ăn ch&uacute;t &iacute;t, nhưng chiều tối khi Bồ C&acirc;u tụ về phải cho ăn thật no. Nếu nu&ocirc;i m&agrave; kh&ocirc;ng cho ăn th&igrave; ch&uacute;ng sẽ t&igrave;m nơi no đủ m&agrave; tới. Ta c&oacute; c&acirc;u &ldquo;Th&oacute;c đ&acirc;u bồ c&acirc;u đấy&rdquo; th&igrave; đủ thấy rằng giống chim n&agrave;y kh&ocirc;n ngoan đến mức n&agrave;o. Nếu những điều kiện tr&ecirc;n kh&ocirc;ng được thực hiện đ&uacute;ng như vậy th&igrave; bạn đừng tr&aacute;ch Bồ C&acirc;u bỏ bạn m&agrave; đi.</p>', 3, 3, '2020-08-03 09:32:13', '2020-08-09 05:03:05'),
	(10, 'Nhãn nồng Hưng Yên', 22000, 'NHÃN LỒNG HƯNG YÊN', 'IMAGE-PRODUCT1596468986nhanNong.jpg', 'nhan-nong-hung-yen-10', 0, '<p>KINH DOANH❯THỊ TRƯỜNG Face Book Twitter B&igrave;nh luận Tin n&oacute;ng Lạ Việt Nam, ch&ugrave;m nh&atilde;n lồng cổ Hưng Y&ecirc;n b&aacute;n gi&aacute; 100 triệu đồng</p>', 3, 1, '2020-08-03 15:36:26', '2020-08-23 19:04:00'),
	(12, 'Máy Cấy lúa tự động', 2000, 'Máy Cấy lúa tự động', 'gia-may-cay-lua-kubota-2.jpg', 'may-cay-lua-tu-dong-12', 5, '<p>M&aacute;y Cấy l&uacute;a tự động</p>', 3, 5, '2020-08-10 10:01:10', '2020-08-13 08:54:34'),
	(13, 'Hồng cát', 15000, 'Loài hồng phổ biến nhất cho trái là hồng Nhật Bản', 'sapo.jpg', 'hong-cat-13', 0, '<div class="title-detail">Lợi &iacute;ch sức khỏe của quả hồng gi&ograve;n</div>\r\n<div class="author-share-top cl99 fs13 d-flex align-items-center mt20 mb20">\r\n<div class="mr-auto"><a class="author cl_green">Mai Hương - Học viện Qu&acirc;n Y</a>&nbsp;-&nbsp;<span class="post-time">07:36 23/09/2019 GMT+7</span></div>\r\n<div class="ml-auto">\r\n<div class="fb-send" data-href="https://suckhoedoisong.vn/loi-ich-suc-khoe-cua-qua-hong-gion-n131112.html" data-width="52" data-height="20" data-colorscheme="light">&nbsp;</div>\r\n<div class="fb-like fb_iframe_widget" data-href="https://suckhoedoisong.vn/loi-ich-suc-khoe-cua-qua-hong-gion-n131112.html" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"><iframe class="" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/v3.1/plugins/like.php?action=like&amp;app_id=532325580277515&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df14a0d8df85633c%26domain%3Dsuckhoedoisong.vn%26origin%3Dhttps%253A%252F%252Fsuckhoedoisong.vn%252Ff2355a521337ca8%26relation%3Dparent.parent&amp;container_width=5&amp;href=https%3A%2F%2Fsuckhoedoisong.vn%2Floi-ich-suc-khoe-cua-qua-hong-gion-n131112.html&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;show_faces=false" name="f3ca9a4c7fc1864" width="1000px" height="1000px" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen" data-testid="fb:like Facebook Social Plugin" data-mce-fragment="1"></iframe></div>\r\n</div>\r\n</div>\r\n<div class="sapo_detail">Suckhoedoisong.vn - C&acirc;y hồng gi&ograve;n được trồng rộng r&atilde;i ở Bắc Mỹ, c&oacute; t&ecirc;n khoa học l&agrave; Diospyros. Quả hồng khi ch&iacute;n c&oacute; m&agrave;u v&agrave;ng hoặc v&agrave;ng sậm bắt mắt, trong th&agrave;nh phần c&oacute; chứa rất nhiều vi chất mang lợi &iacute;ch sức khỏe tuyệt vời đối với cơ thể v&agrave; sắc đẹp của bạn.</div>\r\n<div id="content_detail_news">\r\n<h2><strong>Tăng cường&nbsp;<a title="6 vi chất gi&uacute;p tăng cường sức khỏe v&agrave; hệ miễn dịch m&ugrave;a lạnh" href="http://suckhoedoisong.vn/6-vi-chat-giup-tang-cuong-suc-khoe-va-he-mien-dich-mua-lanh-n110021.html">hệ miễn dịch</a>:</strong></h2>\r\n<p>Nhờ đặc t&iacute;nh của chất chống oxy h&oacute;a v&agrave; ph&ograve;ng chống ung thư, quả hồng gi&ograve;n cũng hữu &iacute;ch trong việc tăng cường hệ miễn dịch. Quả hồng gi&ograve;n c&oacute; chứa h&agrave;m lượng cao nhất c&aacute;c axit ascorbic (vitamin C) v&agrave; c&oacute; thể đ&aacute;p ứng được khoảng 80% nhu cầu h&agrave;ng ng&agrave;y của chất dinh dưỡng n&agrave;y đối với cơ thể. Vitamin C rất c&oacute; lợi để k&iacute;ch th&iacute;ch hệ thống miễn dịch v&agrave; l&agrave;m tăng việc sản xuất c&aacute;c tế b&agrave;o bạch cầu gi&uacute;p cơ thể chống lại vi khuẩn, vi r&uacute;t v&agrave; nhiễm nấm, cũng như c&aacute;c t&aacute;c nh&acirc;n g&acirc;y bệnh từ b&ecirc;n ngo&agrave;i hoặc c&aacute;c chất độc hại.</p>\r\n<h2><strong>Gi&uacute;p cho&nbsp;<a title="M&oacute;n ăn, b&agrave;i thuốc gi&uacute;p hệ ti&ecirc;u h&oacute;a khỏe mạnh" href="http://suckhoedoisong.vn/mon-an-bai-thuoc-giup-he-tieu-hoa-khoe-manh-n4176.html">hệ t</a></strong><strong><a title="M&oacute;n ăn, b&agrave;i thuốc gi&uacute;p hệ ti&ecirc;u h&oacute;a khỏe mạnh" href="http://suckhoedoisong.vn/mon-an-bai-thuoc-giup-he-tieu-hoa-khoe-manh-n4176.html">i&ecirc;u h&oacute;a khỏe mạnh</a>:</strong></h2>\r\n<p>Giống như hầu hết c&aacute;c loại tr&aacute;i c&acirc;y, quả hồng gi&ograve;n cũng l&agrave; một nguồn tốt cung cấp chất xơ, đ&aacute;p ứng gần 20% nhu cầu h&agrave;ng ng&agrave;y. Chất xơ gi&uacute;p c&aacute;c thực phẩm hiệu quả hơn trong qu&aacute; tr&igrave;nh ti&ecirc;u h&oacute;a, bằng c&aacute;ch k&iacute;ch th&iacute;ch nhu động ruột để di chuyển thức ăn qua đường ti&ecirc;u h&oacute;a, l&agrave;m tăng tiết dịch dạ d&agrave;y trong ti&ecirc;u h&oacute;a thức ăn, l&agrave;m giảm c&aacute;c triệu chứng của t&aacute;o b&oacute;n v&agrave; ti&ecirc;u chảy. Nh&igrave;n chung, c&aacute;c loại tr&aacute;i c&acirc;y c&oacute; nhiều chất xơ như quả hồng gi&ograve;n c&oacute; thể l&agrave; một động lực ch&iacute;nh th&uacute;c đẩy nhanh qu&aacute; tr&igrave;nh ti&ecirc;u h&oacute;a, chống lại bệnh ung thư đại trực tr&agrave;ng v&agrave; c&aacute;c bệnh tương tự kh&aacute;c. Quả hồng gi&ograve;n cũng c&oacute; thể gi&uacute;p&nbsp;<a title="12 sai lầm khi giảm c&acirc;n thường gặp" href="http://suckhoedoisong.vn/12-sai-lam-khi-giam-can-thuong-gap-n111330.html">giảm c&acirc;n</a>&nbsp;do giảm sự hấp thu chất b&eacute;o &ndash; l&agrave; nguy&ecirc;n nh&acirc;n dẫn đến b&eacute;o ph&igrave;.</p>\r\n<div id="zones-ads-a597d4e764bd475f85890dc2172d7b96">\r\n<div id="wrapper-ads-a597d4e764bd475f85890dc2172d7b96"><iframe id="a597d4e764bd475f85890dc2172d7b96" src="https://media.yomedia.vn/2020/08/bio-q2/on_image_pc/ngang/index.html?pid=a597d4e764bd475f85890dc2172d7b96&amp;bid=46ae682f7ac54e4a9178fdfcd53d6139&amp;clk=https%3A%2F%2Fshopee.vn%2Fshop%2F140591350%2Fsearch%3Fpage%3D0%26shopCollection%3D37742954%26utm_source%3Dprogrammatics%26utm_medium%3Dseller%26utm_campaign%3Ds140591350_SS_VN_OTHR_Bio-BirdNest-LDN%26utm_content%3Dhttps%253A%252F%252Fshopee.vn%252Fshop%252F140591350%252Fsearch%253Fpage%26deep_and_web%3D1%26pid%3Dprogrammatics%26c%3Ds140591350_SS_VN_OTHR_Bio-BirdNest-LDN" data-mce-fragment="1"></iframe></div>\r\n<img src="http://suckhoedoisong.vn/Images/hohuong/2017/05/03/qua-hong.jpg" alt="" width="500" height="375" /></div>\r\n<h2><strong><a title="Chế độ dinh dưỡng cho bệnh nh&acirc;n ung thư" href="http://suckhoedoisong.vn/che-do-dinh-duong-cho-benh-nhan-ung-thu-n116354.html">Chống ung thư</a>:</strong></h2>\r\n<p>C&ugrave;ng với t&iacute;nh chất chống oxy h&oacute;a c&oacute; thể ph&ograve;ng chống ung thư v&agrave; nguy cơ ph&aacute;t triển khối u. Quả hồng gi&ograve;n chứa Betulinic acid, được chứng minh l&agrave; một hợp chất chống ung thư. Nếu bạn đ&atilde; c&oacute; một khối u, ăn hồng gi&ograve;n c&oacute; thể l&agrave;m giảm k&iacute;ch thước v&agrave; ngăn chặn sự tiến triển th&agrave;nh ung thư. Ngo&agrave;i ra, quả hồng gi&ograve;n c&oacute; chứa h&agrave;m lượng cao vitamin C v&agrave; vitamin A, cũng như c&aacute;c hợp chất phenolic như catechin v&agrave; gallocatechins &ndash; l&agrave; những chất li&ecirc;n quan trực tiếp đến c&ocirc;ng t&aacute;c ph&ograve;ng chống c&aacute;c loại ung thư kh&aacute;c nhau.</p>\r\n<p><img src="http://suckhoedoisong.vn/Images/hohuong/2017/05/03/qua_hong_gion.jpg" alt="" width="600" height="400" /></p>\r\n<p><strong><a title="Những b&agrave;i tập chống l&atilde;o h&oacute;a hiệu quả" href="http://suckhoedoisong.vn/nhung-bai-tap-chong-lao-hoa-hieu-qua-n33598.html">Chống l&atilde;o h&oacute;a</a>:</strong><br />Quả hồng gi&ograve;n rất gi&agrave;u một số vitamin, đặc biệt l&agrave; vitamin A, beta-carotene, lutein, lycopene v&agrave; cryptoxanthins. Tất cả c&aacute;c chất n&agrave;y cũng c&oacute; chức năng như chất chống oxy h&oacute;a v&agrave; ngăn ngừa c&aacute;c dấu hiệu l&atilde;o h&oacute;a sớm của da như nếp nhăn, đốm đen do tuổi t&aacute;c cũng như&nbsp;<a title="7 vấn đề cần lưu &yacute; khi chăm s&oacute;c dinh dưỡng cho người bệnh Alzheimer" href="http://suckhoedoisong.vn/7-van-de-can-luu-y-khi-cham-soc-dinh-duong-cho-nguoi-benh-alzheimer-n124694.html">bệnh Alzheimer</a>&nbsp;(mất tr&iacute; nhớ), mệt mỏi, thị lực, suy nhược cơ bắp v&agrave; c&aacute;c biểu hiện sức khỏe kh&aacute;c.</p>\r\n<h2><strong>Sức khỏe thị gi&aacute;c</strong></h2>\r\n<p>Một số hợp chất trong quả hồng gi&ograve;n cũng tỏ ra c&oacute; lợi cho sức khoẻ của mắt. Zeaxanthin, m&agrave; l&agrave; một th&agrave;nh phần của c&aacute;c vitamin B phức, li&ecirc;n quan trực tiếp đến sự gia tăng sức khỏe mắt v&igrave; bản chất của n&oacute; cũng l&agrave; chất chống oxy h&oacute;a. Nghi&ecirc;n cứu cho thấy rằng Zeaxanthin c&oacute; thể l&agrave;m giảm sự&nbsp;<a title="Cảnh b&aacute;o: Tho&aacute;i h&oacute;a điểm v&agrave;ng đang trẻ h&oacute;a" href="http://suckhoedoisong.vn/canh-bao-thoai-hoa-diem-vang-dang-tre-hoa-n126163.html">tho&aacute;i h&oacute;a điểm v&agrave;ng</a>,&nbsp;<a title="Bệnh Đục Thủy Tinh Thể - Nguy&ecirc;n nh&acirc;n, biểu hiện, c&aacute;ch ph&ograve;ng ngừa v&agrave; chữa trị" href="http://suckhoedoisong.vn/benh-duc-thuy-tinh-the-nguyen-nhan-bieu-hien-cach-phong-ngua-va-chua-tri--n131695.html">đục thủy tinh thể</a>&nbsp;v&agrave;&nbsp;<a title="Bệnh qu&aacute;ng g&agrave; dưới g&oacute;c nh&igrave;n của y học hiện đại" href="http://suckhoedoisong.vn/benh-quang-ga-duoi-goc-nhin-cua-y-hoc-hien-dai-n59633.html">bệnh qu&aacute;ng g&agrave;</a>.</p>\r\n<p><img src="http://suckhoedoisong.vn/Images/hohuong/2017/05/03/thoai_hoa_diem_vang.jpg" alt="" width="300" height="233" /></p>\r\n<h2><br /><strong>G</strong><strong>iảm huyết &aacute;p cao:</strong></h2>\r\n<p>Kali l&agrave; một kho&aacute;ng chất được t&igrave;m thấy c&oacute; một lượng đ&aacute;ng kể trong quả hồng gi&ograve;n. Kali c&oacute; thể hoạt động như một thuốc gi&atilde;n mạch v&agrave; l&agrave;m giảm huyết &aacute;p, do đ&oacute; l&agrave;m tăng lưu lượng m&aacute;u đi khắp cơ thể. Điều n&agrave;y sẽ l&agrave;m giảm sự căng thẳng tr&ecirc;n hệ thống tim mạch v&agrave; ngăn ngừa c&aacute;c loại bệnh tim mạch.</p>\r\n<p><img src="http://suckhoedoisong.vn/Images/hohuong/2017/05/03/qua_hong_gion_giup_giam_tang_huyet_ap.jpg" alt="" width="580" height="326" /></p>\r\n<h2><strong>Cải thiện m&aacute;u lưu th&ocirc;ng:</strong></h2>\r\n<p>C&ugrave;ng với huyết &aacute;p, hồng gi&ograve;n cũng cung cấp đồng, một yếu tố cần thiết cho sự h&igrave;nh th&agrave;nh của c&aacute;c tế b&agrave;o hồng cầu. Nếu kh&ocirc;ng c&oacute; đồng, cơ thể kh&ocirc;ng thể hấp thụ c&aacute;c chất dinh dưỡng cần thiết cho sự h&igrave;nh th&agrave;nh của hemoglobin bổ sung. Tăng lưu th&ocirc;ng của c&aacute;c tế b&agrave;o hồng cầu khỏe mạnh để cải thiện chức năng nhận thức, sự trao đổi chất trong cơ, tăng cường năng lượng, l&agrave;m l&agrave;nh vết thương v&agrave; tăng trưởng tế b&agrave;o.</p>\r\n</div>', 3, 1, '2020-08-19 16:47:52', '2020-08-19 16:47:52'),
	(17, 'adDf', 120000, 'sdfDF', 'cc08f638cd4a3d91d0e0e68bd87752c3.jpg', 'addf-17', 0, '<p>DFSDASD</p>', 2, 1, '2020-08-24 09:23:57', '2020-08-24 09:23:57'),
	(18, 'bưởi', 12000, 'skfhdsjhaf dkfad', '11-xoai_tait.jpg', 'buoi-18', 0, '<div class="jsx-2012952166 thread-title__block">\r\n<div class="jsx-2012952166 thread-title">Samsung cũng c&oacute; chức năng t&igrave;m điện thoại bị mất ngay cả khi bị gỡ SIM, tắt Wi-Fi</div>\r\n</div>\r\n<div class="jsx-2012952166 thread-info">\r\n<div class="jsx-2012952166 info-block--left">\r\n<div class="jsx-2012952166 avatar">&nbsp;</div>\r\n<div class="jsx-2012952166 info"><a class="jsx-2012952166 author-name" href="https://tinhte.vn/profile/duy-luan.39792/">Duy Lu&acirc;n</a>\r\n<div class="jsx-2012952166 date-comment-view"><span class="jsx-2012952166 date">9 giờ trước</span><span class="jsx-2012952166 comment">B&igrave;nh luận:&nbsp;73</span><span class="jsx-2012952166 view">Lượt xem:&nbsp;912</span></div>\r\n</div>\r\n</div>\r\n<div class="jsx-2012952166 info-block--right">\r\n<div class="jsx-2012952166 moderator-action"><button class="jsx-2012952166 moderator-action__button">Nhận th&ocirc;ng b&aacute;o</button></div>\r\n</div>\r\n</div>\r\n<div class="jsx-2012952166">&nbsp;</div>\r\n<div class="jsx-2012952166">\r\n<article class="jsx-2012952166 content">\r\n<div class="jsx-2012952166">\r\n<blockquote class="jsx-2012952166">\r\n<div class="jsx-1061394707">\r\n<div class="jsx-1061394707 xfBody big " data-author="Duy Lu&acirc;n"><span class="bdImage_attachImage"><span class="inner"><img src="https://photo2.tinhte.vn/data/attachment-files/2020/08/5127933_cover_home_samsung_find_my_mobile.jpg" alt="cover_home_samsung_find_my_mobile.jpg" data-height="1332" data-width="2152" /></span></span><br /><br />Ứng dụng&nbsp;<a class="Tinhte_XenTag_TagLink" href="https://tinhte.vn/tags/find-my-mobile/">Find My Mobile</a>&nbsp;của&nbsp;<a class="Tinhte_XenTag_TagLink" href="https://tinhte.vn/tags/samsung/">Samsung</a>&nbsp;đang được cập nhật dần dần để hỗ trợ t&iacute;nh năng Offline Finding. Như c&aacute;i t&ecirc;n đ&atilde; gợi &yacute;, chức năng n&agrave;y gi&uacute;p bạn t&igrave;m lại điện thoại ngay cả khi 4G hoặc Wi-Fi đ&atilde; bị tắt. C&oacute; vẻ như Samsung cũng d&ugrave;ng Bluetooth (giống như c&aacute;ch m&agrave; Apple đang d&ugrave;ng cho iOS 13) để c&aacute;c thiết bị Galaxy ph&aacute;t hiện lẫn nhau v&agrave; truyền dữ liệu một c&aacute;ch an to&agrave;n về m&aacute;y chủ. App cũng c&oacute; cung cấp chức năng m&atilde; h&oacute;a dữ liệu vị tr&iacute; offline để tăng t&iacute;nh ri&ecirc;ng tư.<br /><br />V&igrave; sao lại l&agrave; mất Internet? L&uacute;c bạn bị mất điện thoại, hoặc bị ăn cắp điện thoại th&igrave; SIM đ&atilde; bị gỡ n&ecirc;n kh&ocirc;ng c&oacute; 3G 4G, cũng kh&ocirc;ng c&oacute; Wi-Fi v&igrave; đang ở ngo&agrave;i đường. Trước giờ chức năng t&igrave;m kiếm thiết bị thường hoạt động bằng c&aacute;ch d&ugrave;ng Internet để truyền dữ liệu về server của nh&agrave; sản xuất trong trường hợp bạn k&iacute;ch hoạt chế độ t&igrave;m kiếm, nhưng giờ kh&ocirc;ng c&oacute; Internet th&igrave; truyền kiểu g&igrave;?<br /><br />Đ&acirc;y l&agrave; l&uacute;c m&agrave; Bluetooth ph&aacute;t huy hiệu quả. C&aacute;c thiết bị Galaxy c&ograve;n hoạt động b&igrave;nh thường sẽ scan những chiếc Galaxy kh&aacute;c xung quanh n&oacute;, n&oacute; sẽ truyền dữ liệu về server. L&uacute;c đ&oacute; chiếc m&aacute;y bị mất d&ugrave;ng một c&aacute;i smartphone Galaxy kh&aacute;c như l&agrave; đơn vị trung gian để b&aacute;o về m&aacute;y chủ (v&agrave; người d&ugrave;ng) biết vị tr&iacute; của n&oacute;.<br /><br /><a class="internalLink" href="https://tinhte.vn/thread/cach-ios-13-giup-tim-thay-dien-thoai-cua-ban-ngay-ca-khi-no-da-bi-go-sim-tat-wi-fi.2970611/">C&aacute;ch thức n&agrave;y Apple cũng đang d&ugrave;ng cho iOS 13 để t&igrave;m iPhone bị mất l&uacute;c kh&ocirc;ng c&oacute; mạng</a>.<br /><br /><em><strong>Nguồn:&nbsp;</strong></em><a class="externalLink" href="https://twitter.com/MaxWinebach/status/1297167508127047680/photo/2" target="_blank" rel="noopener"><em><strong>Twitter</strong></em></a></div>\r\n</div>\r\n</blockquote>\r\n</div>\r\n</article>\r\n<div class="jsx-2012952166 thread-comment__attachments-container">\r\n<div class="jsx-2012952166 thread-comment__attachments">&nbsp;</div>\r\n</div>\r\n<div class="jsx-2012952166 thread-actions">\r\n<div class="jsx-2012952166 thread-actions__left">\r\n<div class="jsx-2012952166 thread-like">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>', 3, 1, '2020-08-25 06:50:51', '2020-08-25 06:51:57');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.slide
CREATE TABLE IF NOT EXISTS `slide` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contentHot` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `slide_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.slide: ~3 rows (approximately)
/*!40000 ALTER TABLE `slide` DISABLE KEYS */;
INSERT INTO `slide` (`id`, `image`, `title`, `contentHot`, `slug`, `link`, `created_at`, `updated_at`) VALUES
	(1, 'IMAGE-SLIDE-1597073701An-Xoai.jpg', 'Xoài An Giang', 'An Giang: Xoài đất núi vào mùa trái, có bao nhiêu lái "khuân" hết', 'xoai-an-giang-1', 'xoài an giang', '2020-08-01 09:05:12', '2020-08-10 15:35:01'),
	(2, 'IMAGE-SLIDE-1597073821hoa-qua-say-nguyen-vu-178834.jpg', 'Hoa quả sấy Đà Lạt', 'Đà Lạt là thiên đường của rau củ sấy khô.', 'hoa-qua-say-da-lat-2', 'Hoa quả sấy đà lạt', '2020-08-01 02:45:09', '2020-08-10 15:37:01'),
	(3, 'IMAGE-SLIDE-159707400151433331_2219516694957359_2228405453241450496_n.png', 'Máy bay quan sát Phantom', 'Phantom chụp được những hình ảnh rõ ràng, chi tiết.', 'may-bay-quan-sat-phantom-3', 'Máy bay quan sát Phantom', '2020-08-07 10:23:11', '2020-08-10 15:40:01');
/*!40000 ALTER TABLE `slide` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.theloai
CREATE TABLE IF NOT EXISTS `theloai` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id thể loại',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Chuỗi không dấu',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `theloai_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.theloai: ~6 rows (approximately)
/*!40000 ALTER TABLE `theloai` DISABLE KEYS */;
INSERT INTO `theloai` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'Hoa quả tươi', 'hoa-qua-tuoi-1', '2020-08-01 09:05:12', '2020-08-21 07:54:35'),
	(2, 'Hải sản', 'hai-san-2', '2020-08-02 16:57:42', '2020-08-02 16:57:42'),
	(3, 'Gia xúc, gia cầm', 'gia-xuc-gia-cam-3', '2020-08-03 09:27:37', '2020-08-03 09:27:37'),
	(4, 'Hoa quả sấy', 'hoa-qua-say-4', '2020-08-03 16:15:15', '2020-08-08 08:29:01'),
	(5, 'Máy nông nghiệp', 'may-nong-nghiep-5', '2020-08-10 14:15:26', '2020-08-10 14:15:26'),
	(6, 'Sản phẩm Nông nghiệp', 'san-pham-nong-nghiep-6', '2020-08-10 15:02:45', '2020-08-10 15:02:45');
/*!40000 ALTER TABLE `theloai` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.type_post
CREATE TABLE IF NOT EXISTS `type_post` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `type_post_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.type_post: ~6 rows (approximately)
/*!40000 ALTER TABLE `type_post` DISABLE KEYS */;
INSERT INTO `type_post` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Organic Farming', '2020-08-01 02:48:52', '2020-08-01 02:50:52'),
	(2, 'Fresh Organic Mustard', '2020-08-01 02:49:09', '2020-08-01 02:51:11'),
	(3, 'Organic Green Bell Pepper', '2020-08-01 02:51:22', '2020-08-01 02:51:22'),
	(4, 'Permaculture', '2020-08-01 02:51:34', '2020-08-01 02:51:34'),
	(5, 'Precision Farming', '2020-08-01 02:51:49', '2020-08-01 02:51:49'),
	(6, 'Conservation Agriculture', '2020-08-01 02:52:08', '2020-08-01 02:52:08'),
	(7, 'ANH HARI', '2020-08-25 06:46:49', '2020-08-25 06:46:49');
/*!40000 ALTER TABLE `type_post` ENABLE KEYS */;

-- Dumping structure for table mylaravel_intern.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Avatar',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên đầy đủ',
  `slug` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Số điện thoại',
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Địa chỉ Email',
  `address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Địa chỉ',
  `gender` tinyint(4) DEFAULT NULL COMMENT 'Giới tính',
  `birthday` datetime DEFAULT '1912-01-01 00:00:00' COMMENT 'Ngày sinh',
  `info` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Thông tin thêm',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Mật khẩu đăng nhập',
  `level` tinyint(4) NOT NULL DEFAULT '2' COMMENT 'Cấp độ phân quyền: 1: Tài khoản hệ thống - 2: Tài khoản viết bài - 3: Tài khoản quản lý bài viết - 4: Tài khoản biên tập sản phẩm/dự án - 5: Tài khoản quản lý sản phẩm/dự án ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table mylaravel_intern.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `image`, `name`, `slug`, `phone`, `email`, `address`, `gender`, `birthday`, `info`, `password`, `level`, `created_at`, `updated_at`) VALUES
	(1, 'IMAGE-SLIDE-1596248481102399018_996080340851263_4444681276898649967_n.jpg', 'Quang Merce', 'quang-merce', '0969030421', 'dvq.dev@gmail.com', 'Nam Định', 1, '1998-07-26 21:30:30', '22 Year Old', '$2y$10$Kzh/AosW3boQzoXaHJiP8ehMdkBixQfa54HLsgDRbKEuLXWqY3EA6', 1, '2020-08-01 02:05:12', '2020-08-01 02:21:21'),
	(4, 'IMAGE-SLIDE-1596948955im-105325.jpg', 'Bill Gates', 'bill-gates-4', '094654676312', 'henryjohn.dev@gmail.com', 'THON TON THANH .dddd', 1, '2020-08-26 00:00:00', 'Bill Gates', '$2y$10$/ZACL7TAc1D48/jE85ATouHSIoUwK35J029FslBBSY5a2NJ6Tmg7y', 2, '2020-08-07 10:28:35', '2020-08-09 04:55:55'),
	(5, 'IMAGE-AUTHOR-1596948782100496736-steve-jobs-march-2011-getty.jpg', 'Steve Jobs', 'steve-jobs-5', '0245634789', 'stevejobs@gmail.com', 'New York', 1, '1964-08-09 00:00:00', 'steve jobs ss', '$2y$10$a435R8ZMQTJMHfrKbd7ykuw.SlLG5dcTE7uZMmU7BTgpP6NlQloAO', 3, '2020-08-09 04:53:02', '2020-08-09 04:53:02'),
	(6, 'IMAGE-AUTHOR-1596949117pham-nhat-vuong-giau-tu-dau-giau-nhu-the-nao-bytuong-com.jpg', 'Phạm Nhật Vượng', 'pham-nhat-vuong-6', '0956789368', 'phamnhatvuong@gmail.com', 'Hà Nội, Việt Nam', 1, '1967-08-04 00:00:00', 'Phạm Nhật Vượng là tỷ phú người Việt đầu tiên!', '$2y$10$V61mE/MSZQImKDTg6OufCubIRVx0z7zFK51dN3dk5zaxaTV4LUljO', 2, '2020-08-09 04:58:37', '2020-08-09 04:58:37');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
