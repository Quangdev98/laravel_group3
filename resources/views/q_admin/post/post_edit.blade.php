@extends('q_admin.master')
@section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Post Edit
            <small></small>
        </h1>
    </div>
    <form action="{{ route('ad.post-post-edit',['id'=>$post->id]) }}" method="POST" enctype="multipart/form-data">
        <div class="col-lg-9" style="padding-bottom:50px">
            @csrf
            <div class="form-group">
                <label>Tiêu đề</label>
                <input class="form-control" value="{{ $post->title }}" name="title" placeholder="Vui lòng nhập tiêu đề bài viết" />
                {!! $errors->has('title') ? '<div class="alert alert-danger">'.$errors->first('title').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Nội Dung tóm tắt</label>
                <textarea rows="4" class="form-control" value="" name="contentHot" placeholder="Vui lòng nhập tóm tắt bài viết">{{ $post->contentHot }}</textarea>
                {!! $errors->has('contentHot') ? '<div class="alert alert-danger">'.$errors->first('contentHot').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Nội Dung</label>
                <textarea type="text" value="{{ $post->content }}" class="content" name="content" class="form-control" rows="4">{{ $post->content }}</textarea>
                {!! $errors->has('content') ? '<div class="alert alert-danger">'.$errors->first('content').'</div>' : ''!!}
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label>Ngày Đăng</label>
                <input type="text" value="{{ $post->postDate }}" name="postDate" class="form-control" >
                {!! $errors->has('postDate') ? '<div class="alert alert-danger">'.$errors->first('postDate').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Thẻ liên quan</label>
                <input type="text" value="{{ $post->tag }}" name="tag" class="form-control" placeholder="cách nhau bằng dấu , ">
                {!! $errors->has('tag') ? '<div class="alert alert-danger">'.$errors->first('tag').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Danh mục bài viết</label>
                <select class="form-control" name="typePost_id">
                    @foreach ($type_post as $tp)
                        <option value="{{ $tp->id }}" {{ $post->typePost_id == $tp->id ? 'selected' : '' }}>{{ $tp->name }}</option>
                    @endforeach
                </select>
                {!! $errors->has('typePost_id') ? '<div class="alert alert-danger">'.$errors->first('typePost_id').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Tác giả</label>
                <select class="form-control" name="user_id">
                    @foreach ($user as $us)
                        <option value="{{ $us->id }}" {{ $post->user_id == $us->id ? 'selected' : '' }}>{{ $us->name }}</option>
                        {{-- expr --}}
                    @endforeach
                </select>
                {!! $errors->has('user_id') ? '<div class="alert alert-danger">'.$errors->first('user_id').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Ảnh Avatar</label>
                <input type="file" value="{{ $post->fileImagePost }}" name="fileImagePost" class="form-control">
                <div class="mt-5" style="text-align: center;">
                    <img src="uploads/posts/{{ $post->image?:'' }}" class="images" alt="" style="width:120px;">
                </div>
                {!! $errors->has('fileImagePost') ? '<div class="alert alert-danger">'.$errors->first('fileImagePost').'</div>' : ''!!}
            </div>
        </div>
        <div class="col-lg-12">
            <button type="submit" class="btn btn-success">Thêm mới</button>
        </div>
    <form>
@stop