@extends('q_admin.master')
@section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Danh sách bài viết
            <small></small>
        </h1>
        <div class="btn btn-danger block">
            <p><b>* Lưu ý:</b></p>
            <ul>
                <li>Danh mục quản lý bài viết(quyền Post-Management)</li>
                <li>Trạng thái: 
                Được hiển thị!<br>
                </li>
            </ul>
        </div>
    </div>
    <div class="tab-control">
        <div class="form-search" style="display: inline-block">
            <form action="{{ route('ad.post-list') }}" method="get">
                @csrf
                <div class="form-controls">
                    <label class="nameW">Tìm kiếm</label><br>
                    <input type="search" name="search" placeholder="tìm kiếm bài viết">
                    <button type="submit"><i class="fal fa-search"></i></button>
                </div>
            </form>
        </div>
        <div class="statusPro">
            <span class="statusName">Hiển thị: </span><a href="{{ route('ad.post-list') }}" title="">Tất cả</a>
        </div>
        <div class="wrap-form">
            <form action="{{ route('ad.post-list') }}" method="get">
                <div class="form-controls">
                    <label class="nameW">Danh mục</label><br>
                    <select name="danhmuc">
                        <option value="">--Chọn danh mục--</option>
                        @foreach ($danhmucs as $d)
                            <option value="{{ $d->id }}">{{ $d->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-controls">
                    <label class="nameW">Tác giả</label><br>
                    <select name="author">
                        <option value="">--Chọn tác giả--</option>
                        @foreach ($authors as $au)
                            <option value="{{ $au->id }}">{{ $au->name }}</option>
                        @endforeach
                    </select>
                </div>
{{--                <div class="form-controls">--}}
{{--                    <label class="nameW">Mức giảm giá</label><br>--}}
{{--                    <input type="number" placeholder="nhập mức giảm giá..." name="sale">--}}
{{--                </div>--}}
                <button type="submit">Thao tác</button>
            </form>
        </div>
    </div>
    <table class="table table-striped table-bordered table-hover {{ $user_admin->slug }}_3" id="dataTables-example">
        <thead>
            <tr align="center">
                <th>ID</th>
                <th>Ảnh thumnail</th>
                <th>Tên Bài Viết</th>
                <th>Thể Loại bài viết</th>
                <th class="center">Ngày Đăng</th>
                <th>Tác Giả</th>
                {{-- <th>Status</th> --}}
                <th>Hành Động</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($post as $po)
                {{-- expr --}}
            <tr class="odd gradeX" align="center">
                <td>{{ $po->id }}</td>
                <td><img src="uploads/posts/{{ $po->image }}" alt=""></td>
                <td>{{ $po->title }}</td>
                <td>{{ $po->type_post_name }}</td>
                <td class="">{{ \Carbon\Carbon::create($po->postDate)->toFormattedDateString() }}</td>
                <td>{{ $po->name_author }}</td>
                <td class="center treatment">
                        <a href="{{ Route('ad.post-edit',['id'=> $po->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{ route('ad.post-delete',['id'=>$po->id]) }}" class="btn-delete"><i class="fad fa-trash-alt fa-fw"></i></a>
                    </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $post->links() }}
@stop
