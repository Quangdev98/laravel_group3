 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Slide
            <small>{{ $info->id }}</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{{ route('ad.post-info-edit', ['id'=>$info->id]) }}" method="POST" >
            @csrf
            <div class="form-group">
                <label>Địa chỉ</label>
                <input type="text" class="form-control" name="address" value="{{ $info->address }}" />
                {!! $errors->has('address') ? '<div class="alert alert-danger">'.$errors->first('address').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Số điện thoại</label>
                <input class="form-control" name="phone" placeholder="Nhập Tiêu đề" value="{{ $info->phone }}" />
                {!! $errors->has('phone') ? '<div class="alert alert-danger">'.$errors->first('phone').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Email</label>
                <input name="email" value="{{ $info->email }}" class="form-control" type="email">
                {!! $errors->has('email') ? '<div class="alert alert-danger">'.$errors->first('email').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Vị trí</label>
                <select name="status" class="form-control">
                    <option value="1" {{ $info->status == 1 ? 'selected' : '' }}>Trụ sở chính</option>
                    <option value="2" {{ $info->status == 2 ? 'selected' : '' }}>Chi nhánh</option>
                </select>
                {!! $errors->has('status') ? '<div class="alert alert-danger">'.$errors->first('status').'</div>' : ''!!}
            </div>
            
            <button type="submit" class="btn btn-default">Sủa thông tin</button>
        <form>
    </div>
@stop