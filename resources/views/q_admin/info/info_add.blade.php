 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Thông tin
            <small>Thêm</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{{ route('ad.post-info-add') }}" method="POST" >
            @csrf
            <div class="form-group">
                <label>Địa chỉ</label>
                <input type="text" class="form-control" name="address" />
                {!! $errors->has('address') ? '<div class="alert alert-danger">'.$errors->first('address').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Số điện thoại</label>
                <input class="form-control" name="phone" placeholder="Nhập Tiêu đề" />
                {!! $errors->has('phone') ? '<div class="alert alert-danger">'.$errors->first('phone').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Email</label>
                <input name="email" class="form-control" type="email">
                {!! $errors->has('email') ? '<div class="alert alert-danger">'.$errors->first('email').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Vị trí</label>
                <select name="status" class="form-control">
                    {{-- <option value="1">Trụ sở chính</option> --}}
                    <option value="2" selected>Chi nhánh</option>
                </select>
                {!! $errors->has('status') ? '<div class="alert alert-danger">'.$errors->first('status').'</div>' : ''!!}
            </div>
            
            <button type="submit" class="btn btn-default">Thêm</button>
        <form>
    </div>
@stop