 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Slide
            <small>Danh Sách</small>
        </h1>
        <div class="btn btn-danger block">
            <p><b>* Lưu ý:</b></p>
            <ul>
                <li>Danh mục quản lý thông tin địa chỉ công ty</li>
                <li>Vị trí hiển thị<br>
                    - Trụ sở chính: hiển thị ở trang chủ và cuối trang. (mặc định chỉ 1 trụ sở chính) <br>
                    - Các chi nhánh: hiển thị ở trang liên hệ!. <br>
                    -Ở các vị trí có thông tin như địa chỉ, số điện thoại, email liên hệ và trang liên hệ
                </li>
            </ul>
        </div>
    </div>
    <!-- /.col-lg-12 -->
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr align="center">
                <th>ID</th>
                <th class="imageClass">Địa chỉ</th>
                <th class="titleClass">Số điện thoại</th>
                <th>Email</th>
                <th>Vị trí</th>
                <th>Sửa / Xóa</th>
               
            </tr>
        </thead>
        <tbody>
            @foreach ($info as $i)
                <tr class="odd gradeX" align="center">
                    <td>{{ $i ->id }}</td>
                    <td>{{ $i ->address }}</td>
                    <td>{{ $i ->phone }}</td>
                    <td>{{ $i ->email }}</td>
                    <td>{{ $i ->status == 1 ? 'Trụ sở Chính' : 'Chi nhánh' }}</td>
                    <td class="center treatment">
                        <a href="{{ Route('ad.info-edit',['id'=> $i->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{ route('ad.info-delete',['id'=>$i->id]) }}" class="btn-delete"><i class="fad fa-trash-alt fa-fw"></i></a>
                    </td>
                </tr>
            @endforeach
            
        </tbody>
    </table>
@stop
