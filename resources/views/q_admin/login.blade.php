<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Khóa Học Lập Trình Laravel Framework 5.x Tại Khoa Phạm">
    <meta name="author" content="">

    <title>Login</title>
    <base href="{{ asset('') }}">
    <!-- Bootstrap Core CSS -->
    <link href="q_admin/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="q_admin/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="q_admin/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="q_admin/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body class="bodyLogin">
    
    <div class="container">
        <div class="row">
            <div class="col-md-4 marginAuto">
                @if (session('thongbao'))
                    <div class="alert alert-success">
                        {{ session('thongbao') }}
                    </div>
                @endif
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">ĐĂNG NHẬP</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="{{ route('web.postLogin') }}" method="POST">
                            @csrf
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                    {!! $errors->has('email') ? '<p class="alert alert-danger pv-5">'.$errors->first('email').'</p>' : ''!!}
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                    {!! $errors->has('password') ? '<p class="alert alert-danger pv-5">'.$errors->first('password').'</p>' : ''!!}
                                </div>
                                <button type="submit" class="btn btn-lg btn-success btn-block colorLogout">Đăng nhập</button>
                            </fieldset>
                        </form>
                    </div>
                    @if (session('thongbao'))
                        <div class="alert alert-danger">
                            {{ session('thongbao') }}
                        </div>                            
                    @endif
                    <div class="returnHome">
                        <a href="{{ route('web.index') }}" title=""><i class="fal fa-angle-right"></i> Trang Chủ</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="q_admin/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="q_admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="q_admin/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="q_admin/dist/js/sb-admin-2.js"></script>

</body>

</html>
