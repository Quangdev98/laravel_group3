 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Slide
            <small>Danh Sách</small>
        </h1>
        <div class="btn btn-danger block">
            <p><b>* Lưu ý:</b></p>
            <ul>
                <li>Danh mục quản lý slide nổi bật ở trang chủ( vị trí nằm ở đầu trang chủ)</li>
                {{-- <li>Trạng thái Phân quyền: <br>
                1:Maintainer(Admin): Có quyền quản trị Admin có tất cả các quyền để quản lý admin <br>
                2:Producter(Quản lý sản phẩm): Có quyền quản trị trong phạm vi Thêm / Sửa / Xóa sản phẩm <br>
                3:Post-Management(quản lý bài viết): Có quyền quản trị trong phạm vi Thêm / Sửa / Xóa Bài viết <br>
                </li> --}}
            </ul>
        </div>
    </div>
    <!-- /.col-lg-12 -->
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr align="center">
                <th>ID</th>
                <th class="imageClass">Ảnh</th>
                <th class="titleClass">Tiêu đề</th>
                <th>Nội dung nổi bật</th>
                {{-- <th>Alt</th> --}}
                <th>Đường dẫn(link)</th>
                 <th>Sửa / Xóa</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($slide as $sli)
                <tr class="odd gradeX" align="center">
                    <td>{{ $sli ->id }}</td>
                    <td><img src="uploads/slides/{{ $sli->image }}" alt=""></td>
                    <td>{{ $sli ->title }}</td>
                    <td>{{ $sli ->contentHot }}</td>
                    {{-- <td>{{ $sli ->alt }}</td> --}}
                    <td>{{ $sli ->link }}</td>
                    <td class="center treatment">
                        <a href="{{ Route('ad.slide-edit',['id'=> $sli->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{ route('ad.slide-delete',['id'=>$sli->id]) }}" class="btn-delete"><i class="fad fa-trash-alt fa-fw"></i></a>
                    </td>
                </tr>
            @endforeach
            
        </tbody>
    </table>
    {{ $slide->links() }}
@stop
