 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Slide
            <small>Thêm</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{{ route('ad.post-slide-add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Ảnh Background</label>
                <input type="file" class="form-control" name="fileImage" />
                {!! $errors->has('fileImage') ? '<div class="alert alert-danger">'.$errors->first('fileImage').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Tiêu đề</label>
                <input class="form-control" name="title" placeholder="Nhập Tiêu đề" />
                {!! $errors->has('title') ? '<div class="alert alert-danger">'.$errors->first('title').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Nội dung nổi bật</label>
                <textarea name="contentHot" rows="4" class="form-control"></textarea>
                {!! $errors->has('contentHot') ? '<div class="alert alert-danger">'.$errors->first('contentHot').'</div>' : ''!!}
            </div>
            {{-- <div class="form-group">
                <label>Alt</label>
                <input type="text" class="form-control" name="alt" placeholder="Nhập Alt" />
            </div> --}}
            <div class="form-group">
                <label>Đường dẫn(link)</label>
                <input type="text" class="form-control" name="link" placeholder="Nhập đường dẫn" />
                {!! $errors->has('link') ? '<div class="alert alert-danger">'.$errors->first('link').'</div>' : ''!!}
            </div>
            
            <button type="submit" class="btn btn-default">Thêm Slide</button>
        <form>
    </div>
@stop