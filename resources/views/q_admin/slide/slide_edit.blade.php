 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Slide
            <small>{{ $slide->title }}</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{{ route('ad.post-slide-edit',['id'=>$slide->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Ảnh Background</label>
                <input type="file" class="form-control" name="fileImage" value="" />
                <div class="mt-5">
                    <img src="uploads/slides/{{ $slide->image?:'' }}" alt="" style="width:120px;">
                </div>
            </div>
            <div class="form-group">
                <label>Tiêu đề</label>
                <input class="form-control" name="title" placeholder="Nhập Tiêu đề" value="{{ $slide->title }}" />
            </div>
            <div class="form-group">
                <label>Nội dung nổi bật</label>
                <textarea name="contentHot" rows="4" class="form-control">{{ $slide->contentHot }}</textarea>
            </div>
            {{-- <div class="form-group">
                <label>Alt</label>
                <input type="text" class="form-control" name="alt" placeholder="Nhập Alt" />
            </div> --}}
            <div class="form-group">
                <label>Đường dẫn(link)</label>
                <input type="text" class="form-control" name="link" placeholder="Nhập đường dẫn" value="{{ $slide->link }}" />
            </div>
            
            <button type="submit" class="btn btn-default">Sửa Slide</button>
        <form>
    </div>
@stop