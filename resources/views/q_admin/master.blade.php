{{--copyright by Quang Dev--}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Quản trị viên</title>
    <base href="{{ asset('') }}">
    <link type="text/css" href="{{ url('q_admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{ url('q_admin/bower_components/metisMenu/dist/metisMenu.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{ url('q_admin/bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link type="text/css" href="{{ url('q_admin/dist/css/sb-admin-2.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
    <style type="text/css" media="screen">
        .{{ $user_admin->slug }}-1 .treatment a{
            cursor: pointer;
        }
        .{{ $user_admin->slug }}-2 .{{ $user_admin->slug }}_2 .treatment a,
        .{{ $user_admin->slug }}-3 .{{ $user_admin->slug }}_3 .treatment a{
            cursor: pointer;
        }
    </style>
</head>
<body class="{{ $user_admin->slug }}-{{ $user_admin->level }}">
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar_top navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><i class="fab fa-apple"></i>&nbsp; Admin</a>
            </div>
            <!-- /.navbar-header -->
            <ul class="navUser nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="DateTime">
                    <span id="titmeDate">
                        <span id="getWeek"></span>,
                        <span id="getDay"></span>
                        <span id="getMonth"></span>
                        <span id="getYear"></span>
                    </span>&nbsp;||
                    <span class="" id="time_now"></span>
                </li>
                <li class="dropdown">
                    @if (isset($user_admin))
                    <a class="uuuuser dropdown-toggle" data-toggle="dropdown" href="#">
                        <div class="user">
                            <span class="interFaceUser">
                                <img src="uploads/avatars/{{ $user_admin->image }}" alt="">
                            </span>
                            <span class="nameUser">{{ $user_admin->name }}</span>
                        </div> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="far fa-user fa-fw"></i> {{ $user_admin->name }}</a>
                        </li>
                        <li><a href="#"><i class="fal fa-cogs fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ Route('web.logout') }}"><i class="fal fa-sign-out-alt fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    @endif
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="nav_control_left navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li class="{{ Route::current()->getName() == 'ad.admin' ? 'active': '' }}">
                            <a href="{{ Route('ad.admin') }}"><span class="box_icon"><i class="fad fa-home fa-fw"></i></span><span class="name_titleAdmin"> Trang Chủ</span></a>
                        </li>
                        <li >
                            <a href="#"><span class="box_icon"><i class="fad fa-cubes fa-fw"></i></span><span class="name_titleAdmin">Quản Lý Thành viên</span><span class="fal arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ Route('ad.author-list') }}">Danh Sách</a>
                                </li>
                                <li>
                                    <a href="{{ Route('ad.author-add') }}">Thêm Thành viên</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="#"><span class="box_icon"><i class="fad fa-chart-line fa-fw"></i></span><span class="name_titleAdmin">Quản Lý Danh Mục Sản Phẩm</span><span class="fal arrow"></span></a>
                            <ul class="nav nav-second-level">

                                <li>
                                    <a href="{{ Route('ad.category-list') }}">Danh Sách</a>
                                </li>
                                <li>
                                    <a href="{{ Route('ad.category-add') }}">Thêm Danh Mục</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span class="box_icon"><i class="fad fa-cubes fa-fw"></i></span><span class="name_titleAdmin">Quản Lý Sản Phẩm</span><span class="fal arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ Route('ad.product-list') }}">Danh Sách Sản Phẩm<span class="numberTable"><span>{{ $countProduct }}</span></span></a>
                                </li>
                                <li>
                                    <a href="{{ Route('ad.product-add') }}">Thêm Sản Phẩm</a>
                                </li>
                                <li>
                                    <a href="{{ Route('ad.product-delete-list') }}">Sản Phẩm đã xóa <span class="numberTable"><span>{{ $countProductDelete }}</span></span></a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="#"><span class="box_icon"><i class="fad fa-blog fa-fw"></i></span><span class="name_titleAdmin">Quản Lý Thể loại bài viết</span><span class="fal arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ Route('ad.typePost-list') }}">Danh Sách Thể loại</a>
                                </li>
                                <li>
                                    <a href="{{ Route('ad.typePost-add') }}">Thêm Thể loại</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span class="box_icon"><i class="fad fa-blog fa-fw"></i></span><span class="name_titleAdmin">Quản Lý Bài Viết</span><span class="fal arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ Route('ad.post-list') }}">Danh Sách Bài Viết</a>
                                </li>
                                <li>
                                    <a href="{{ Route('ad.post-add') }}">Thêm Bài Viết</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><span class="box_icon"><i class="fad fa-sliders-h-square"></i></span><span class="name_titleAdmin">Slide</span><span class="fal arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ Route('ad.slide-list') }}">Danh Sách Slide</a>
                                </li>
                                <li>
                                    <a href="{{ Route('ad.slide-add') }}">Thêm Slide</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="{{ Route('ad.order') }}"><span class="box_icon"><i class="fad fa-bags-shopping"></i></span><span class="name_titleAdmin">Đơn hàng</span><span class="fal arrow"></span></a>
                        </li>
                        <li>
                            <a href="#"><span class="box_icon"><i class="fad fa-info"></i></span><span class="name_titleAdmin">Thông tin liên hệ</span><span class="fal arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ Route('ad.info-list') }}">Danh Sách</a>
                                </li>
                                <li>
                                    <a href="{{ Route('ad.info-add') }}">Thêm thông tin</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="{{ Route('ad.phanhoi') }}">
                                <span class="box_icon"><i class="fas fa-comments-dollar"></i></span>
                                <span class="name_titleAdmin">Phản hồi của khách hàng</span><span class="fal arrow"></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ Route('ad.ykien') }}">
                                <span class="box_icon"><i class="fad fa-user-md-chat"></i></span>
                                <span class="name_titleAdmin">Ý kiến chuyên gia</span><span class="fal arrow"></span>
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">


                 <!-- master_content -->
                 @yield('content')
                 <!-- end master_content -->


             </div>
             <!-- /.row -->
         </div>
         <!-- /.container-fluid -->
     </div>
     <!-- /#page-wrapper -->

 </div>
 <script src="{{ url('q_admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
 <script src="{{ url('q_admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
 <script src="{{ url('q_admin/bower_components/metisMenu/dist/metisMenu.min.js')}}"></script>
 <script src="{{ url('q_admin/dist/js/sb-admin-2.js')}}"></script>
 <script src="{{ url('q_admin/dist/js/getDate.js') }}"></script>
 <script src="{{ url('q_admin/dist/js/fix_tenkhongdau.js') }}"></script>
<script src="{{ url('q_admin/dist/js/tyniMCE.min.js') }}" referrerpolicy="origin"></script>
<script src="{{ url('q_admin/js/chart.min.js') }}"></script>
<script src="{{ url('q_admin/js/chart.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
@yield('data')

<script>
  tinymce.init({
    selector: '.content',
        // selector: ,
        height: 400,
        plugins: [
        'advlist autolink link image lists charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
        'table emoticons template paste help'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
        menu: {
          favs: {title: 'My Favorites', items: 'code visualaid | searchreplace | emoticons'}
      },
      menubar: 'favs file edit view insert format tools table help',
      content_css: 'css/content.css',

  });
</script>
    <script>
        $('.btn-delete').on('click',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            // console.log(url);
            swal({
                    title: "Bạn có muốn xóa?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Có",
                    cancelButtonText: "Quay lại",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        window.location.href = url;
                        swal("Đã xóa", "", "success");
                    } else {
                        swal("Hủy thành công!", "", "error");
                    }
                });
        });
    </script>
</body>

</html>
