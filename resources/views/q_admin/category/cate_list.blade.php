@extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Danh Mục
            <small>Danh sách</small>
        </h1>
        <div class="btn btn-danger block">
            <p><b>* Lưu ý:</b></p>
            <ul>
                {{-- <li>Phần mềm quản trị thành viên Admin.</li> --}}
                <li>Danh sách thể loại sản phẩm<br>
                {{-- 1:Maintainer(Admin): Có quyền quản trị Admin có tất cả các quyền để quản lý admin <br>
                2:Producter(Quản lý sản phẩm): Có quyền quản trị trong phạm vi Thêm / Sửa / Xóa sản phẩm <br>
                3:Post-Management(quản lý bài viết): Có quyền quản trị trong phạm vi Thêm / Sửa / Xóa Bài viết <br> --}}
                </li>
            </ul>
        </div>
    </div>
    <!-- /.col-lg-12 -->
  {{--   @if (session('thongbao'))
        <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>
    @endif --}}
    <table class="table table-striped table-bordered table-hover {{ $user_admin->slug }}_2" id="dataTables-example">
        <thead>
            <tr align="center">
                <th>ID</th>
                <th>Tên</th>
                <th>Slug</th>
                {{-- <th>Category Parent</th> --}}
                <th>Số lượng sản phẩm</th>
                <th>Sửa / Xóa</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($countProducts as $tl)
                <tr class="odd gradeX" align="center">
                    <td>{{ $tl ->id }}</td>
                    <td>{{ $tl ->name }}</td>
                    <td>{{ $tl ->slug }}</td>
                    {{-- <td>None</td> --}}
                    <td>{{ $tl->count_product }}</td>
                    <td class="center treatment">
                        <a href="{{ route('ad.category-edit',['id'=> $tl->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{ route('ad.category-delete',['id'=>$tl->id]) }}" class="btn-delete"><i class="fad fa-trash-alt fa-fw"></i></a>
                    </td>
                </tr>
             @endforeach
        </tbody>
    </table>
    {{ $countProducts->links() }}
@stop
