 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Category
            <small>Add</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{{ route('ad.post-category-add') }}" method="POST">
            @csrf
           {{--  <div class="form-group">
                <label>Category Parent</label>
                <select class="form-control">
                    <option value="0">Please Choose Category</option>
                    <option value="">Tin Tức</option>
                </select>
            </div> --}}
            <div class="form-group">
                <label>Category Name</label>
                <input class="form-control" type="text" name="theloaiName" placeholder="Điền tên thể loại" />
            </div>
            <button type="submit" class="btn btn-default">Thêm Thể Loại</button>
            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
        <form>
    </div>
@stop
