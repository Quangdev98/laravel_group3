@extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Danh Mục
            <small>Sửa</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
        @if (count($errors) >0)
           <div class="alert alert-danger">
               @foreach ($errors->all() as $err)
                   {{$err}}
               @endforeach
           </div>
        @endif
        @if (session('thongbao'))
            <div class="alert alert-success">
                {{ session('thongbao') }}
            </div>
        @endif
        <form action="{{ route('ad.post-category-edit', ['id'=>$theloai->id]) }}" method="POST">
            @csrf
            <div class="form-group">
                <label>Tên Danh Mục</label>
                <input class="form-control" name="cateName" value="{{ $theloai->name }}" placeholder="Nhập tên thể loại" />
            </div>
            <button type="submit" class="btn btn-default">Sửa</button>
            <button type="reset" class="btn btn-default">Reset</button>
        <form>
    </div>
@stop