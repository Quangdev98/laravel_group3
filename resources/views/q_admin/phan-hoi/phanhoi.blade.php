@extends('q_admin.master')
@section('content')
    @if (session('thongbao'))
        <div class="alert bg-danger" role="alert">
            <svg class="glyph stroked checkmark">
                <use xlink:href="#stroked-checkmark"></use>
            </svg>{{session('thongbao')}}<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
        </div>
    @endif
    <div class="col-lg-12">
        <h1 class="page-header">Phản hồi của khách hàng
            <small>Danh sách phản hồi</small>
        </h1>
    </div>

    <!-- /.col-lg-12 -->
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
        <tr align="center">
            <th class="idPro">ID</th>
            <th class="titleClass">Tên Khách Hàng</th>
            <th>Email</th>
            <th>Nội dung</th>
            <th>Thao tác</th>
{{--            <th>  </th>--}}
        </tr>
        </thead>
        <tbody>
        @foreach ($phanhoi as $row)
            <tr class="odd gradeX" align="center">
                <td>{{$row->id}}</td>
                <td>{{$row->name}}</td>
                <td>{{$row->email}}</td>
                <td>{{$row->message}}</td>
                <td> <a href="{{ Route('ad.phanhoi-delete',['id'=>$row->id]) }}" class="btn-delete">Xóa</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $phanhoi->links() }}
@stop
@section('data')

@stop

