@extends('q_admin.master')
@section('content')
@if (isset($user_admin))
	{{-- expr --}}
    <div class="col-lg-12">
        <h1 class="page-header">Trang Chủ
            <small></small>
        </h1>
{{--        <h4>Chào! {{ $user_admin->name }}</h4>--}}
    @if(session('thongbao'))
            <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">{{session('thongbao')}}</h4>
                <p></p>
                <p class="mb-0"></p>
            </div>
        @endif
    </div>
    <div class="wrap_admin">
        <h3>Tổng có {{$order}} Đơn hàng</h3>
        <div class="large"></div>

        </div>
        <div class="large"></div>
        <a href="{{ Route('ad.order') }}"><button type="button" class="btn btn-success">Còn {{$order_w}} đơn hàng chưa xử lý</button></a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Biểu đồ doanh thu</div>
                <div class="panel-body">
                    <div class="canvas-wrapper">
                        <canvas class="main-chart" id="line-chart" height="200" width="600"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@stop

@section('data')
<script>
    	var lineChartData = {
			labels : [
				@foreach($monthjs as $month)
				"{{$month}}",
				@endforeach],
			datasets : [

				{
					label: "My Second dataset",
					fillColor : "rgba(48, 164, 255, 0.2)",
					strokeColor : "rgba(48, 164, 255, 1)",
					pointColor : "rgba(48, 164, 255, 1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(48, 164, 255, 1)",
					data : [@foreach($numberjs as $number)
						{{$number}},
						@endforeach]
				}
			]

		}






window.onload = function(){
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
		responsive: true
	});
	var chart2 = document.getElementById("bar-chart").getContext("2d");
	window.myBar = new Chart(chart2).Bar(barChartData, {
		responsive : true
	});
	var chart3 = document.getElementById("doughnut-chart").getContext("2d");
	window.myDoughnut = new Chart(chart3).Doughnut(doughnutData, {responsive : true
	});
	var chart4 = document.getElementById("pie-chart").getContext("2d");
	window.myPie = new Chart(chart4).Pie(pieData, {responsive : true
	});

};
</script>
@endsection


