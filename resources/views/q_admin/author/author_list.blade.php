@extends('q_admin.master')
@section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Thành viên ban quản trị
            <small>Admin, Producter, Post-Management</small>
        </h1>
        <div class="btn btn-danger block">
            <p><b>* Lưu ý:</b></p>
            <ul>
                <li>Phần mềm quản trị thành viên Admin.</li>
                <li>Trạng thái Phân quyền: <br>
                1:Maintainer(Admin): Có quyền quản trị Admin có tất cả các quyền để quản lý admin <br>
                2:Producter(Quản lý sản phẩm): Có quyền quản trị trong phạm vi Thêm / Sửa / Xóa sản phẩm <br>
                3:Post-Management(quản lý bài viết): Có quyền quản trị trong phạm vi Thêm / Sửa / Xóa Bài viết <br>
                </li>
            </ul>
        </div>
    </div>
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr align="center">
                <th>ID</th>
                <th>Ảnh</th>
                <th>Tên</th>
                <th>Số Điện Thoại</th>
                <th>Email</th>
                <th>Thông tin</th>>
                {{-- <th>Mật khẩu</th> --}}
                <th>Phân Quyền</th>>
                <th>Hành Động</th>
            </tr>
        </thead>
        <tbody class="fix">
            @foreach ($users as $au)   
                <tr class="odd" align="center">
                    <td>{{ $au ->id }}</td>
                    <td><img src="uploads/avatars/{{ $au ->image }}" alt=""></td>
                    <td>{{ $au ->name }}</td>
                    <td>{{ $au ->phone }}</td>
                    <td>{{ $au ->email }}</td>
                    <td>{{ $au ->info }}</td>
                    {{-- <td>{{ $au ->password }}</td> --}}
                    <td>
                        {{ $au ->level == 1 ? 'Maintainer' : '' }}
                        {{ $au ->level == 2 ? 'Producter' : '' }}
                        {{ $au ->level == 3 ? 'Post-Management' : '' }}
                    </td>
                    <td class="treatment center">
                        <a href="{{ Route('ad.author-edit',['id'=> $au->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{ route('ad.author-delete',['id'=>$au->id]) }}" class="btn-delete"><i class="fad fa-trash-alt fa-fw"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop
