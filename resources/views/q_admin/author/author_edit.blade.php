@extends('q_admin.master')
@section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Author Edit
            <small></small>
        </h1>
    </div>
    <form action="{{ route('ad.post-author-edit',['id'=>$users->id]) }}" method="POST" enctype="multipart/form-data">
        @if (count($errors) >0)
           <div class="alert alert-danger">
               @foreach ($errors->all() as $err)
                   {{$err}}
               @endforeach
           </div>
        @endif
        @if (session('thongbao'))
            <div class="alert alert-success">
                {{ session('thongbao') }}
            </div>
        @endif
        <div class="col-lg-7" style="padding-bottom:120px">
            @csrf
            <div class="form-group">
                <label>Ảnh avatar</label>
                <input type="file" class="form-control" name="image" value="" />
                <div class="mt-5">
                    <img src="uploads/avatars/{{ $users->image?:'' }}" alt="" style="width:120px;">
                </div>
            </div>
            <div class="form-group">
                <label>Tên tác giả</label>
                <input class="form-control" name="name" value="{{ $users->name }}" placeholder="Nhập tên tác giả" />
            </div>
            <div class="form-group">
                <label>Số Điện Thoại</label>
                <input type="text" class="form-control" name="phone" value="{{ $users->phone }}" placeholder="Nhập số điện thoại" />
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email" value="{{ $users->email }}" placeholder="Nhập Email" />
            </div>
           {{--  <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" value="{{ $users->password }}" placeholder="Nhập mật khẩu" />
            </div> --}}
            <div class="form-group">
                <label>Địa chỉ</label>
                <input type="text" class="form-control" name="address" value="{{ $users->address }}" placeholder="Nhập mật khẩu" />
            </div>
            <div class="form-group">
                <label>Info more</label>
                <textarea name="info" class="form-control" rows="5">{{ $users->info }}</textarea>
            </div>
        </div>
        <div class="col-lg-5">
            {{-- <div class="form-group">
                <label>Ngày Sinh</label>
                <input type="text" name="birthday" class="form-control" value="{{ old('birthday')?old('birthday'):date_time_now_type_1() }}" placeholder="Ngày sinh theo định dạng: 1998/07/26">
            </div> --}}
            <div class="form-group">
                <label>Giới tính</label>
                <select name="gender" class="form-control" value="{{ $users->gender }}">
                    <option value="1"  {{ $users->gender == 1 ? 'selected' : '' }}>Nam</option>
                    <option value="2"  {{ $users->gender == 2 ? 'selected' : '' }}>Nữ</option>
                </select>
            </div>
            <div class="form-group">
                <label>Phân Quyền</label>
                <select name="level" class="form-control" value="{{ $users->level }}">
                    <option value="1" {{ $users->level == 1 ? 'selected' : '' }}>Tài khoản hệ thống</option>
                    <option value="2" {{ $users->level == 2 ? 'selected' : '' }}>Tài khoản quản lý sản phẩm</option>
                    <option value="3" {{ $users->level == 3 ? 'selected' : '' }}>Tài khoản quản lý bài viết</option>
                   {{--  <option value="4" {{ $users->level == 4 ? 'selected' : '' }}>Tài khoản biên tập sản phẩm/dự án</option>
                    <option value="5" {{ $users->level == 5 ? 'selected' : '' }}>Tài khoản quản lý sản phẩm/dự án</option> --}}
                    option
                </select>
            </div>
            
        </div>
        <div class="col-lg-12 margin_bottom">
            <button type="submit" class="btn btn-success">Sửa Tác Giả</button>
            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
        </div>
    <form>
@stop
