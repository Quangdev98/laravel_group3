@extends('q_admin.master')
@section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Thêm Tác Giả
            <small></small>
        </h1>
    </div>
    <form action="{{ route('ad.post-author-add') }}" method="POST" enctype="multipart/form-data">
        <div class="col-lg-7" style="padding-bottom:120px">
            @csrf
            <div class="form-group">
                <label>Ảnh avatar</label>
                <input class="form-control" type="file" name="image" placeholder="Nhập tên tác giả" />
                {!! $errors->has('image') ? '<div class="alert alert-danger">'.$errors->first('image').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Tên tác giả</label>
                <input class="form-control" name="name" placeholder="Nhập tên tác giả" />
                {!! $errors->has('name') ? '<div class="alert alert-danger">'.$errors->first('name').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Số Điện Thoại</label>
                <input type="text" class="form-control" name="phone" placeholder="Nhập số điện thoại" />
                {!! $errors->has('phone') ? '<div class="alert alert-danger">'.$errors->first('phone').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email" placeholder="Nhập Email" />
                {!! $errors->has('email') ? '<div class="alert alert-danger">'.$errors->first('email').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="text" class="form-control" name="password" placeholder="Nhập mật khẩu" />
                {!! $errors->has('password') ? '<div class="alert alert-danger">'.$errors->first('password').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Địa chỉ</label>
                <input type="text" class="form-control" name="address" placeholder="Nhập mật khẩu" />
                {!! $errors->has('address') ? '<div class="alert alert-danger">'.$errors->first('address').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Info more</label>
                <textarea name="info" class="form-control" rows="5"></textarea>
                {{-- {!! $errors->has('fileImage') ? '<div class="alert alert-danger">'.$errors->first('fileImage').'</div>' : ''!!} --}}
            </div>
        </div>
        <div class="col-lg-5">
            <div class="form-group">
                <label>Ngày Sinh</label>
                <input type="text" name="birthday" class="form-control" value="{{ old('birthday')?old('birthday'):date_time_now_type_1() }}" placeholder="Ngày sinh theo định dạng: 1998/07/26">
            </div>
            <div class="form-group">
                <label>Giới tính</label>
                <select name="gender" class="form-control">
                    <option value="1">Nam</option>
                    <option value="1" selected>Nữ</option>
                    option
                </select>
            </div>
            <div class="form-group">
                <label>Phân Quyền</label>
                <select name="level" class="form-control">
                    <option value="1">Tài khoản hệ thống</option>
                    <option value="2" selected>Tài khoản quản lý sản phẩm</option>
                    <option value="3">Tài khoản quản lý bài viết</option>
                    {{-- <option value="4">Tài khoản biên tập sản phẩm/dự án</option>
                    <option value="5">Tài khoản quản lý sản phẩm/dự án</option> --}}
                    option
                </select>
            </div>
            
        </div>
        <div class="col-lg-12 margin_bottom">
            <button type="submit" class="btn btn-success">Thêm Tác Giả</button>
            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
        </div>
    <form>
@stop
