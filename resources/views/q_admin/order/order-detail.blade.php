@extends('q_admin.master')
@section('content')
    <div class="col-12aa">
        <div class="col-lg-12">
            <h1 class="page-header">Chi tiết đơn hàng
                <small>số {{ $orderView->id }}</small>
            </h1>
        </div>

    </div>
    <div class="wrapper-orderView">
        <div class="row">
            <div class="col-lg-9 marginauto">
                <div class="wrapdiv">
                    <div class="nameCustomer divTitle status"><span class="nameTitle">Trạng thái: </span>{{ $orderView->status == 0 ? 'Chưa xử lý' : 'Đã xử lý' }}</div>
                    <div class="nameCustomer divTitle"><span class="nameTitle">Tên khách hàng: </span>{{ $orderView->name }}</div>
                    <div class="emailCustomer divTitle"><span class="nameTitle">Email: </span>{{ $orderView->email }}</div>
                    <div class="phoneCustomer divTitle"><span class="nameTitle">Số Điện thoại: </span>{{ $orderView->phone }}</div>
                    <div class="addCustomer divTitle"><span class="nameTitle">Địa chỉ: </span>{{ $orderView->address }}</div>
                    <div class="addCustomer divTitle"><span class="nameTitle">Ngày đặt hàng: </span>{{ $orderView->created_at }}</div>
                    <div class="totalCustomer divTitle"><span class="nameTitle">Tổng tiền:</span> <span class="total">{{ number_format($orderView->total) }} VND</span></div>
                    <div>
                        <table class="table table-striped table-bordered table-hover" style="margin-top: 30px;">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th class="imageClass">Ảnh</th>
                                <th>Tên sản phẩm</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($order as $o)
                                    <tr class="odd gradeX" align="center">
                                        <td>{{ $o->id }}</td>
                                        <td><img src="/uploads/products/{{ $o->image }}" alt=""></td>
                                        <td>{{ $o->name }}</td>
                                        <td>{{ $o->price }}</td>
                                        <td>{{ $o->quantity }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
