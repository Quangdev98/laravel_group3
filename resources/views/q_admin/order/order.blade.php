 @extends('q_admin.master')
 @section('content')
 @if (session('thongbao'))
 <div class="alert bg-danger" role="alert">
     <svg class="glyph stroked checkmark">
         <use xlink:href="#stroked-checkmark"></use>
     </svg>{{session('thongbao')}}<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
 </div>
 @endif
    <div class="col-lg-12">
        <h1 class="page-header">Đơn hàng
            <small>Chưa được xử lý</small>
        </h1>
    </div>
    <a href="{{ Route('ad.order-process') }}"><button type="button" class="btn btn-primary" style="margin:10px 0 20px">Đơn hàng đã xử lý</button></a>
    <!-- /.col-lg-12 -->
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr align="center">
                <th>Xem</th>
                <th>ID</th>
                <th class="titleClass">Tên Khách Hàng</th>
                <th>Số Điện Thoại</th>
                <th>Địa chỉ</th>
                <th>Thời gian</th>
                <th>Thao tác</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($customer as $row)
                <tr class="odd gradeX" align="center">
                    <td>
                        <a href="{{ Route('ad.order-view',['id'=>$row->id]) }}"><i class="far fa-eye fa-fw"></i></a>
                    </td>
                    <td>{{$row->id}}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->phone}}</td>
                    <td>{{$row->address}}</td>
                    <td>{{$row->updated_at}}</td>
                    <td class="center treatment">
                        <a href="{{ Route('ad.order-active',['id'=>$row->id]) }}"><button type="button" class="btn btn-success">Xử lý</button></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
     {{ $customer->links() }}
@stop

