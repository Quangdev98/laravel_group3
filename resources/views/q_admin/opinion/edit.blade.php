@extends('q_admin.master')
@section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Ý Kiến chuyên gia
            <small>Sửa</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{{ route('ad.post-opinion-edit',['id'=>$opinion->id]) }}" method="POST" enctype='multipart/form-data'>
            @csrf
            <div class="form-group">
                <label>Ảnh</label>
                <input value="" type="file" name="images_" class="form-control"  />
                <div class="mt-5" >
                    <img src="uploads/opinion/{{ $opinion->image?:'' }}" class="images" alt="" style="width:100px;margin:10px 0; text-align: left;">
                </div>
                {!! $errors->has('images_') ? '<div class="alert alert-danger">'.$errors->first('images_').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Tên</label>
                <input value="{{ $opinion->name }}" class="form-control" name="name" placeholder="Nhập tên chuyên gia" />
                {!! $errors->has('name') ? '<div class="alert alert-danger">'.$errors->first('name').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Lĩnh vực</label>
                <input value="{{ $opinion->cate }}" name="cate" class="form-control" >
                {!! $errors->has('cate') ? '<div class="alert alert-danger">'.$errors->first('cate').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Nội dung</label>
                <textarea name="content" class="form-control" rows="3">{{ $opinion->content }}</textarea>
                {!! $errors->has('content') ? '<div class="alert alert-danger">'.$errors->first('content').'</div>' : ''!!}
            </div>

            <button type="submit" class="btn btn-default">Sửa</button>
            <form>
    </div>
@stop
