@extends('q_admin.master')
@section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Ý Kiến chuyên gia
            <small>Danh Sách</small>
        </h1>
        <div class="btn btn-danger block">
            <p><b>* Lưu ý:</b></p>
            <ul>
                <li>Danh mục ý kiến chuyên gia</li>

            </ul>
        </div>
    </div>
    <div class="form-add-chucnang">
        <ul>
            <li><a href="{{ Route('ad.opinion-add') }}">Thêm</a></li>
        </ul>
    </div>
    <!-- /.col-lg-12 -->
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
        <tr align="center">
            <th>ID</th>
            <th class="imageClass">Ảnh</th>
            <th class="titleClass">Tên</th>
            <th>Lĩnh vực</th>
            <th>Nội dung</th>
            <th>Sửa / Xóa</th>

        </tr>
        </thead>
        <tbody>
        @foreach ($opinion as $o)
            <tr class="odd gradeX" align="center">
                <td>{{ $o ->id }}</td>
                <td><img src="/uploads/opinion/{{ $o ->image }}" alt=""></td>
                <td>{{ $o ->name }}</td>
                <td>{{ $o ->cate }}</td>
                <td>{{ $o ->content }}</td>
                <td class="center treatment">
                    <a href="{{ Route('ad.opinion-edit',['id'=> $o->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
                    <a href="{{ route('ad.opinion-delete',['id'=>$o->id]) }}" class="btn-delete"><i class="fad fa-trash-alt fa-fw"></i></a>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop
