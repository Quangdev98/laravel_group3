@extends('q_admin.master')
@section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Ý Kiến chuyên gia
            <small>Thêm</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{{ route('ad.post-opinion-add') }}" method="POST" enctype='multipart/form-data'>
            @csrf
            <div class="form-group">
                <label>Ảnh</label>
                <input type="file" name="images_" class="form-control"  />
                {!! $errors->has('images_') ? '<div class="alert alert-danger">'.$errors->first('images_').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Tên</label>
                <input class="form-control" name="name" placeholder="Nhập tên chuyên gia" />
                {!! $errors->has('name') ? '<div class="alert alert-danger">'.$errors->first('name').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Lĩnh vực</label>
                <input name="cate" class="form-control" >
                {!! $errors->has('cate') ? '<div class="alert alert-danger">'.$errors->first('cate').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Nội dung</label>
                <textarea name="content" class="form-control" rows="3"></textarea>
                {!! $errors->has('content') ? '<div class="alert alert-danger">'.$errors->first('content').'</div>' : ''!!}
            </div>

            <button type="submit" class="btn btn-default">Thêm</button>
            <form>
    </div>
@stop
