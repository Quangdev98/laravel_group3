 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Sản Phẩm
            <small>Đã xóa</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <table class="table table-striped table-bordered table-hover {{ $user_admin->slug }}_2" id="dataTables-example">
        <thead>
            <tr align="center">
                <th>ID</th>
                <th class="imageClass">Ảnh</th>
                <th class="titleClass">Tên sản phẩm</th>
                <th>Nội dung tiêu đề</th>
                <th>Giá</th>
                {{-- <th>Date</th> --}}
                <th>Giảm giá</th>
                <th>Trạng thái</th>
                <th class="categorysClass">Danh Mục</th>
                <th>Sửa / Xóa</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($productDeleteList as $pro)
                <tr class="odd gradeX" align="center">
                    <td>{{ $pro->id }}</td>
                    <td><img src="uploads/products/{{ $pro->image }}" alt=""></td>
                    <td>{{ $pro->name }}</td>
                    <td>{{ $pro->contentHot }}</td>
                    <td>{{ number_format($pro->price, 0,'',',') }} VND/1KG</td>
                    <td>{{ $pro->sale }}%</td>
                    <td>
                        {{-- {{ $pro->status }} --}}
                       {{--  {{ $pro->status == 1 ? 'Bản nháp' : '' }}
                        {{ $pro->status == 2 ? 'Chờ duyệt' : '' }}
                        {{ $pro->status == 3 ? 'Đã xuất bản' : '' }} --}}
                        {{ $pro->status == 4 ? 'Đã xóa' : '' }}
                    </td>
                    <td>{{ $pro->nameCate }}</td>
                    <td class="center treatment">
                        <a href="{{ Route('ad.product-delete-edit',['id'=> $pro->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{ route('ad.product-delete',['id'=>$pro->id]) }}" class="btn-delete"><i class="fad fa-trash-alt fa-fw"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop
