 @extends('q_admin.master')
 @section('content')
 
    <div class="col-lg-12">
        <h1 class="page-header">Sản phẩm
            <small>Thêm</small>
        </h1>
        <div class="btn btn-danger block">
            <p><b>* Lưu ý:</b></p>
            <ul>
                <li>Kích thước sản phẩm phải theo tỷ lệ 1:1 (ví dụ: cao 100mm và rộng 100mm)</li>
            </ul>
        </div>
    </div>
    <!-- /.col-lg-12 -->
    <form action="{{ route('ad.post-product-add') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="col-lg-9" style="padding-bottom:50px">
            <div class="form-group">
                <label>Tên sản phẩm</label>
                <input class="form-control" name="name" type="text" placeholder="Nhập tên sản phẩm" />
            </div>
            <div class="form-group">
                <label>Nội Dung nổi bật</label>
                <textarea class="form-control" rows="4" name="contentHot"></textarea>
            </div>
            <div class="form-group">
                <label>Nội Dung</label>
                <textarea class="form-control content" rows="4" name="content"></textarea>
            </div>
        </div>
        <div class="col-lg-3">
             <div class="form-group">
                <label>Giá</label>
                <input class="form-control" name="price" type="text" placeholder="Nhập giá sản phẩm" />
            </div>
            <div class="form-group">
                <label>Mức giảm giá</label>
                <input type="text" name="sale" class="form-control">
            </div>
            <div class="form-group">
                <label>Trạng thái</label>
                <select name="status" class="form-control">
                    <option value="1">Bản nháp</option>
                    <option value="2" selected>Chờ duyệt</option>
                    <option value="3">Đã xuất bản</option>
                    {{-- <option value="4">Đã xóa</option> --}}
                </select>
            </div>
            <div class="form-group">
                <label>Danh mục</label>
                <select name="category_id" class="form-control">
                    @foreach ($theloai as $element)
                        <option value="{{ $element->id }}">{{ $element->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Ảnh Thumnail</label>
                <input type="file" name="imageProduct" class="form-control">
            </div>
            <div class="form-group">
                <label>Image detail</label>
                <input multiple="multiple" type="file" name="imageDetail[]" class="form-control">
            </div>

            {{-- <div class="form-group">
                <label>Product Status</label>
                <label class="radio-inline">
                    <input name="rdoStatus" value="1" checked="" type="radio">Bản nháp
                </label>
                <label class="radio-inline">
                    <input name="rdoStatus" value="2" type="radio">Chờ duyệt
                </label>
                <label class="radio-inline">
                    <input name="rdoStatus" value="3" type="radio">Đã xuất bản
                </label>

            </div> --}}
        </div>
        <div class="col-lg-12 margin_bottom">
            <button type="submit" class="btn btn-default">Product Add</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </div>
    <form>
@stop
