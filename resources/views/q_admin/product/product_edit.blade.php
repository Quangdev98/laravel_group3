 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Product
            <small>Edit</small>
        </h1>
        <div class="btn btn-danger block">
            <p><b>* Lưu ý:</b></p>
            <ul>
                <li>Kích thước sản phẩm phải theo tỷ lệ 1:1 (ví dụ: cao 100mm và rộng 100mm)</li>
            </ul>
        </div>
    </div>
    <!-- /.col-lg-12 -->
    <form action="{{ route('ad.post-product-edit', ['id'=>$product->id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="col-lg-9" style="padding-bottom:50px">
            <div class="form-group">
                <label>Tên sản phẩm</label>
                <input class="form-control" value="{{ $product->name }}" name="name" type="text" placeholder="Nhập tên sản phẩm" />
                {!! $errors->has('name') ? '<div class="alert alert-danger">'.$errors->first('name').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Nội Dung nổi bật</label>
                <textarea class="form-control" rows="4" value="{{ $product->contentHot }}" name="contentHot">{{ $product->contentHot }}</textarea>
                {!! $errors->has('contentHot') ? '<div class="alert alert-danger">'.$errors->first('contentHot').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Nội Dung</label>
                <textarea class="form-control content" rows="4" value="{{ $product->content }}" name="content">{{ $product->content }}</textarea>
            </div>
            {!! $errors->has('content') ? '<div class="alert alert-danger">'.$errors->first('content').'</div>' : ''!!}
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label>Giá</label>
                <input class="form-control" value="{{ $product->price }}" name="price" type="text" placeholder="Nhập giá sản phẩm" />
                {!! $errors->has('price') ? '<div class="alert alert-danger">'.$errors->first('price').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Mức giảm giá(%)</label>
                <input type="text" value="{{ $product->sale }}" name="sale" class="form-control">
                {!! $errors->has('sale') ? '<div class="alert alert-danger">'.$errors->first('sale').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Trạng thái</label>
                <select value="{{ $product->status }}" name="status" class="form-control">
                    <option value="1" {{ $product->status == 1 ? 'selected' : '' }}>Bản nháp</option>
                    <option value="2"  {{ $product->status == 2 ? 'selected' : '' }}>Chờ duyệt</option>
                    <option value="3"  {{ $product->status == 3 ? 'selected' : '' }}>Đã xuất bản</option>
                    <option value="4"  {{ $product->status == 4 ? 'selected' : '' }}>Đã xóa</option>
                </select>
                {!! $errors->has('status') ? '<div class="alert alert-danger">'.$errors->first('status').'</div>' : ''!!}
            </div>
            <div class="form-group">
                <label>Danh mục</label>
                <select value="{{ $product->category_id }}" name="category_id" class="form-control">
                    @foreach ($theloai as $element)
                        <option value="{{ $element->id }}" {{ $product->category_id == $element->id ? 'selected' : '' }}>{{ $element->name }}</option>
                    @endforeach
                </select>
            </div>
             <div class="form-group">
                <label>Images</label>
                <input type="file" value="" name="imageProduct" class="form-control">
                <div class="mt-5" style="text-align: center;">
                    <img src="uploads/products/{{ $product->image?:'' }}" class="images" alt="" style="width:80%;margin:10px 0">
                </div>
                {!! $errors->has('imageProduct') ? '<div class="alert alert-danger">'.$errors->first('imageProduct').'</div>' : ''!!}
            </div>
             <div class="form-group">
                <label>Image detail</label>
                <input multiple="multiple" type="file" name="imageDetail[]" class="form-control">
            </div>
            {{-- <div class="form-group">
                <label>Product Status</label>
                <label class="radio-inline">
                    <input name="rdoStatus" value="1" checked="" type="radio">Bản nháp
                </label>
                <label class="radio-inline">
                    <input name="rdoStatus" value="2" type="radio">Chờ duyệt
                </label>
                <label class="radio-inline">
                    <input name="rdoStatus" value="3" type="radio">Đã xuất bản
                </label>
                
            </div> --}}
        </div>
        <div class="col-lg-12 margin_bottom">
            <button type="submit" class="btn btn-default">Sửa sản phẩm</button>
            {{-- <button type="reset" class="btn btn-default">Reset</button> --}}
        </div>
    <form>
@stop