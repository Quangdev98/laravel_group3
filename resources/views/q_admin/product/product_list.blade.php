 @extends('q_admin.master')
 @section('content')
 @if(session('thongbao'))
 <div class="col-lg-12">
     <div class="alert alert-success" role="alert">
     <h4 class="alert-heading">{{session('thongbao')}}</h4>
     </div>
 @endif
    <div class="col-lg-12">
        <h1 class="page-header">Sản Phẩm
            <small>Danh sách</small>
        </h1>
        <div class="btn btn-danger block">
            <p><b>* Lưu ý:</b></p>
            <ul>
                <li>Danh sách sản phẩm</li>
                <li>Trạng thái Sản phẩm<br>
                1:Đã xuất bản: sản phẩm đã được xử lý <br>
                2:Bản nháp: Sản phẩm test chưa qua xử lý <br>
                3:Chờ duyệt: Sản phẩm đang trong quá trình kiểm tra chờ xuất bản<br>
                4:Đã xóa: Sản phẩm chuyển trạng thái thành đã xóa được lưu trong mục Danh mục <a href="{{ route('ad.product-delete-list') }}" title="">Sản phẩm đã xóa</a><br>
                </li>
                <li>Kích thước sản phẩm phải theo tỷ lệ 1:1 (ví dụ: cao 100mm và rộng 100mm)</li>
            </ul>
        </div>
    </div>
     <div class="tab-control">
         <div class="form-search" style="display: inline-block">
             <form action="{{ route('ad.product-list') }}" method="get">
                 @csrf
                 <div class="form-controls">
                     <label class="nameW">Tìm kiếm</label><br>
                     <input type="search" name="search" placeholder="tìm kiếm sản phẩm">
                     <button type="submit"><i class="fal fa-search"></i></button>
                 </div>
             </form>
         </div>
         <div class="statusPro">
             <span class="statusName">Hiển thị: </span><a href="{{ route('ad.product-list') }}" title="">Tất cả</a>
         </div>
         <div class="wrap-form">
             <form action="{{ route('ad.product-list') }}" method="get">
                 <div class="form-controls">
                     <label class="nameW">Danh mục</label><br>
                     <select name="danhmuc">
                         <option value="">--Chọn danh mục--</option>
                         @foreach ($danhmucs as $d)
                             <option value="{{ $d->id }}">{{ $d->name }}</option>
                         @endforeach
                     </select>
                 </div>
                 <div class="form-controls">
                     <label class="nameW">Trạng thái</label><br>
                     <select name="status">
                         <option value="">--Chọn trạng thái--</option>
                         <option value="1">Bản nháp</option>
                         <option value="2">Chờ duyệt</option>
                         <option value="3">Đã xuất bản</option>
                     </select>
                 </div>
                 <div class="form-controls">
                     <label class="nameW">Mức giảm giá</label><br>
                     <input type="number" placeholder="nhập mức giảm giá..." name="sale">
                 </div>
                 <button type="submit">Thao tác</button>
             </form>
         </div>
     </div>
     <form action="{{ route('ad.product-list') }}" method="get">
        <table class="table table-striped table-bordered table-hover {{ $user_admin->slug }}_2" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>ID</th>
{{--                    <th class="idPro">Chọn</th>--}}
                    <th class="imageClass">Ảnh</th>
                    <th class="titleClass">Tên sản phẩm</th>
                    <th>Nội dung tiêu đề</th>
                    <th>Giá</th>
                    {{-- <th>Date</th> --}}
                    <th>Giảm giá</th>
                    <th class="categorysClass">Danh Mục</th>
                    <th class="statusTable">Trạng thái</th>
                    <th>Sửa / Xóa</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($product as $pro)
                    <tr class="odd gradeX" align="center">
                        <td>{{ $pro->id }}</td>
{{--                        <td><input type="checkbox" name="checked" value="{{$pro->id}}"></td>--}}
                        <td><img src="uploads/products/{{ $pro->image }}" alt=""></td>
                        <td>{{ $pro->name }}</td>
                        <td>{{ $pro->contentHot }}</td>
                        <td>{{ number_format($pro->price, 0,'',',') }} VND/1KG</td>
                        <td>{{ $pro->sale }}%</td>
                        <td>{{ $pro->nameCate }}</td>
                        <td>
                            <span class="
                                {{ $pro->status == 1 ? 'bannhap' : '' }}
                                {{ $pro->status == 2 ? 'choduyet' : '' }}
                                {{ $pro->status == 3 ? 'daxuatban' : '' }}
                                {{ $pro->status == 4 ? 'daxoa' : '' }}
                            ">
                                {{ $pro->status == 1 ? 'Bản nháp' : '' }}
                                {{ $pro->status == 2 ? 'Chờ duyệt' : '' }}
                                {{ $pro->status == 3 ? 'Đã xuất bản' : '' }}
                                {{ $pro->status == 4 ? 'Đã xóa' : '' }}
                            </span>
                        </td>
                        <td class="center treatment">
                            <a href="{{ Route('ad.product-edit',['id'=> $pro->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
                            <a href="{{ route('ad.product-delete-status',['id'=>$pro->id]) }}" class="btn-delete"><i class="fad fa-trash-alt fa-fw"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
{{--         <button type="submit">DDDD</button>--}}
     </form>
    {{ $product->links() }}
@stop
