 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Slide
            <small>{{ $typePost->name }}</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
        <form action="{{ route('ad.post-typePost-edit', ['id'=>$typePost->id]) }}" method="POST">
            @csrf
            <div class="form-group">
                <label>Tiêu đề</label>
                <input class="form-control" name="name" value="{{ $typePost->name }}" placeholder="Nhập Tiêu đề" />
            </div>
            
            
            <button type="submit" class="btn btn-default">Thêm Thể loại</button>
        <form>
    </div>
@stop