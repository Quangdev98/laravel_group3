 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Slide
            <small>Danh Sách</small>
        </h1>
        <div class="btn btn-danger block">
            <p><b>* Lưu ý:</b></p>
            <ul>
                <li>Danh mục quản lý danh mục bài viết</li>
               {{--  <li>Trạng thái Phân quyền: <br>
                1:Maintainer(Admin): Có quyền quản trị Admin có tất cả các quyền để quản lý admin <br>
                2:Producter(Quản lý sản phẩm): Có quyền quản trị trong phạm vi Thêm / Sửa / Xóa sản phẩm <br>
                3:Post-Management(quản lý bài viết): Có quyền quản trị trong phạm vi Thêm / Sửa / Xóa Bài viết <br>
                </li> --}}
            </ul>
        </div>
    </div>
    <!-- /.col-lg-12 -->
    <table class="table table-striped table-bordered table-hover {{ $user_admin->slug }}_3" id="dataTables-example">
        <thead>
            <tr align="center">
                <th>ID</th>
                <th>Tên Thể loại</th>
                {{-- <th>Số lượng bài viết</th> --}}
                <th>Sửa / Xóa</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($typePost as $tP)
                <tr class="odd gradeX" align="center">
                    <td>{{ $tP ->id }}</td>
                    <td>{{ $tP ->name }}</td>
                    {{-- <td>{{ $tP ->countPost }}</td> --}}
                    <td class="center treatment">
                        <a href="{{ Route('ad.typePost-edit',['id'=> $tP->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{ route('ad.typePost-delete',['id'=>$tP->id]) }}" class="btn-delete"><i class="fad fa-trash-alt fa-fw"></i></a>
                    </td>
                </tr>
            @endforeach
            
        </tbody>
    </table>
@stop
