 @extends('q_admin.master')
 @section('content')
    <div class="col-lg-12">
        <h1 class="page-header">Slide
            <small>Thêm</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
         @if (count($errors) >0)
           <div class="alert alert-danger">
               @foreach ($errors->all() as $err)
                   {{$err}}
               @endforeach
           </div>
        @endif
        @if (session('thongbao'))
            <div class="alert alert-success">
                {{ session('thongbao') }}
            </div>
        @endif
        <form action="{{ route('ad.post-typePost-add') }}" method="POST">
            @csrf
            <div class="form-group">
                <label>Tiêu đề</label>
                <input class="form-control" name="name" placeholder="Nhập Tiêu đề" />
            </div>
            
            
            <button type="submit" class="btn btn-default">Thêm Thể loại</button>
        <form>
    </div>
@stop