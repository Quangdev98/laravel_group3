@extends('q_web.master')
@section('titlePage', 'bài viết')
@section('classPage', 'blog_page')
@section('content')
	<!--Blog With Sidebar-->
	<div class="blog_sidebar_wrapper clv_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-9">
					<div class="blog_left_section">
						@foreach ($post as $value)
							<div class="blog_section">
								<div class="agri_blog_image">
									<img src="uploads/posts/{{ $value->image }}" alt="image">
									<span class="agri_blog_date">{{ \Carbon\Carbon::create($value->postDate)->toFormattedDateString() }}</span>
								</div>
								<div class="agri_blog_content">
									<h3><a href="{{ route('web.blog-single',['slug'=>$value->slug]) }}">{{ $value->title }}</a></h3>
									<div class="blog_user">
										<div class="user_name">
											<img src="uploads/avatars/{{ $value->avatar }}" alt="image">
											<a href="javascript:;"><span>{{ $value->name_author }}</span></a>
										</div>
										<div class="comment_block">
											<span><i class="fa fa-comments-o" aria-hidden="true"></i></span>
											<a href="javascript:;">26 Lượt xem</a>
										</div>
									</div>
									<p>{{ $value->contentHot }}</p>
									<a href="{{ route('web.blog-single',['slug'=>$value->slug]) }}">read more <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
								</div>
							</div>
						@endforeach
						
						
						<div class="blog_pagination_section">
							{{ $post->links() }}
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3">
					<div class="blog_sidebar">
						<div class="sidebar_block">
							<div class="sidebar_heading">
								<h3>Tìm kiếm</h3>
								<img src="q_web/images/footer_underline.png" alt="image">
							</div>
							<div class="sidebar_search">
                                <form action="{{ route('web.blog') }}" method="get">
                                    @csrf
                                    <input type="text" name="search" placeholder="Tìm kiếm tin tức">
                                    <button type="submit" class="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
							</div>
						</div>
						<div class="sidebar_block">
							<div class="sidebar_heading">
								<h3>Danh mục</h3>
								<img src="q_web/images/footer_underline.png" alt="image">
							</div>
							<div class="sidebar_category">
								<ul>
                                    @foreach ($countPost as $cp)
                                        <li><a href="{{ route('web.blog',['id'=>$cp->id]) }}">{{ $cp->name }}<span>({{$cp->countPostNumber}})</span></a></li>
                                    @endforeach
                                </ul>
							</div>
						</div>
						<div class="sidebar_block">
							<div class="sidebar_heading">
								<h3>Bài viết liên quan</h3>
								<img src="q_web/images/footer_underline.png" alt="image">
							</div>
							<div class="sidebar_post">
								<ul>
									@foreach ($postSlide as $element)
										{{-- expr --}}
										<li>
											<div class="post_image">
												<img src="uploads/posts/{{ $element->image }}" alt="image">
											</div>
											<div class="post_content">
												<p>{{ \Carbon\Carbon::create($element->postDate)->toFormattedDateString() }}</p>
												<a href="{{ route('web.blog-single',['slug'=>$element->slug]) }}">{{ $element->title }}</a>
											</div>
										</li>
									@endforeach
									
								</ul>
							</div>
						</div>
{{--						<div class="sidebar_block">--}}
{{--							<div class="sidebar_heading">--}}
{{--								<h3>archives</h3>--}}
{{--								<img src="q_web/images/footer_underline.png" alt="image">--}}
{{--							</div>--}}
{{--							<div class="sidebar_category">--}}
{{--								<ul>--}}
{{--									<li><a href="javascript:;">August  2018<span>(12)</span></a></li>--}}
{{--									<li><a href="javascript:;">September  2018<span>(16)</span></a></li>--}}
{{--									<li><a href="javascript:;">October  2018<span>(156)</span></a></li>--}}
{{--									<li><a href="javascript:;">November 2018<span>(260)</span></a></li>--}}
{{--									<li><a href="javascript:;">December  2018<span>(96)</span></a></li>--}}
{{--									<li><a href="javascript:;">January  2019<span>(12)</span></a></li>--}}
{{--								</ul>--}}
{{--							</div>--}}
{{--						</div>--}}
						<div class="sidebar_block">
							<div class="sidebar_test_slider">
								<div class="swiper-container">
									<div class="swiper-wrapper">
                                        @foreach ($opinionSlideBar  as $op)
										    <div class="swiper-slide">
                                                <div class="sidebar_test_slide">
                                                    <div class="test_slide_image">
                                                        <img src="/uploads/opinion/{{ $op->image }}" alt="image" style="width: 66px; height: 66px; object-fit: cover">
                                                    </div>
                                                    <div class="test_slide_content">
                                                        <p class="textOpinion">{{ $op->content }}</p>
                                                        <h5>{{ $op->name }}</h5>
                                                        <img src="q_web/images/blogside_quote.png" alt="image">
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
									</div>
									<div class="sidebar_arrow_wrapper">
										<span class="sidebar_test_arrow test_left">
											<i class="fa fa-angle-left" aria-hidden="true"></i>
										</span>
										<span class="sidebar_test_arrow test_right">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="sidebar_block">
							<div class="sidebar_heading">
								<h3>Thẻ</h3>
								<img src="q_web/images/footer_underline.png" alt="image">
							</div>
							<div class="sidebar_tags">
								<ul>
                                    @foreach ($keyTag as $keyt)
                                        <li><a href="{{ route('web.blog',['tag'=>$keyt]) }}">{{ $keyt }}</a></li>
                                    @endforeach


								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
