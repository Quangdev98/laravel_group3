@extends('q_web.master')
@section('titlePage', 'Hoàn thành đơn hàng')
@section('classPage', 'index_v1')
@section('content')
    <!--Checkout-->
    <div class="form-success" style="margin:50px 15px 30px; padding:0 15px">
        <div class="col-md-6" style="margin:0 auto">
            <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Bạn đã đạt hàng thành công! <br> Vui lòng kiểm tra email để xem thông tin đơn hàng của bạn.</h4>
                <p></p>
                <p class="mb-0"></p>
            </div>
            <div class="closeIndex">
                <a class="" href="{{ route('web.index') }}" title="" style="text-decoration: revert;"><i class="fa fa-arrow-right"></i>Quay lại trang chủ</a>
            </div>
        </div>
    </div>
    
@stop
