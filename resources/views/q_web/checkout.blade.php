@extends('q_web.master')
@section('titlePage', 'Check-Out')
@section('classPage', 'index_v1')
@section('content')
    <!--Checkout-->
    <div class="clv_checkout_wrapper clv_section">
        <div class="container">
            <form action="checkout/post" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="checkout_inner">
                        <div class="checkout_heading">
                            <h3>Thủ tục thanh toán</h3>
                            <h5>Thông tin liên hệ</h5>
                        </div>
                        <div class="checkout_form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form_block"><input name="name" type="text" class="form_field" placeholder="Họ và Tên" ></div>
                                    {{showErrors($errors,'name')}}
                                </div>
                                <div class="col-md-6">
                                    <div class="form_block"><input name="email" type="text" class="form_field" placeholder="Email" ></div>
                                    {{showErrors($errors,'email')}}
                                </div>

                                <div class="col-md-6">
                                    <div class="form_block"><input name="address" type="text" class="form_field" placeholder="Địa chỉ" ></div>
                                    {{showErrors($errors,'address')}}
                                </div>
                                <div class="col-md-6">
                                    <div class="form_block"><input name="phone" type="text" class="form_field" placeholder="Số điện thoại" ></div>
                                    {{showErrors($errors,'phone')}}
                                </div>
                            </div>
                        </div>
                        {{-- <div class="checkout_heading">
                            <h5>Phương thức thanh toán</h5>
                        </div> --}}
                        {{-- <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="payment_option">
                                    <ul>
                                        <li>
                                            <input type="radio" name="p1" id="p1" checked>
                                            <label for="p1">
                                                <span><img src="images/mastercard.png" alt="image"></span>
                                                <span>master card</span>
                                            </label>
                                        </li>
                                        <li>
                                            <input type="radio" name="p1" id="p2">
                                            <label for="p2">
                                                <span><img src="images/visa.png" alt="image"></span>
                                                <span>visa card</span>
                                            </label>
                                        </li>
                                        <li>
                                            <input type="radio" name="p1" id="p3">
                                            <label for="p3">
                                                <span><img src="images/paypal.png" alt="image"></span>
                                                <span>paypal</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="checkout_submit">
                                    <button class="clv_btn">submit</button>
                                    <a href="/"><span><i class="fa fa-angle-left" aria-hidden="true"></i></span> Tiếp tục mua sắm</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="cart_summery_block">
                        <h3>Giỏ hàng của bạn</h3>
                        <h5>Bạn có {{Cart::content()->count()}} sản phẩm trong giỏ hàng</h5>
                        <ul>
                            @foreach ($cart as $row)
                            <li>
                                <div class="product_img"><img src="uploads/products/{{$row->options->img}}" alt="image"></div>
                                <div class="product_quantity">
                                    <h6 class="namePro">{{$row->name}}</h6>
                                    <p>x{{$row->qty}}</p>
                                </div>
                                <div class="product_price">
                                    <h4>{{number_format($row->price,0,'',',')}} VNĐ</h4>
                                </div>
                            </li>
                            @endforeach

                            <li>
                                <div class="total_amount">
                                    <h4>Tổng</h4>
                                </div>
                                <div class="product_price">
                                    <h4>{{number_format($total,0,'',',')}} VNĐ</h4>
                                </div>
                            </li>
                        </ul>
                        {{-- <a href="javascript:;">have a discount code?</a> --}}
                    </div>
                    <div class="guarantee_block">
                        <div class="guaranty_icon"><img src="q_web/images/protection.png" alt="image"></div>
                        <h3>money back guaranty</h3>
                        <p>Consectetur adipisicing elit se eiusmod tempor incididunt ut labore et dolore magna aliquanim ad minim veniam quis nostrud.</p>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>
@stop
