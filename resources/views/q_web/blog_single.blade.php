@extends('q_web.master')
@section('titlePage', 'Chi tiết bài viết')
@section('classPage', 'blog_page')
@section('content')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0" nonce="nzZkdmr7"></script>
    <!--Blog With Sidebar-->
    <div class="blog_sidebar_wrapper clv_section blog_single_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9">
                    <div class="blog_left_section">
                        <div class="blog_section">
                            <div class="agri_blog_image">
                                <img src="/uploads/posts/{{ $blog->image }}" alt="image">
                                <span class="agri_blog_date">{{ \Carbon\Carbon::create($blog->postDate)->toFormattedDateString() }}</span>
                            </div>
                            <div class="agri_blog_content">
                                <h3><a href="function:;" title="">{{ $blog->title }}</a></h3>
                                <div class="blog_user">
                                    <div class="user_name">
                                        <img src="{{ $blog->avatar !== null ? '/uploads/avatars/'.$blog->avatar : '/q_web/images/user.png' }}" alt="{{ $blog->name_author }}">
                                        <a href="javascript:;"><span>{{ $blog->name_author }}</span></a>
                                    </div>
                                    <div class="comment_block">
                                        <span><i class="fa fa-comments-o" aria-hidden="true"></i></span>
                                        <a href="javascript:;">26 Lượt xem</a>
                                    </div>

                                    <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large">
                                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a>
                                    </div>

                                </div>
                                <div>
                                    {!! $blog->content !!}
                                </div>
                            </div>
                            <div class="author_message_box">
                                <div class="author_image">
                                    <img src="https://via.placeholder.com/150x176" alt="image">
                                </div>
                                <div class="author_content">
                                    <div class="social_name">
                                        <h3>Quản trị viên - <span>Quang Dev</span></h3>
                                        <ul>
                                            <li><a href="javascript:;"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
                                            <li><a href="javascript:;"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a></li>
                                            <li><a href="javascript:;"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
                                            <li><a href="javascript:;"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a></li>
                                        </ul>
                                    </div>
                                    <p>Hiện nay, thực phẩm bẩn đang là nỗi lo thường trực trong mỗi gia đình khi mà báo chí và các phương tiện truyền thông thường xuyên đăng tải những vụ việc rùng rợn về vấn nạn vệ sinh an toàn thực phẩm. Chính vì thế, kinh doanh thực phẩm sạch đang là một hướng đi được nhiều doanh nghiệp quan tâm đầu tư.</p>
                                </div>
                            </div>
                            <div class="blog_single_heading">
                                <h3>comments</h3>
                                <img src="/q_web/images/footer_underline.png" alt="image">
                            </div>
                            <div class="comment-face">
                                <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5" data-width=""></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="blog_sidebar">
                        <div class="sidebar_block">
                            <div class="sidebar_heading">
                                <h3>search</h3>
                                <img src="/q_web/images/footer_underline.png" alt="image">
                            </div>
                            <div class="sidebar_search">
                                <form action="{{ route('web.blog') }}" method="get">
                                    @csrf
                                    <input type="text" name="search" placeholder="Tìm kiếm tin tức">
                                    <button type="submit" class="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="sidebar_block">
                            <div class="sidebar_heading">
                                <h3>category</h3>
                                <img src="/q_web/images/footer_underline.png" alt="image">
                            </div>
                            <div class="sidebar_category">
                                <ul>
                                    @foreach ($countPost as $cp)
                                        <li><a href="{{ route('web.blog',['id'=>$cp->id]) }}">{{ $cp->name }}<span>({{$cp->countPostNumber}})</span></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="sidebar_block">
                            <div class="sidebar_heading">
                                <h3>recent post</h3>
                                <img src="/q_web/images/footer_underline.png" alt="image">
                            </div>
                            <div class="sidebar_post">
                                <ul>
                                    @foreach ($postSlide as $element)
                                        {{-- expr --}}
                                        <li>
                                            <div class="post_image">
                                                <img src="/uploads/posts/{{ $element->image }}" alt="image">
                                            </div>
                                            <div class="post_content">
                                                <p>{{ \Carbon\Carbon::create($element->postDate)->toFormattedDateString() }}</p>
                                                <a href="{{ route('web.blog-single',['slug'=>$element->slug]) }}">{{ $element->title }}</a>
                                            </div>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                        {{--						<div class="sidebar_block">--}}
                        {{--							<div class="sidebar_heading">--}}
                        {{--								<h3>archives</h3>--}}
                        {{--								<img src="/q_web/images/footer_underline.png" alt="image">--}}
                        {{--							</div>--}}
                        {{--							<div class="sidebar_category">--}}
                        {{--								<ul>--}}
                        {{--									<li><a href="javascript:;">August  2018<span>(12)</span></a></li>--}}
                        {{--									<li><a href="javascript:;">September  2018<span>(16)</span></a></li>--}}
                        {{--									<li><a href="javascript:;">October  2018<span>(156)</span></a></li>--}}
                        {{--									<li><a href="javascript:;">November 2018<span>(260)</span></a></li>--}}
                        {{--									<li><a href="javascript:;">December  2018<span>(96)</span></a></li>--}}
                        {{--									<li><a href="javascript:;">January  2019<span>(12)</span></a></li>--}}
                        {{--								</ul>--}}
                        {{--							</div>--}}
                        {{--						</div>--}}
                        <div class="sidebar_block">
                            <div class="sidebar_test_slider">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        @foreach ($opinionSlideBar  as $op)
                                            <div class="swiper-slide">
                                                <div class="sidebar_test_slide">
                                                    <div class="test_slide_image">
                                                        <img src="/uploads/opinion/{{ $op->image }}" alt="image" style="width: 66px; height: 66px; object-fit: cover">
                                                    </div>
                                                    <div class="test_slide_content">
                                                        <p class="textOpinion">{{ $op->content }}</p>
                                                        <h5>{{ $op->name }}</h5>
                                                        <img src="/q_web/images/blogside_quote.png" alt="image">
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="sidebar_arrow_wrapper">
										<span class="sidebar_test_arrow test_left">
											<i class="fa fa-angle-left" aria-hidden="true"></i>
										</span>
                                        <span class="sidebar_test_arrow test_right">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar_block">
                            <div class="sidebar_heading">
                                <h3>tags</h3>
                                <img src="/q_web/images/footer_underline.png" alt="image">
                            </div>
                            <div class="sidebar_tags">
                                <ul>
                                    @foreach ($keyTag as $keyt)
                                        <li><a href="{{ route('web.blog',['tag'=>$keyt]) }}">{{ $keyt }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
