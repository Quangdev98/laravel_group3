<div class="clv_menu">
	<div class="clv_menu_nav">
		<ul>
			<li>
				<a href="{{ route('web.index') }}" class="{{ \Route::current()->getName() == 'web.index' ? "active" : " " }}">trang chủ</a>

			</li>
			<li><a href="{{ route('web.about') }}" class="{{ \Route::current()->getName() == 'web.about' ? "active" : " " }}">giới thiệu</a></li>
			<li><a href="{{ route('web.service') }}" class="{{ \Route::current()->getName() == 'web.service' ? "active" : " " }}">dịch vụ</a></li>
            <li><a href="{{ route('web.products') }}" class="{{ \Route::current()->getName() == 'web.products' ? "active" : " " }}">Sản phẩm</a></li>
            <li>
				<a href="{{ route('web.blog') }}" class="{{ \Route::current()->getName() == 'web.blog' ? "active" : " " }}">Bài viết</a>
			</li>
           <li>
                <a href="javascript:;">khác</a>
                <ul>
                    <li><a href="{{ route('web.gallery') }}" class="{{ \Route::current()->getName() == 'web.gallery' ? "active" : " " }}">bộ sưu tập</a></li>
                    {{-- <li><a href="{{ route('web.profile') }}" class="{{ \Route::current()->getName() == 'web.profile' ? "active" : " " }}">profile</a></li> --}}
					{{-- <li><a href="{{ route('web.product_single') }}" class="{{ \Route::current()->getName() == 'web.product_single' ? "active" : " " }}">product single</a></li> --}}
					{{-- <li><a href="{{ route('web.checkout') }}" class="{{ \Route::current()->getName() == 'web.checkout' ? "active" : " " }}">checkout</a></li> --}}
					<li><a href="{{ route('web.cart_single') }}" class="{{ \Route::current()->getName() == 'web.cart_single' ? "active" : " " }}">Giỏ hàng</a></li>
					<li><a href="{{ route('web.login') }}" {{-- class="{{ \Route::current()->getName() == 'web.login' ? "active" : " " }}" --}}>Đăng nhập</a></li>
					{{-- <li><a href="404.html">404</a></li> --}}
				</ul>
			</li>
            <li><a href="{{ route('web.contact') }}" class="{{ \Route::current()->getName() == 'web.contact' ? "active" : " " }}">liên hệ</a></li>
        </ul>
    </div>
	<div class="cart_nav">
		<ul>
			<li>
				<a class="search_toggle" href="javascript:;"><i class="fa fa-search" aria-hidden="true"></i></a>
			</li>
			<li>
				<a class="cart_toggle" href="javascript:;">
					<svg
					 xmlns="http://www.w3.org/2000/svg"
					 xmlns:xlink="http://www.w3.org/1999/xlink"
					 width="18px" height="20px">
					<path fill-rule="evenodd"  fill="rgb(0, 0, 0)"
					 d="M16.712,5.566 C16.682,5.244 16.404,4.997 16.071,4.997 L12.857,4.997 L12.857,3.747 C12.857,1.678 11.127,-0.004 9.000,-0.004 C6.873,-0.004 5.143,1.678 5.143,3.747 L5.143,4.997 L1.928,4.997 C1.595,4.997 1.318,5.244 1.288,5.566 L0.002,19.315 C-0.014,19.490 0.046,19.664 0.168,19.793 C0.290,19.923 0.462,19.997 0.643,19.997 L17.357,19.997 C17.538,19.997 17.710,19.923 17.832,19.793 C17.952,19.664 18.014,19.490 17.997,19.315 L16.712,5.566 ZM6.428,3.747 C6.428,2.367 7.582,1.247 9.000,1.247 C10.417,1.247 11.571,2.367 11.571,3.747 L11.571,4.997 L6.428,4.997 L6.428,3.747 ZM1.347,18.745 L2.515,6.247 L5.143,6.247 L5.143,7.670 C4.759,7.887 4.500,8.286 4.500,8.746 C4.500,9.436 5.076,9.996 5.786,9.996 C6.495,9.996 7.071,9.436 7.071,8.746 C7.071,8.286 6.812,7.887 6.428,7.670 L6.428,6.247 L11.571,6.247 L11.571,7.670 C11.188,7.887 10.928,8.284 10.928,8.746 C10.928,9.436 11.505,9.996 12.214,9.996 C12.924,9.996 13.500,9.436 13.500,8.746 C13.500,8.286 13.240,7.887 12.857,7.670 L12.857,6.247 L15.484,6.247 L16.653,18.745 L1.347,18.745 Z"/>
					</svg>
                <span>{{Cart::content()->count()}}</span>
				</a>
				<div class="clv_cart_box">
					<div class="cart_section">
						<p>Bạn có <span>{{Cart::content()->count()}}</span> sản phẩm trong giỏ hàng</p>
						<ul>
                            @foreach (Cart::content() as $row)
                            
                            <li>
								<div class="cart_block">
									<img src="uploads/products/{{ $row->options->img }}" alt="image" style="width: 60px;height: 60px; object-fit: cover" />
								</div>
								<div class="cart_block">
                                <h5 class="namePro">{{ $row->name }}</h5>
									<div class="item_quantity">
{{--										<a href="javascript:;" class="quantity_minus" >-</a>--}}
                                    <input type="text" value="{{$row->qty}}" class="quantity" disabled />
{{--										<a href="javascript:;" class="quantity_plus" >+</a>--}}
									</div>
								</div>
								<div class="cart_block">
									<h4><span><i class="fa fa-usd" aria-hidden="true"></i></span> {{ number_format($row->price*$row->qty,0,'',',') }}</h4>
								</div>
							</li>

                            @endforeach


							<li>
								<h3>total</h3>
								<h4><span><i class="fa fa-usd" aria-hidden="true"></i></span>{{ number_format(cart::total(0,'','')) }}</h4>
							</li>
						</ul>
					</div>
					<a href="checkout" class="cart_action_btn">check out</a>
				</div>
			</li>
			<li class="menu_toggle">
				<span>
					<?xml version="1.0" encoding="iso-8859-1"?>
					<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 53 53" style="enable-background:new 0 0 53 53;" xml:space="preserve" width="20px" height="20px">
					<g>
						<g>
							<path d="M2,13.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,13.5,2,13.5z"/>
							<path d="M2,28.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,28.5,2,28.5z"/>
							<path d="M2,43.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,43.5,2,43.5z"/>
						</g>
					</g>
					</svg>
				</span>
			</li>
		</ul>
	</div>
</div>
