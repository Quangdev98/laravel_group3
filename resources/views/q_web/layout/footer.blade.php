<!--Footer-->
<div class="clv_footer_wrapper clv_section" style="background-image: url(/uploads/banner/footer.jpg);">
    <div class="wrapper_footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-3">
                    <div class="footer_block">
                        <div class="footer_logo"><a href="javascript:;"><img src="/q_web/images/footer_logo.png" alt="image" /></a></div>
                        <p>Lorem ipsum dolor sit amet  onsectetadip isicing elit, sed do eiusmod tempordidunt ut labore et dolaliqua.</p>
                        <h6>call now</h6>
                        <h3>{{ $infoHeader->phone }}</h3>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3">
                    <div class="footer_block">
                        <div class="footer_heading">
                            <h4>services</h4>
                            <img src="/q_web/images/footer_underline.png" alt="image" />
                        </div>
                        <ul class="useful_links">
                            <li><a href="javascript:;"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Weed & Pest Control</a></li>
                            <li><a href="javascript:;"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Grass Seeding</a></li>
                            <li><a href="javascript:;"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Programs & Grants</a></li>
                            <li><a href="javascript:;"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Poultry & Agricultural Products</a></li>
                            <li><a href="javascript:;"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Education & event</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3">
                    <div class="footer_block">
                        <div class="footer_heading">
                            <h4>instagram</h4>
                            <img src="/q_web/images/footer_underline.png" alt="image" />
                        </div>
                        <ul class="instagram_links">
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3">
                    <div class="footer_block">
                        <div class="footer_heading">
                            <h4>Hỗ trợ</h4>
                            <img src="/q_web/images/footer_underline.png" alt="image" />
                        </div>
                        <ul class="useful_links">
                            <li><a href="{{ route('web.about') }}"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>giới thiệu</a></li>
                            <li><a href="javascript:;"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Delivery Information</a></li>
                            <li><a href="javascript:;"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Privacy Policy</a></li>
                            <li><a href="javascript:;"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Terms And Conditions</a></li>
                            <li><a href="javascript:;"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Hỗ trợ 24/7</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <img class="foot_girl" src="https://via.placeholder.com/398x529" alt="image"> --}}
</div>
<!--Copyright-->
<div class="clv_copyright_wrapper">
    <p>copyright &copy; 2019 <a href="{{ route('web.index') }}">cultivation.</a> all right reserved.</p>
</div>