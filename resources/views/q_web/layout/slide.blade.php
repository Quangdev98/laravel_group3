<!--Banner Slider-->
	<div class="clv_banner_slider">
		<!-- Swiper -->
		<div class="swiper-container">
			<div class="swiper-wrapper">
				@foreach ($slideHome as $s)
					{{-- expr --}}
			
				<div class="swiper-slide">
					<div class="clv_slide slide2" style="background: url( uploads/slides/{{ $s->image }}) no-repeat center; background-size: cover; height: calc(100vh - 103px)">
						<div class="container">
							<div class="clv_slide_inner">
								<h3>{{ $s->title }}</h3>
								<h1>{{ $s->contentHot }}</h1>
								<a href="/san-pham" class="clv_btn">Khám phá ngay</a>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			<!-- Add Pagination -->
			<div class="swiper-pagination"></div>
			<!-- Add Arrows -->
			<span class="banner_left org_arrow">
				<i class="fa fa-angle-left" aria-hidden="true"></i> <u>prev</u>
			</span>
			<span class="banner_right org_arrow right_arrow">
				<u>next</u><i class="fa fa-angle-right" aria-hidden="true"></i>
			</span>
		</div>
	</div>
