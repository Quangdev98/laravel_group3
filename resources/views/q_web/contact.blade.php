@extends('q_web.master')
@section('titlePage', 'Liên hệ')
@section('classPage', 'index_v1')
@section('content')
    <!--Contact Block-->
    <div class="contact_blocks_wrapper clv_section">

{{--        @if (session('thongbao'))--}}
{{--        <div class="alert alert-success alert-thongbao-success text-center" role="alert">--}}
{{--            <h4 class="alert-heading">{{session('thongbao')}}</h4>--}}
{{--            <p class="checkAA" align="center"><i class="fa fa-check" ></i></p>--}}
{{--            <p class="mb-0"></p>--}}
{{--        </div>--}}

{{--        @endif--}}
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="contact_block">
                        <span></span>
                        <div class="contact_icon"><img src="/q_web/images/contact_icon1.png" alt="image" /></div>
                        <h4>contact us</h4>
                        @foreach ($infoList as $phoneNumber)
                            <p>{{ $phoneNumber->phone }}</p>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="contact_block">
                        <span></span>
                        <div class="contact_icon"><img src="/q_web/images/contact_icon2.png" alt="image" /></div>
                        <h4>email</h4>
                        @foreach ($infoList as $emailNumber)
                            <p>{{ $emailNumber->email }}</p>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="contact_block">
                        <span></span>
                        <div class="contact_icon"><img src="/q_web/images/contact_icon3.png" alt="image" /></div>
                        <h4>địa chỉ</h4>
                        @foreach ($infoList as $addressNumber)
                            <p>{{ $addressNumber->address }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Contact Form-->
    <div class="contact_form_wrapper clv_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="contact_form_section">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <h3>Gửi tin nhắn cho chúng tôi</h3>
                            </div>
							<form action="{{ route('web.contact_post') }}" method="POST">
                                @csrf
                                <div class="col-md-6 col-lg-6">
                                    <div class="form_block">
                                        <input type="text" name="name" class="form_field require" placeholder="Name" required >
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form_block">
                                        <input required type="text" name="email" class="form_field require" placeholder="Email" data-valid="email" data-error="Email should be valid." >
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form_block">
                                        <textarea required placeholder="Message" name="message" class="form_field require" ></textarea>
                                        <div class="response"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-12">
                                    <div class="form_block">
                                        <button type="submit" class="clv_btn submitForm alert-thongbao-success" >gửi</button>
                                    </div>
                                </div>
							</form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="working_time_section">
                        <div class="timetable_block">
                            <h5>giờ làm việc</h5>
                            <ul>
                                <li>
                                    <p>thứ 2</p>
                                    <p>9:30 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>thứ ba</p>
                                    <p>9:00 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>thứ tư</p>
                                    <p>9:45 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>thứ năm</p>
                                    <p>10:30 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>thứ sáu</p>
                                    <p>9:30 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>thứ bảy</p>
                                    <p>9:30 am - 6:00 pm</p>
                                </li>
                                <li>
                                    <p>chủ nhật</p>
                                    <p>Nghỉ</p>
                                </li>
                            </ul>
                        </div>
                        <div class="tollfree_block">
                            {{-- <h5>toll free number</h5>
                            <h3>+1-202-555-0101</h3> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Contact Map-->
    <div class="contact_map_wrapper">
        <div id="map"></div>
    </div>
@stop
@section('linkJavasript')
    <script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 40.674, lng: -73.945},
        zoom: 12,
        styles: [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
            featureType: 'administrative.locality',
            elementType: 'labels.text.fill',
            stylers: [{color: '#d59563'}]
            },
            {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{color: '#d59563'}]
            },
            {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{color: '#263c3f'}]
            },
            {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{color: '#6b9a76'}]
            },
            {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{color: '#38414e'}]
            },
            {
            featureType: 'road',
            elementType: 'geometry.stroke',
            stylers: [{color: '#212a37'}]
            },
            {
            featureType: 'road',
            elementType: 'labels.text.fill',
            stylers: [{color: '#9ca5b3'}]
            },
            {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{color: '#746855'}]
            },
            {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{color: '#1f2835'}]
            },
            {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{color: '#f3d19c'}]
            },
            {
            featureType: 'transit',
            elementType: 'geometry',
            stylers: [{color: '#2f3948'}]
            },
            {
            featureType: 'transit.station',
            elementType: 'labels.text.fill',
            stylers: [{color: '#d59563'}]
            },
            {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{color: '#17263c'}]
            },
            {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{color: '#515c6d'}]
            },
            {
            featureType: 'water',
            elementType: 'labels.text.stroke',
            stylers: [{color: '#17263c'}]
            }
        ]
        });
    }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY6rgnfIZd8qdQ9a3bxM4rkkCK9e7Uy-4&callback=initMap" async defer></script>
    <script>
        // $('.alert-thongbao-success').on('click',function(e){
        //     // e.preventDefault();
        //     swal("Gửi thành công!", "Ok để thoát!", "success");
        // });
{{--        @if (session('thongbao'))--}}

{{--                {{session('thongbao')}}--}}

{{--        @endif--}}
    </script>
@stop
