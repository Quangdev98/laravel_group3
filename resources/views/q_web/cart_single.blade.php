@extends('q_web.master')
@section('titlePage', 'chi tiết giỏ hàng')
@section('classPage', 'index_v1')
@section('content')
@if(session('thongbao'))
                        <div class="alert alert-success" role="alert">
                        <h1 class="alert-heading">{{session('thongbao')}}</h1>
                            <p></p>
                            <p class="mb-0"></p>
                          </div>
                        @endif
    <!--Cart Single-->
    <div class="cart_single_wrapper clv_section">
        <div class="container">
            <div class="cart_table_section table-responsive">
                <div class="table_heading">
                    <h3>Giỏ Hàng</h3>
                    <h4> Có {{Cart::content()->count()}} đơn hàng trong giỏ hàng của bạn</h4>
                </div>
                <table class="cart_table table-responsive woocommerce-cart-form__contents">
                    <tr>
                        <th>Sản phẩm</th>
                        <th>Số Lượng</th>
                        <th>Gía</th>
                        <th>Tổng</th>
                        <th>Xóa</th>
                    </tr>
                    @foreach ($cart as $row)
                    <tr>
                        <td>
                            <div class="product_img">
                            <img src="uploads/products/{{$row->options->img}}" alt="image">
                                <h6>{{$row->name}}</h6>
                            </div>
                        </td>
                        <td>
                            <div class="item_quantity">
                                {{-- <a href="javascript:;" class="quantity_minus">-</a> --}}
                            {{-- <input onchange="update_qty('{{$row->qty}}',this.value)" type="text" value="{{$row->qty}}" class="quantity" disabled=""> --}}
                            <input onChange="update_qty('{{$row->rowId}}',this.value)" min="1" type="number" id="quantity" name="quantity" class="form-control input-number text-center" value="{{$row->qty}}">
                                {{-- <a href="javascript:;" class="quantity_plus">+</a> --}}
                            </div>
                        </td>
                        <td>
                            <div class="pro_price">
                                <h5>{{number_format($row->price,0,'',',')}}</h5>
                            </div>
                        </td>
                        <td>
                            <div class="pro_price">
                                <h5>{{number_format($row->price*$row->qty,0,'',',')}}</h5>
                            </div>
                        </td>
                        <td>
                            <div class="pro_remove">
{{--                                <div class="one-eight text-center">--}}
{{--                                    <div class="display-tc">--}}
{{--                                     <a onclick="return del_cart('{{$row->name}}')" href="/cart/removeCart/{{$row->rowId}}" class="closed">dđ</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <span style="display: inline-block">
                                    {{-- <?xml version="1.0" encoding="iso-8859-1"?>
                                    <svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve" width="12px" height="12px">
                                    <g>
                                        <path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
                                            c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
                                            C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
                                            s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/>
                                    </g>
                                    </svg> --}}
                                    <a onclick="return del_cart()" href="cart_single/remove/{{$row->rowId}}" class="close" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </a>
                                </span>
                            </div>
                        </td>
                    </tr>
                    @endforeach


                    <tr>
                        <td>
                            <div class="pro_price">
                                <h5>Tổng cộng</h5>
                            </div>
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <div class="pro_price">
                                <h5>{{number_format($total,0,'',',')}}</h5>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <div class="checkout_btn_block">
                    <a href="checkout" class="clv_btn checkout-button">proceed to checkout</a>
                </div>
            </div>
        </div>
    </div>
   @stop

   <script>
       function del_cart(){
           return confirm("bạn có muốn xóa sản phẩm này khỏi giỏ hàng không?")
       }
        function update_qty(rowId,qty)
		{
        // alert(rowId+'||'+qty)
		$.get("/cart_single/update/"+rowId+"/"+qty,
            function(data)
            {
                if(data=='success'){
                    window.location.reload();
                }
                else
                {
                    alert('Cập nhật thất bại!');
                }
            }
		);
		}
   </script>
