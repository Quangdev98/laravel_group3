<div class="product_items_section" id="product">
    <ul>
        @foreach ($product as $p)
            <li>
                <div class="product_item_block">
                    <form action="/cart_single/add" method="GET">
                        <div class="org_product_block">
                            @if ($p->sale !== 0)
                                <span class="product_label">{{-- {{ $p->sale < 10 ? '0' : '' }} --}}{{ $p->sale }}% off</span>
                            @endif
                            <div class="org_product_image"><a href="{{ route('web.product_single',['slug'=>$p->slug]) }}" title="" class="imageProduct"><img src="/uploads/products/{{ $p->image }}" alt="{{ $p->name }}"></a></div>
                            <a href="{{ route('web.product_single',['slug'=>$p->slug]) }}" title="" class="titleName"><h4>{{ $p->name }}</h4></a>
                            <h3><span></span>{{ number_format($p->price, 0,'', ',') }}Đ</h3>
                            {{-- <a href="javascript:;" class="addCart">Thêm vào giỏ hàng</a> --}}
                            <input type="hidden" name="id_product" value="{{$p->id}}">
                            <button type="submit" class="addCart">Thêm vào giỏ hàng</button>
                        </div>
                        <div class="content_block">
                            <div class="product_price_box">
                                <a href="{{ route('web.product_single',['slug'=>$p->slug]) }}" title="" class="titleName"><h3>{{ $p->name }}</h3></a>
                                <h5><span></span>{{ number_format($p->price, 0,'', ',') }}Đ</h5>
                            </div>
                            <p>{{ $p->nameCate }}</p>
                            <div class="rating_section">
                                <span>4.1</span>
                                <ul>
                                    <li><a class="active" href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                    <li><a class="active" href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                    <li><a class="active" href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                    <li><a href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                    <li><a href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                </ul>
                                <p>151 reviews</p>
                            </div>
                            <ul class="product_code">
                                <li>
                                    <p>Mã sản phẩm: {{ $p->id <10 ? '0' : '' }}{{ $p->id }}</p>
                                </li>
                                <li>
                                    <p>Trạng thái: <span>còn hàng</span></p>
                                </li>
                            </ul>
                            <p>{{ $p->contentHot }}</p>
                        </div>
                    </form>
                </div>
            </li>
        @endforeach
    </ul>
</div>
<div class="blog_pagination_section">
    {{ $product->links() }}
</div>
