<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-9 marginAuto">
            <div class="wraperEmail">
                <h5>Cảm ơn quý khách đã mua hàng của chúng tôi!</h5>
                <h1 class="text-center">Hóa đơn sản phẩm</h1>
                <span class="titleName">Tên khách hàng:</span> {{$name}}
                <br>
                <span class="titleName">Email:</span>{{$email}}
                <br>
                <span class="titleName">Địa chỉ:</span>{{$address}}
                <br>
                <span class="titleName">Điện thoại:</span>{{$phone}}
                <br>
                <span class="titleName">Tổng tiền:</span><span class="totalPrice">{{$tongtien}}</span>
                <br>
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr align="center">
                        <th>ID</th>
                        {{--            <th>Ảnh</th>--}}
                        <th>Tên</th>
                        <th>Giá</th>
                        <th>Số lượng</th>
                        <th>Tổng giá</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach (cart::content() as $pro)
                        <tr class="odd gradeX" align="center">
                            <td>{{ $pro->id }}</td>
                            {{--                <td><img src="/uploads/products/{{ $pro->options->img }}" alt=""></td>--}}
                            <td>{{ $pro->name }}</td>
                            <td>{{ number_format($pro->price) }}</td>
                            <td>{{ $pro->qty }}</td>
                            <td>{{ number_format(($pro->price)*($pro->qty)) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
