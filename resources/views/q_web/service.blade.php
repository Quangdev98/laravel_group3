@extends('q_web.master')
@section('titlePage', 'dịch vụ')
@section('classPage', 'service_page')
@section('content')
	<!--Features-->
	<div class="clv_features_wrapper clv_section">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-lg-4">
					<div class="feature_block">
						<div class="feature_img">
							<img src="/q_web/images/agri_product.png" alt="image" />
						</div>
						<h3>Sản phẩm nông nghiệp</h3>
						<p>Dolor sit amet, consectetur adipisicing elit impor incididunt ut labore et dolore magna.</p>
						<a href="javascript:;" class="clv_btn">read more</a>
					</div>
				</div>
				<div class="col-md-4 col-lg-4">
					<div class="feature_block">
						<div class="feature_img">
							<img src="/q_web/images/agri_dairy.png" alt="image" />
						</div>
						<h3>sản phẩm từ sữa tươi</h3>
						<p>Dolor sit amet, consectetur adipisicing elit impor incididunt ut labore et dolore magna.</p>
						<a href="javascript:;" class="clv_btn">read more</a>
					</div>
				</div>
				<div class="col-md-4 col-lg-4">
					<div class="feature_block">
						<div class="feature_img">
							<img src="/q_web/images/agri_organic.png" alt="image" />
						</div>
						<h3>phân bón hưu cơ</h3>
						<p>Dolor sit amet, consectetur adipisicing elit impor incididunt ut labore et dolore magna.</p>
						<a href="javascript:;" class="clv_btn">read more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Counter Section-->
	<div class="clv_counter_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading white_heading">
						<h3>we are an expert in this field</h3>
						<div class="clv_underline"><img src="/q_web/images/underline2.png" alt="image" /></div>
						<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
					</div>
				</div>
			</div>
			<div class="counter_section">
				<div class="row">
					<div class="col-lg-3 col-md-3">
						<div class="counter_block">
							<div class="counter_img">
								<span class="red_bg"><img src="/q_web/images/counter_customer.png" alt="image" class="img-fluid" /></span>
							</div>
							<div class="counter_text">
								<h4><span class="count_no" data-to="26" data-speed="3000">26</span><span>k+</span></h4>
								<h5>happy customers</h5>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="counter_block">
							<div class="counter_img">
								<span class="yellow_bg"><img src="/q_web/images/counter_project.png" alt="image" class="img-fluid" /></span>
							</div>
							<div class="counter_text">
								<h4><span class="count_no" data-to="700" data-speed="3000">700</span><span>+</span></h4>
								<h5>project complete</h5>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="counter_block">
							<div class="counter_img">
								<span class="orange_bg"><img src="/q_web/images/counter_branch.png" alt="image" class="img-fluid" /></span>
							</div>
							<div class="counter_text">
								<h4><span class="count_no" data-to="200" data-speed="3000">200</span><span>+</span></h4>
								<h5>world wide branch</h5>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3">
						<div class="counter_block">
							<div class="counter_img">
								<span class="blue_bg"><img src="/q_web/images/counter_winner.png" alt="image" class="img-fluid" /></span>
							</div>
							<div class="counter_text">
								<h4><span class="count_no" data-to="6" data-speed="3000">6</span><span>k+</span></h4>
								<h5>award winner</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Service 2-->
	<div class="garden_service2_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>discover services we provided</h3>
						<div class="clv_underline"><img src="/q_web/images/underline3.png" alt="image" /></div>
						<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
					</div>
				</div>
			</div>
			<div class="garden_service2_section">
				<div class="row">
					<div class="col-md-4">
						<div class="service2_block">
							<div class="service2_image"><img src="/q_web/images/garden_service4.png" alt="image" /></div>
							<div class="service2_content">
								<h3>Hedge Cutting</h3>
								<p>Dolor sit amet consectetur adipisicing elit sed do eiusmod tempor.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="service2_block">
							<div class="service2_image"><img src="/q_web/images/garden_service5.png" alt="image" /></div>
							<div class="service2_content">
								<h3>Garden Clean Up</h3>
								<p>Dolor sit amet consectetur adipisicing elit sed do eiusmod tempor.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="service2_block">
							<div class="service2_image"><img src="/q_web/images/garden_service6.png" alt="image" /></div>
							<div class="service2_content">
								<h3>Watering</h3>
								<p>Dolor sit amet consectetur adipisicing elit sed do eiusmod tempor.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="service2_block">
							<div class="service2_image"><img src="/q_web/images/garden_service7.png" alt="image" /></div>
							<div class="service2_content">
								<h3>Snow & Ice Removal</h3>
								<p>Dolor sit amet consectetur adipisicing elit sed do eiusmod tempor.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="service2_block">
							<div class="service2_image"><img src="/q_web/images/garden_service8.png" alt="image" /></div>
							<div class="service2_content">
								<h3>Tree Planting</h3>
								<p>Dolor sit amet consectetur adipisicing elit sed do eiusmod tempor.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="service2_block">
							<div class="service2_image"><img src="/q_web/images/garden_service9.png" alt="image" /></div>
							<div class="service2_content">
								<h3>Rush Removal</h3>
								<p>Dolor sit amet consectetur adipisicing elit sed do eiusmod tempor.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop