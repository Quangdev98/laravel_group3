
@extends('q_web.master')
@section('titlePage', 'Product-Detail')
@section('classPage', 'index_v1')
@section('content')
    <!--Product Single-->
    <form action="/cart_single/add" method="get">
    <div class="product_single_wrapper clv_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-7">
                    <div class="product_single_slider">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="fd_pro_thumnail">
                                    <div class="swiper-container gallery-thumbs">
                                        <div class="swiper-wrapper">
                                            @foreach ($imagepProductDetail as $pI)
                                                <div class="swiper-slide">
                                                    <div class="fd_pro_img_thumnail">
                                                        <img src="/uploads/products/details/{{ $pI->linksImage }}" alt="image">
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9">
                                <div class="fd_product_img">
                                    <div class="swiper-container gallery-top">
                                        <div class="swiper-wrapper">
                                            @foreach ($imagepProductDetail as $pI)
                                            <div class="swiper-slide">
                                                <div class="fd_pro_img">
                                                    <img src="/uploads/products/details/{{ $pI->linksImage }}" alt="image">
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 col-md-5">
                    <div class="product_single_details">
                        <div class="product_price_box">
                            <h3>{{ $productDetail->name }}</h3>
                        </div>
                        <p>{{ $productDetail->nameCate }}</p>
                        <div class="rating_section">
                            <span>4.5</span>
                            <ul>
                                <li><a class="active" href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a class="active" href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a class="active" href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a class="active" href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                            </ul>
                            <p>151 reviews</p>
                        </div>
                        <ul class="product_code">
                            <li>
                                <p>Mã sản phẩm: {{ $productDetail->id <10 ? '0' : ''}}{{ $productDetail->id }}</p>
                            </li>
                            <li>
                                <p>Trạng thái: <span>Còn hàng</span></p>
                            </li>
                        </ul>
                        <p>{{ $productDetail->contentHot }}</p>
                        <div class="product_prices">
                            <h2>{{ number_format($productDetail->priceSale, 0,'',',')}}Đ</h2>
                            @if ($productDetail->sale !== 0)
                                <h3>{{ number_format($productDetail->price,0,'',',') }}Đ</h3>
                                <span class="product_discount">{{ $productDetail->sale }}% off</span>
                            @endif
                        </div>
                        <div class="product_delivery">
{{--                            <p>--}}
{{--                                <span class="pro_icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>--}}
{{--                                <input type="text" placeholder="Enter your Delivery Pincode">--}}
{{--                                <a href="javascript:;" class="pin_check"> check</a>--}}
{{--                            </p>--}}
                            <p class="pro_cod"><span class="pro_iocn"><i class="fa fa-check-circle" aria-hidden="true"></i></span> COD Available</p>
                        </div>
                        <div class="fd_pro_quantity">
                            <div class="quantity_wrapper">
                                <div class="input-group">
                                    <span class="quantity_minus"> - </span>
                                    <input id="quantity" name="quantity" type="text" class="quantity" value="1">
                                    <span class="quantity_plus"> + </span>
                                </div>
                            </div>
                        </div>
                        <div class="fd_pro_add_btn">
                            <input type="hidden" name="id_product" value="{{$productDetail->id}}">
                            <p><button class="clv_btn" type="submit">add to cart</button></p>
                        </div>
                        <div>
                            <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="large">
                                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
    <!--Products Details-->
    <div class="product_detail_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="product_detail_tab">
                        <ul class="nav nav-tabs">
                            <li><a data-toggle="tab" class="active" href="#description">Description</a></li>
{{--                            <li><a data-toggle="tab" href="#info">Additional Information</a></li>--}}
{{--                            <li><a data-toggle="tab" href="#review">Review</a></li>--}}
                        </ul>
                        <div class="tab-content">
                            <div id="description" class="tab-pane fade show active">
                                {!! $productDetail->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
