<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Cultivation</title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="kamleshyadav">
    <meta name="MobileOptimized" content="320">
    <!--Start Style -->
    <base href="{{ asset('') }}">
    <link rel="stylesheet" type="text/css" href="q_web/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="q_web/css/font.css">
    <link rel="stylesheet" type="text/css" href="q_web/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="q_web/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="q_web/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="q_web/css/layers.css">
    <link rel="stylesheet" type="text/css" href="q_web/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="q_web/css/settings.css">
    <link rel="stylesheet" type="text/css" href="q_web/css/range.css">
    <link rel="stylesheet" type="text/css" href="q_web/css/nice-select.css">
    <link rel="stylesheet" type="text/css" href="q_web/css/style.css">
    <link rel="shortcut icon" type="image/png" href="q_web/images/favicon.png">
</head>
<body>
	{{-- <div class="preloader_wrapper">
		<div class="preloader_inner">
			<img src="q_web/images/preloader.gif" alt="image" />
		</div>
	</div> --}}
<div class="clv_main_wrapper index_v5">
	<div class="clv_header">
		<span class="header_shape">
			<svg
			 xmlns="http://www.w3.org/2000/svg"
			 xmlns:xlink="http://www.w3.org/1999/xlink"
			 width="1920px" height="135px">
			<path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
			 d="M0.000,100.999 C0.000,100.999 217.931,100.999 284.971,100.999 C352.010,100.999 392.861,134.998 445.971,134.998 C499.080,134.998 544.349,100.999 609.971,100.999 C666.112,100.999 1920.000,100.999 1920.000,100.999 L1920.000,-0.000 L0.000,-0.000 L0.000,100.999 Z"/>
			</svg>
		</span>
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3">
					<div class="clv_left_header">
						<div class="clv_logo">
							<a href="{{ route('web.index') }}"><img src="q_web/images/logo5.png" alt="Cultivation" /></a>
						</div>
					</div>
				</div>
				<div class="col-lg-9 col-md-9">
					<div class="clv_right_header">
						@include('q_web.layout.menu')
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('q_web.layout.slide')
	<!--Services-->
	<div class="org_service_wrapper clv_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="org_left_service">
						<div class="service_description">
							<h3>Sản phẩm hữu cơ tuyệt vời</h3>
							<img src="q_web/images/org_underline.png" alt="image" />
							<p>Nông nghiệp Hữu cơ (NNHC) là một hệ thống, trong đó từ chối sử dụng các loại phân bón vô cơ, thuốc BVTV và cả cây trồng biến đổi gen. Hệ canh tác này hướng vào sử dụng phân bón hữu cơ, Làm cỏ bằng cơ giới và quản lý dịch hại bằng biện pháp sinh học .</p>
						</div>
						@foreach($phoneCategory as $list)
						<div class="service_contact">
							<span>
								<?xml version="1.0" encoding="iso-8859-1"?>
								<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 viewBox="0 0 480.56 480.56" style="enable-background:new 0 0 480.56 480.56;" xml:space="preserve"  width="32px" height="32px">
								<g>
									<g>
										<path style="fill:#27ae93;" d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8
											c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4
											c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8
											c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3
											c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9
											c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z"/>
										<path style="fill:#27ae93;" d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1
											c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z"/>
										<path style="fill:#27ae93;" d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5
											l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z"/>
									</g>
								</g>
								</svg>
							</span>
							<h4>{{$list->phone}}</h4>
						</div>
							@endforeach
					</div>
				</div>
				<div class="col-lg-8 col-md-8">
					<div class="org_right_service">
						<div class="row">
							<div class="col-md-4">
								<div class="service_block">
									<img src="q_web/images/org_service1.png" alt="image" />
									<h3>Rau</h3>
									<p>Rau tươi giàu vitamin nhất là vitamin A và C ... đặc biệt chưa nhiều chất sơ tốt cho tiêu hóa .</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="service_block">
									<img src="q_web/images/org_service2.png" alt="image" />
									<h3>Hoa quả</h3>
									<p>Hoa quả giàu dinh dưỡng chứa nhiều protein, vitamin B,  C, kali và các chất oxy hóa.</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="service_block">
									<img src="q_web/images/org_service3.png" alt="image" />
									<h3>Organic Pulses</h3>
									<p>Organic Vegetables umption top listeie Vegetables.</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="service_block">
									<img src="q_web/images/org_service4.png" alt="image" />
									<h3>Sữa</h3>
									<p>Sữa tươi đặc biệt tốt chứa chất béo, protein, đường lactose, vitamin, khoáng chất.</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="service_block">
									<img src="q_web/images/org_service5.png" alt="image" />
									<h3>Thịt</h3>
									<p>Thịt là nguồn cung vitamin phong phú như vitamin B1, B2, B5, B6, B12 và PP.</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="service_block">
									<img src="q_web/images/org_service6.png" alt="image" />
									<h3>Trứng</h3>
									<p>Trứng chứa các chất khoáng như sắt, kẽm, đồng, mangan, i-ot... Các vitamin như B1, B6, A, D, K .</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--About Us-->
	<div class="org_about_wrapper clv_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="org_about_image">
						<img src="q_web/images/unnamed.jpg" alt="image" />
					</div>
				</div>
				@foreach($phoneCategory2 as $list)
				<div class="col-lg-6 col-md-6">
					<div class="org_about_contents">
						<h5>Chất lượng đáng tin cậy theo thời gian</h5>
						<h2>Hoa quả trái cây và rau được giao hàng tuần trực tiếp đến cửa hàng đảm bảo luôn tươi mới</h2>
						<img src="q_web/images/org_underline2.png" alt="image" />
						<p>Cửa hàng nông sản sạch shopnongsansach.com là một website cung cấp thực phẩm an toàn, nông sản sạch cho người dân. Với mong muốn mang tới mọi gia đình những thực phẩm an toàn nhất trước mối lo ngại về thực phẩm bẩn tại Việt Nam.</p>
						<p>Cùng chung tay xây dựng một cuộc sống an toàn và chất lượng cho người dân Việt Nam.</p>
						<a href="javascript:;" class="clv_btn">Khám phá thêm</a>
						<div class="org_support">
							<h6>Hỗ trợ thân thiện và tận tâm trong mọi bước trên đường đi</h6>
							<h3>{{$list->phone}}</h3>
						</div>
					</div>
				</div>
					@endforeach
			</div>
		</div>
	</div>
	<!--Products-->
	<div class="org_product_wrapper clv_section">

		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>sản phẩm của chúng tôi</h3>
						<div class="clv_underline"><img src="q_web/images/org_underline2.png" alt="image" /></div>
						<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
					</div>
				</div>
			</div>
			<div class="org_product_section">
				<div class="row">
                    @foreach ($productsOur as $pro)

						<div class="col-md-4">
                            <form action="/cart_single/add" method="GET">
                                <div class="org_product_block">
                                    @if ($pro->sale !== 0)
                                        <span class="product_label">{{ $pro->sale }}% 0ff</span>
                                    @endif
                                    <div class="org_product_image">
                                        <a href="{{ route('web.product_single',['slug'=>$pro->slug]) }}" title="">
                                            <img src="/uploads/products/{{ $pro->image }}" alt="image" />
                                        </a>
                                    </div>
                                    <a href="{{ route('web.product_single',['slug'=>$pro->slug]) }}" title="" class="titleName"><h4>{{ $pro->name }}</h4></a>
                                    <h3><span></span>{{ number_format($pro->price, 0,'',',') }}Đ</h3>
                                    {{-- <a class="addCart" href="javascript:;">add to cart</a> --}}
                                    <input type="hidden" name="id_product" value="{{$pro->id}}">
                                    <button class="addCart" type="submit">Thêm giỏ hàng</button>
                                </div>
                            </form>
                        </div>
					@endforeach
				</div>
				<div class="load_more_btn"><a href="{{ route('web.products') }}" class="clv_btn">view more</a></div>
			</div>
		</div>

    </div>

	<!--Team-->
	<div class="org_team_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>đội ngũ của chúng tôi</h3>
						<div class="clv_underline"><img src="q_web/images/org_underline2.png" alt="image" /></div>
						<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
					</div>
				</div>
			</div>
			<div class="org_team_section">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="dairy_team_slider org_team_slider">
							<div class="swiper-container">
								<div class="swiper-wrapper">
									@foreach ($author as $au)
									<div class="swiper-slide">
										<div class="org_team_slide">
											<div class="org_team_image">
												<img src="/uploads/avatars/{{ $au->image }}" alt="image" />
												<div class="org_team_overlay">
													<ul class="org_social_links">
														<li><a href="javascript:;"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
														<li><a href="javascript:;"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
														<li><a href="javascript:;"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a></li>
														<li><a href="javascript:;"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a></li>
													</ul>
												</div>
											</div>
											<div class="org_team_name">
												<h4>{{ $au->name }}</h4>
												<p>
													{{ $au->level == 1? 'Maintainer' : '' }}
													{{ $au->level == 2? 'Producter' : '' }}
													{{ $au->level == 3? 'Post-Management' : '' }}
												</p>
												<div class="org_social">
													<ul class="org_social_links">
														<li><a href="javascript:;"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
														<li><a href="javascript:;"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
														<li><a href="javascript:;"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a></li>
														<li><a href="javascript:;"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a></li>
													</ul>
													<p>{{ $au->phone }}</p>
												</div>
											</div>
										</div>
									</div>
									@endforeach

								</div>
								<!-- Add Pagination -->
								<div class="swiper-pagination"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Products-->
	<div class="org_product_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>sản phẩm mới</h3>
						<div class="clv_underline"><img src="q_web/images/org_underline2.png" alt="image" /></div>
						<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
					</div>
				</div>
			</div>
			<div class="org_product_section">
				<div class="row">
					@foreach ($productsNew as $pr)
					<div class="col-md-4">
                        <form action="/cart_single/add" method="GET">
						<div class="org_product_block">
							@if ($pr->sale !== 0)
								<span class="product_label">{{ $pr->sale }}% off</span>
							@endif
							<div class="org_product_image">
								<a href="{{ route('web.product_single',['slug'=>$pr->slug]) }}" title="">
									<img src="/uploads/products/{{ $pr->image }}" alt="image" />
								</a>
							</div>
							<a href="{{ route('web.product_single',['slug'=>$pr->slug]) }}" title="" class="titleName"><h4>{{ $pr->name }}</h4></a>
							<h3><span></span>{{ number_format($pr->price, 0,'',',') }}Đ</h3>
                            {{-- <a class="addCart" href="javascript:;">add to cart</a> --}}
                            <input type="hidden" name="id_product" value="{{$pr->id}}">
                            <button class="addCart" type="submit">Thêm giỏ hàng</button>
                        </div>
                        </form>
					</div>
					@endforeach

				</div>
				<div class="load_more_btn"><a href="{{ route('web.products') }}" class="clv_btn">view more</a></div>
			</div>
		</div>
	</div>
	<!--Testimonial-->
	<div class="org_testimonial_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>Chúng tôi đã tạo ra nó cho bạn</h3>
						<div class="clv_underline"><img src="q_web/images/org_underline2.png" alt="image" /></div>
						<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-10">
					<div class="org_testimonial_slider">
						<div class="swiper-container">
                            <div class="swiper-wrapper">
                                @foreach ($opinion as $op)
                                    <div class="swiper-slide">
                                        <div class="org_testimonial_slide">
                                            <div class="org_test_image">
                                                <img src="/uploads/opinion/{{ $op->image }}" alt="image" style="width: 180px;height: 220px; object-fit: cover"/>
                                            </div>
                                            <div class="org_testimonial_message">
                                                <img src="q_web/images/bg_quote2.png" alt="image" />
                                                <p>{{ $op->content }}</p>
                                                <h5>{{ $op->name }} <span>- {{ $op->cate }}</span></h5>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>
							<!-- Add Arrows -->
							<div class="org_test_btn org_left">
								<span class="arrow">

									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129" width="15px" height="15px">
									  <g>
										<path d="m88.6,121.3c0.8,0.8 1.8,1.2 2.9,1.2s2.1-0.4 2.9-1.2c1.6-1.6 1.6-4.2 0-5.8l-51-51 51-51c1.6-1.6 1.6-4.2 0-5.8s-4.2-1.6-5.8,0l-54,53.9c-1.6,1.6-1.6,4.2 0,5.8l54,53.9z"/>
									  </g>
									</svg>
								</span>
								<span class="hover_arrow">
									<?xml version="1.0" encoding="iso-8859-1"?>
									<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 viewBox="0 0 31.494 31.494" style="enable-background:new 0 0 31.494 31.494;" xml:space="preserve" width="20px" height="20px">
									<path style="fill:#27ae93;" d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554
										c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587
										c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/>
									</svg>
								</span>
							</div>
							<div class="org_test_btn org_right">
								<span class="arrow">

									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129" width="15px" height="15px">
									  <g>
										<path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/>
									  </g>
									</svg>
								</span>
								<span class="hover_arrow">
									<?xml version="1.0" encoding="iso-8859-1"?>
									<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve" width="20px" height="20px">
									<path style="fill:#27ae93;" d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
										C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
										c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
									</svg>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Pricing Table-->
	{{-- <div class="garden_pricing_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>Bảng giá</h3>
						<div class="clv_underline"><img src="q_web/images/garden_underline.png" alt="image" /></div>
						<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
					</div>
				</div>
			</div>
			<div class="pricing_section">
				<div class="row">
					<div class="col-lg-4 col-md-4">
						<div class="pricing_block">
							<div class="pricing_header">
								<h3>basic</h3>
							</div>
							<h1><span><i class="fa fa-usd" aria-hidden="true"></i></span>20</h1>
							<ul>
								<li>
									<p>Install a Patio or Pathway</p>
								</li>
								<li>
									<p>Install Landscaping</p>
								</li>
								<li>
									<p>Waterproof a Deck Costs</p>
								</li>
								<li>
									<p>Remove a Tree Stump</p>
								</li>
							</ul>
							<a href="javascript:;">purchase</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="pricing_block">
							<div class="pricing_header premium">
								<h3>premium</h3>
							</div>
							<h1><span><i class="fa fa-usd" aria-hidden="true"></i></span>20</h1>
							<ul>
								<li>
									<p>Install a Patio or Pathway</p>
								</li>
								<li>
									<p>Install Landscaping</p>
								</li>
								<li>
									<p>Waterproof a Deck Costs</p>
								</li>
								<li>
									<p>Remove a Tree Stump</p>
								</li>
							</ul>
							<a href="javascript:;">purchase</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="pricing_block">
							<div class="pricing_header ultimate">
								<h3>ultimate</h3>
							</div>
							<h1><span><i class="fa fa-usd" aria-hidden="true"></i></span>20</h1>
							<ul>
								<li>
									<p>Install a Patio or Pathway</p>
								</li>
								<li>
									<p>Install Landscaping</p>
								</li>
								<li>
									<p>Waterproof a Deck Costs</p>
								</li>
								<li>
									<p>Remove a Tree Stump</p>
								</li>
							</ul>
							<a href="javascript:;">purchase</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> --}}
	<!--Partner-->
	<div class="clv_partner_wrapper clv_section slideBanner">
		<div class="container">
			{{--  --}}
			@include('q_web.layout.slideBanner')
			{{--  --}}
		</div>
	</div>
	<!--Footer-->
	<div class="clv_footer_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-9">
					<div class="footer_service_wrapper">
						<div class="row">
							<div class="col-md-4">
								<div class="footer_service_block">
									<img src="q_web/images/guarantee_icon.png" alt="image" />
									<h4>100% hài lòng</h4>
								</div>
							</div>
							<div class="col-md-4">
								<div class="footer_service_block">
									<img src="q_web/images/shiping_icon1.png" alt="image" />
									<h4>giao hàng miễn phí</h4>
								</div>
							</div>
							<div class="col-md-4">
								<div class="footer_service_block">
									<img src="q_web/images/discount_icon.png" alt="image" />
									<h4>giảm giá mỗi ngày</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-lg-3">
					<div class="footer_block">
						<div class="footer_logo"><a href="javascript:;"><img src="q_web/images/footer_logo5.png" alt="image" /></a></div>
						<p><span><svg
								 xmlns="http://www.w3.org/2000/svg"
								 xmlns:xlink="http://www.w3.org/1999/xlink"
								 width="16px" height="16px">
								<defs>
								<filter id="Filter_0">
									<feFlood flood-color="#27ae93" flood-opacity="1" result="floodOut" />
									<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
									<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
								</filter>

								</defs>
								<g filter="url(#Filter_0)">
								<path fill-rule="evenodd"  fill="rgb(81, 176, 30)"
								 d="M14.873,0.856 C14.815,0.856 14.700,0.856 14.643,0.913 L0.850,6.660 C0.620,6.776 0.505,6.948 0.505,7.176 C0.505,7.465 0.677,7.695 0.965,7.752 L6.942,9.189 C7.057,9.189 7.114,9.305 7.172,9.419 L8.608,15.396 C8.666,15.626 8.896,15.855 9.183,15.855 C9.413,15.855 9.643,15.683 9.700,15.511 L15.447,1.718 C15.447,1.660 15.505,1.603 15.505,1.488 C15.447,1.085 15.217,0.856 14.873,0.856 ZM9.355,8.902 L9.068,7.695 C9.011,7.465 8.838,7.350 8.666,7.292 L7.459,7.005 C7.172,6.948 7.172,6.545 7.401,6.487 L11.022,4.993 C11.252,4.878 11.482,5.109 11.424,5.395 L9.930,9.017 C9.758,9.189 9.413,9.131 9.355,8.902 Z"/>
								</g>
								</svg></span>{{ $infoHeader->address }}</p>
						<p><span><svg 
								 xmlns="http://www.w3.org/2000/svg"
								 xmlns:xlink="http://www.w3.org/1999/xlink"
								 width="16px" height="15px">

								<g filter="url(#Filter_0)">
								<path fill-rule="evenodd" fill="#27ae93"
								 d="M13.866,7.235 C13.607,5.721 12.892,4.344 11.802,3.254 C10.653,2.108 9.197,1.381 7.592,1.156 L7.755,-0.002 C9.613,0.257 11.296,1.096 12.626,2.427 C13.888,3.692 14.716,5.284 15.019,7.039 L13.866,7.235 ZM10.537,4.459 C11.296,5.222 11.796,6.181 11.977,7.238 L10.824,7.436 C10.684,6.617 10.300,5.874 9.713,5.287 C9.091,4.666 8.304,4.276 7.439,4.155 L7.601,2.996 C8.719,3.151 9.734,3.657 10.537,4.459 ZM4.909,8.182 C5.709,9.162 6.611,10.033 7.689,10.711 C7.920,10.854 8.176,10.960 8.417,11.092 C8.538,11.160 8.623,11.139 8.723,11.035 C9.088,10.661 9.460,10.293 9.831,9.924 C10.318,9.440 10.931,9.440 11.421,9.924 C12.017,10.516 12.614,11.110 13.207,11.707 C13.704,12.207 13.701,12.818 13.201,13.324 C12.864,13.665 12.505,13.989 12.186,14.345 C11.721,14.866 11.140,15.035 10.472,14.997 C9.500,14.944 8.607,14.623 7.745,14.205 C5.831,13.275 4.194,11.985 2.823,10.355 C1.808,9.150 0.971,7.834 0.422,6.355 C0.153,5.639 -0.038,4.906 0.022,4.129 C0.059,3.651 0.237,3.242 0.590,2.907 C0.971,2.546 1.330,2.168 1.705,1.800 C2.192,1.319 2.804,1.319 3.295,1.797 C3.598,2.093 3.894,2.396 4.194,2.696 C4.485,2.988 4.775,3.277 5.065,3.570 C5.578,4.085 5.578,4.684 5.069,5.197 C4.703,5.565 4.341,5.933 3.969,6.293 C3.873,6.390 3.863,6.468 3.913,6.586 C4.160,7.173 4.513,7.694 4.909,8.182 Z"/>
								</g>
								</svg></span> {{ $infoHeader->phone }}</p>
						<p><span><svg 
							 xmlns="http://www.w3.org/2000/svg"
							 xmlns:xlink="http://www.w3.org/1999/xlink"
							 width="16px" height="16px">
							<defs>
							<filter id="Filter_0">
								<feFlood flood-color="#27ae93" flood-opacity="1" result="floodOut" />
								<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
								<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
							</filter>

							</defs>
							<g filter="url(#Filter_0)">
							<path fill-rule="evenodd"  fill="#27ae93"
							 d="M16.000,5.535 C16.000,4.982 15.680,4.507 15.280,4.191 L8.000,-0.002 L0.720,4.191 C0.320,4.507 0.000,4.982 0.000,5.535 L0.000,13.447 C0.000,14.317 0.720,15.028 1.600,15.028 L14.400,15.028 C15.280,15.028 16.000,14.317 16.000,13.447 L16.000,5.535 ZM8.000,9.491 L1.360,5.376 L8.000,1.579 L14.640,5.376 L8.000,9.491 Z"/>
							</g>
							</svg></span> {{ $infoHeader->email }}</p>
						<ul class="agri_social_links">
							<li><a href="javascript:;"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
							<li><a href="javascript:;"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
							<li><a href="javascript:;"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a></li>
							<li><a href="javascript:;"><span><i class="fa fa-youtube-play" aria-hidden="true"></i></span></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-2 col-lg-2">
					<div class="footer_block">
						<div class="footer_heading">
							<h4>thông tin</h4>
							<img src="q_web/images/org_underline3.png" alt="image" />
						</div>
						<ul class="useful_links">
							<li><a href="{{ route('web.products') }}"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>cửa hàng</a></li>
							{{-- <li><a href="javascript:;"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>Our Range</a></li> --}}
							<li><a href="{{ route('web.about') }}"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>giới thiệu</a></li>
							<li><a href="{{ route('web.blog') }}"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>tin tức</a></li>
							<li><a href="{{ route('web.contact') }}"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>liên hệ </a></li>
						</ul>
						
					</div>
				</div>
				<div class="col-md-4 col-lg-4">
					<div class="footer_block">
						<div class="footer_heading">
							<h4>thể loại</h4>
							<img src="q_web/images/org_underline3.png" alt="image" />
						</div>
						<ul class="useful_links">
							@foreach ($cate as $c)
								<li><a href="{{ route('web.products',['slug'=>$c->slug]) }}" title="{{ $c->slug }}"><span><i class="fa fa-angle-right" aria-hidden="true"></i></span>{{ $c->name }}</a></li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-lg-3">
					<div class="footer_block">
						<div class="footer_heading">
							<h4>tin mới nhất</h4>
							<img src="q_web/images/org_underline3.png" alt="image" />
						</div>
						<div class="footer_post_section">
							@foreach ($postFooter as $p)
								<div class="footer_post_slide">
									<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
									<div class="blog_links">
										<a href="{{ route('web.blog-single',['slug'=>$p->slug]) }}" title="{{ $p->slug }}">
											<p class="lessText">{{ $p->title }}</p>
										</a>
										<a class="lessText2" href="{{ route('web.blog-single',['slug'=>$p->slug]) }}" title="{{ $p->slug }}">https://cultivation.comm/bai-viet/{{ $p->slug }}.html</a>
									</div>
								</div>
							@endforeach
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Copyright-->
	<div class="clv_copyright_wrapper">
		<p>copyright &copy; 2019 <a href="javascript:;">cultivation.</a> all right reserved.</p>
	</div>

	<!--Popup-->
	<div class="search_box">
		<div class="search_block">
			<h3>Tìm kiếm sản phẩm</h3>
			<div class="search_field">
                <form action="{{ route('web.products') }}" method="get">
                    @csrf
                    <input type="text" name="search" placeholder="Tìm kiếm sản phẩm">
                    <button type="submit" class="submit">search</button>
                </form>
			</div>
		</div>
		<span class="search_close">
			<?xml version="1.0" encoding="iso-8859-1"?>
			<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"  width="30px" height="30px">
			<g>
				<path style="fill:#27ae93;" d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
					c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
					C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
					s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/>
			</g>
			</svg>
		</span>
	</div>
    <!--Payment Success Popup-->
    <div class="success_wrapper">
        <div class="success_inner">
            <div class="success_img"><img src="q_web/images/success.png" alt=""></div>
            <h3>payment success</h3>
            <img src="q_web/images/clv_underline.png" alt="">
            <p>Your order has been successfully processed! Please direct any questions you have to the store owner. Thanks for shopping</p>
            <a href="javascript:;" class="clv_btn">continue browsing</a>
            <span class="success_close">
                <?xml version="1.0" encoding="iso-8859-1"?>
                <svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 212.982 212.982" style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="11px" height="11px" >
                <g>
                    <path fill="#fec007" style="fill-rule:evenodd;clip-rule:evenodd;" d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312
                        c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312
                        l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937
                        c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"/>
                </g>
                </svg>
            </span>
        </div>
    </div>
    <!--Thank You Popup-->
    <div class="thankyou_wrapper">
        <div class="thankyou_inner">
            <div class="thankyou_img"><img src="q_web/images/thankyou.png" alt=""></div>
            <h3>your order is being processed</h3>
            <h5>We Have Just Sent You An Email With Complete Information About Your Booking</h5>
            <div class="download_button">
                <a href="javascript:;" class="clv_btn">download PDF</a>
                <a href="index.html" class="clv_btn">back to site</a>
            </div>
            <span class="success_close">
                <?xml version="1.0" encoding="iso-8859-1"?>
                <svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 212.982 212.982" style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="11px" height="11px" >
                <g>
                    <path fill="#fec007" style="fill-rule:evenodd;clip-rule:evenodd;" d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312
                    c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312
                    l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937
                    c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"/>
                </g>
                </svg>
            </span>
        </div>
    </div>

{{--    <div class="profile_toggle"><a href="javascript:;"><img src="q_web/images/login.gif" alt=""></a></div>--}}
</div>
<!--Main js file Style-->
<script src="q_web/js/jquery.js"></script>
<script src="q_web/js/bootstrap.min.js"></script>
<script src="q_web/js/swiper.min.js"></script>
<script src="q_web/js/magnific-popup.min.js"></script>
<script src="q_web/js/jquery.themepunch.tools.min.js"></script>
<script src="q_web/js/jquery.themepunch.revolution.min.js"></script>
<script src="q_web/js/jquery.appear.js"></script>
<script src="q_web/js/jquery.countTo.js"></script>
<script src="q_web/js/isotope.min.js"></script>
<script src="q_web/js/nice-select.min.js"></script>
<script src="q_web/js/range.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="q_web/js/revolution.extension.actions.min.js"></script>
<script src="q_web/js/revolution.extension.kenburn.min.js"></script>
<script src="q_web/js/revolution.extension.layeranimation.min.js"></script>
<script src="q_web/js/revolution.extension.migration.min.js"></script>
<script src="q_web/js/revolution.extension.parallax.min.js"></script>
<script src="q_web/js/revolution.extension.slideanims.min.js"></script>
<script src="q_web/js/revolution.extension.video.min.js"></script>
<script src="q_web/js/custom.js"></script>
<script src="/q_web/js/rutgon.js"></script>
</body>
</html>
