@extends('q_web.master')
@section('titlePage', 'bộ sưu tập')
@section('classPage', 'gallery_page')
@section('content')
	<!--Gallery-->
	<div class="clv_gallery_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>our gallery</h3>
						<div class="clv_underline"><img src="images/underline3.png" alt="image" /></div>
						<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dole magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="gallery_slide">
						<div class="gallery_grid">
							<div class="gallery_grid_item">
								<div class="gallery_image">
									<img src="q_web/images/thit-tuo222i-sach.jpg" alt="image" />
									<div class="gallery_overlay">
										<a href="q_web/images/thit-tuoi-sach.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
									</div>
								</div>
							</div>
							<div class="gallery_grid_item">
								<div class="gallery_image">
									<img src="q_web/images/Buoi-Ddsdsien-Xin.jpg" alt="image" />
									<div class="gallery_overlay">
										<a href="q_web/images/Buoi-Diensad231-Xin.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
									</div>
								</div>
							</div>
							<div class="gallery_grid_item">
								<div class="gallery_image">
									<img src="q_web/images/unnamed (655665).jpg" alt="image" />
									<div class="gallery_overlay">
										<a href="q_web/images/rau-xanh-2dsdsds112.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
									</div>
								</div>
							</div>
							<div class="gallery_grid_item">
								<div class="gallery_image">
									<img src="q_web/images/no-ro-mo-hinh-thue-dat-trongJHBH-rau-tai-ha-noi-2.jpg" alt="image" />
									<div class="gallery_overlay">
										<a href="q_web/images/no-ro-mo-hinh-thue-dat-trong-rau-tajhhhjkjki-ha-noi-2.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
									</div>
								</div>
							</div>
							<div class="gallery_grid_item">
								<div class="gallery_image">
									<img src="q_web/images/đas-trung-ga-ta.jpg" alt="image" />
									<div class="gallery_overlay">
										<a href="q_web/images/dâs-trung-ga-ta.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
									</div>
								</div>
							</div>
							<div class="gallery_grid_item">
								<div class="gallery_image">
									<img src="q_web/images/img2097154598581861414.jpg" alt="image" />
									<div class="gallery_overlay">
										<a href="q_web/images/img20200523174452-1595597154598581861414.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
									</div>
								</div>
							</div>
							<div class="gallery_grid_item">
								<div class="gallery_image">
									<img src="q_web/images/dua-xiehgm.jpg" alt="image" />
									<div class="gallery_overlay">
										<a href="q_web/images/adsadsa.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
									</div>
								</div>
							</div>
							<div class="gallery_grid_item">
								<div class="gallery_image">
									<img src="q_web/images/duađá-xiem.jpg" alt="image" />
									<div class="gallery_overlay">
										<a href="q_web/images/hinh-anh-cay-mit-to-đasanu.jpg" class="view_image"><span><i class="fa fa-search" aria-hidden="true"></i></span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="load_more_btn">
						<a href="javascript:;" class="clv_btn">view more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop