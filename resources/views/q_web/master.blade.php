<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Cultivation</title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="kamleshyadav">
    <meta name="MobileOptimized" content="320">
    <link rel="stylesheet" type="text/css" href="/q_web/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/q_web/css/font.css">
    <link rel="stylesheet" type="text/css" href="/q_web/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/q_web/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="/q_web/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="/q_web/css/layers.css">
    <link rel="stylesheet" type="text/css" href="/q_web/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="/q_web/css/settings.css">
    <link rel="stylesheet" type="text/css" href="/q_web/css/range.css">
    <link rel="stylesheet" type="text/css" href="/q_web/css/nice-select.css">
    <link rel="stylesheet" type="text/css" href="/q_web/css/style.css">
    <link rel="shortcut icon" type="image/png" href="/q_web/images/favicon.png">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
    @yield('linkCss')
</head>
<body class="index_v1 woocommerce-cart">
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    {{-- <div class="preloader_wrapper">
        <div class="preloader_inner">
            <img src="/q_web/images/preloader.gif" alt="image" />
        </div>
    </div> --}}
    <div class="clv_main_wrapper @yield('classPage')">

      @include('q_web.layout.header')

        <div class="breadcrumb_wrapper" style="background: url(/q_web/images/banner.jpg) no-repeat center;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <div class="breadcrumb_inner">
                            <h3>@yield('titlePage')</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="breadcrumb_block">
                <ul>
                    <li><a href="{{route('web.index')}}">home</a></li>
                    <li>@yield('titlePage')</li>
                </ul>
            </div>
        </div>
        <!--Partners-->
        <!--  -->
        @yield('content')
        <!--  -->
        <div class="clv_partner_wrapper clv_section slideBanner">
            <div class="container">
                {{--  --}}
                @include('q_web.layout.slideBanner')
                {{--  --}}
                <div class="clv_newsletter_wrapper">
                    <div class="newsletter_text">
                        <h2>get update from <br/>anywhere</h2>
                        <h4>subscribe us to get more info</h4>
                    </div>
                    <div class="newsletter_field">
                        <h3>don't miss out on the good news!</h3>
                        <div class="newsletter_field_block">
                            <input type="text" placeholder="Enter Your Email Here" />
                            <a href="javascript:;">subscribe now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('q_web.layout.footer')

        <!--Popup-->
        <div class="search_box">
            <div class="search_block">
                <h3>Tìm kiếm sản phẩm</h3>
                <div class="search_field">
                    <form action="{{ route('web.products') }}" method="get">
                        @csrf
                        <input type="text" name="search" placeholder="Tìm kiếm sản phẩm">
                        <button type="submit" class="submit">search</button>
                    </form>
                </div>
            </div>
            <span class="search_close">
                <?xml version="1.0" encoding="iso-8859-1"?>
                <svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"  width="30px" height="30px">
                <g>
                    <path style="fill:#fec007;" d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
                        c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
                        C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
                        s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/>
                </g>
                </svg>
            </span>
        </div>
        <!--Payment Success Popup-->
        <div class="success_wrapper">
            <div class="success_inner">
                <div class="success_img"><img src="/q_web/images/success.png" alt=""></div>
                <h3>payment success</h3>
                <img src="/q_web/images/clv_underline.png" alt="">
                <p>Your order has been successfully processed! Please direct any questions you have to the store owner. Thanks for shopping</p>
                <a href="javascript:;" class="clv_btn">continue browsing</a>
                <span class="success_close">
                    <?xml version="1.0" encoding="iso-8859-1"?>
                    <svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 212.982 212.982" style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="11px" height="11px" >
                    <g>
                        <path fill="#fec007" style="fill-rule:evenodd;clip-rule:evenodd;" d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312
                            c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312
                            l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937
                            c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"/>
                    </g>
                    </svg>
                </span>
            </div>
        </div>
        <!--Thank You Popup-->
        <div class="thankyou_wrapper">
            <div class="thankyou_inner">
                <div class="thankyou_img"><img src="/q_web/images/thankyou.png" alt=""></div>
                <h3>your order is being processed</h3>
                <h5>We Have Just Sent You An Email With Complete Information About Your Booking</h5>
                <div class="download_button">
                    <a href="javascript:;" class="clv_btn">download PDF</a>
                    <a href="index.html" class="clv_btn">back to site</a>
                </div>
                <span class="success_close">
                    <?xml version="1.0" encoding="iso-8859-1"?>
                    <svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 212.982 212.982" style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="11px" height="11px" >
                    <g>
                        <path fill="#fec007" style="fill-rule:evenodd;clip-rule:evenodd;" d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312
                        c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312
                        l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937
                        c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"/>
                    </g>
                    </svg>
                </span>
            </div>
        </div>
        <!--SignUp Popup-->

        <!--SignIn Popup-->

{{--        <div class="profile_toggle"><a href="javascript:;"><img src="/q_web/images/login.gif" alt=""></a></div>--}}
    </div>
    <script src="/q_web/js/jquery.js"></script>
    <script src="/q_web/js/bootstrap.min.js"></script>
    <script src="/q_web/js/swiper.min.js"></script>
    <script src="/q_web/js/magnific-popup.min.js"></script>
    <script src="/q_web/js/jquery.themepunch.tools.min.js"></script>
    <script src="/q_web/js/jquery.themepunch.revolution.min.js"></script>
    <script src="/q_web/js/jquery.appear.js"></script>
    <script src="/q_web/js/jquery.countTo.js"></script>
    <script src="/q_web/js/isotope.min.js"></script>
    <script src="/q_web/js/nice-select.min.js"></script>
    <script src="/q_web/js/range.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="/q_web/js/revolution.extension.actions.min.js"></script>
    <script src="/q_web/js/revolution.extension.kenburn.min.js"></script>
    <script src="/q_web/js/revolution.extension.layeranimation.min.js"></script>
    <script src="/q_web/js/revolution.extension.migration.min.js"></script>
    <script src="/q_web/js/revolution.extension.parallax.min.js"></script>
    <script src="/q_web/js/revolution.extension.slideanims.min.js"></script>
    <script src="/q_web/js/revolution.extension.video.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
    @yield('linkJavasript')
    <script src="/q_web/js/custom.js"></script>
    <script src="/q_web/js/rutgon.js"></script>
</body>
