@extends('q_web.master')
@section('titlePage', 'Sản Phẩm')
@section('content')
    <!--Breadcrumb-->
    <!--Produst List-->
    <div class="products_wrapper clv_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="product_sidebar">
                        <div class="product_block">
                            <div class="sidebar_heading">
                                <h3>Tìm kiếm</h3>
                                <img src="/q_web/images/footer_underline.png" alt="image">
                            </div>
                            <div class="sidebar_search">
                                <form action="{{ route('web.products') }}" method="get">
                                    @csrf
                                    <input type="text" name="search" placeholder="Tìm kiếm sản phẩm">
                                    <button type="submit" class="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="product_block">
                            <div class="sidebar_heading">
                                <h3>Danh mục sản <phẩm></phẩm></h3>
                                <img src="/q_web/images/footer_underline.png" alt="image">
                            </div>
                            <div class="product_category">
                                <ul>
                                    <li>
                                        {{-- <input type="checkbox" id="cat0" checked> --}}
                                        <a href="{{ route('web.products') }}" title="">
                                            <label for="cat0">All<span>({{ $countPro }})</span></label>
                                        </a>
                                    </li>
                                    @foreach ($countCate as $cc)
                                    <li id="product{{ $cc->id }}" >
                                        <a href="{{ route('web.products',['slug'=>$cc->slug]) }}" title="{{ $cc->slug }}">
                                            {{-- <input type="checkbox" id="cat{{ $cc->id }}">
                                             --}}<label for="cat{{ $cc->id }}">{{ $cc->name }}<span class="numberCount">({{ $cc->count_product }})</span></label>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="product_block">
                            <div class="sidebar_heading">
                                <h3>Lọc theo giá</h3>
                                <img src="/q_web/images/footer_underline.png" alt="image">
                            </div>
                            <div class="ds_progress_rangeslider Range_slider">
                                <div id="slider-range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content slider-range-page-product">
                                    <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" id="click-min"></span>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" id="click-max"></span>
                                </div>
                                
                                <div class="price_range"><p><span id="amount"></span></p></div>
{{--                                <button id="click" type="">Chọn</button>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="product_section">
                        <div class="ads_section"><img src="q_web/images/ảnh-1-3.jpg" alt="image"></div>
                    </div>
                    <div class="product_list_section">
                        <div class="product_list_filter">
                            <ul>
                                <li>
                                    <p>Hiển thị <span>9 trong {{ $countPro }}</span> sản phẩm</p>
                                </li>
                                <li>
                                    <ul class="list_view_toggle">
                                        <li><span>view style</span></li>
                                        <li>
                                            <a href="javascript:;" class="active grid_view">
                                                <svg 
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                width="12px" height="12px">
                                                <path fill-rule="evenodd"  fill="rgb(112, 112, 112)"
                                                d="M6.861,12.000 L6.861,6.861 L12.000,6.861 L12.000,12.000 L6.861,12.000 ZM6.861,-0.000 L12.000,-0.000 L12.000,5.139 L6.861,5.139 L6.861,-0.000 ZM-0.000,6.861 L5.139,6.861 L5.139,12.000 L-0.000,12.000 L-0.000,6.861 ZM-0.000,-0.000 L5.139,-0.000 L5.139,5.139 L-0.000,5.139 L-0.000,-0.000 Z"/>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" class="list_view">
                                                <svg 
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                width="12px" height="10px">
                                                <path fill-rule="evenodd"  fill="rgb(112, 112, 112)"
                                                d="M3.847,10.000 L3.847,7.783 L12.000,7.783 L12.000,10.000 L3.847,10.000 ZM3.847,3.892 L12.000,3.892 L12.000,6.108 L3.847,6.108 L3.847,3.892 ZM3.847,-0.000 L12.000,-0.000 L12.000,2.216 L3.847,2.216 L3.847,-0.000 ZM-0.000,7.783 L2.297,7.783 L2.297,10.000 L-0.000,10.000 L-0.000,7.783 ZM-0.000,3.892 L2.297,3.892 L2.297,6.108 L-0.000,6.108 L-0.000,3.892 ZM-0.000,-0.000 L2.297,-0.000 L2.297,2.216 L-0.000,2.216 L-0.000,-0.000 Z"/>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div id="product-foreach-box-id">
                            @include('q_web.products_foreach',['product'=>$product])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 @stop
@section('linkJavasript')
<script>
    $(function() {    // <== doc ready
        $( ".slider-range-page-product" ).slider({
            change: function(event, ui) {
                let price_min = ui.values[0];
                let price_max = ui.values[1];
                let this_ajax = true;
                $.ajax({
                    method: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    url: '{{Request::fullUrl()}}',
                    data: {
                        this_ajax: this_ajax,
                        price_min: price_min,
                        price_max: price_max
                    }
                }).done(function (res) {
                    $('#product-foreach-box-id').html(res.html);
                }).fail(function (error) {
                    console.log(error);
                });
            }
        });

    });
</script>
@stop

