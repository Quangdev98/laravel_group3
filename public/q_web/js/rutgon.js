$(document).ready(function() {
	var showCharpage5 = 50;
	var ellipsestextpage5 = "...";
	var showCharpage6 = 28;
	var ellipsestextpage6 = "...";
	var showCharpage7 = 13;
	var ellipsestextpage7 = "...";
	var showCharpage8 = 120;
	var ellipsestextpage8 = "...";

	$('.lessText').each(function() {
		var contentpage5 = $(this).html();

		if(contentpage5.length > showCharpage5) {

			var c = contentpage5.substr(0, showCharpage5);
			var h = contentpage5.substr(showCharpage5-1, contentpage5.length - showCharpage5);

			var html = c + '<span class="moreellipses">' +
			ellipsestextpage5+ '&nbsp;</span><span class="morecontent"><span>' 
			+ h;

			$(this).html(html);
		}
	});
	$('.lessText2').each(function() {
		var contentpage6 = $(this).html();

		if(contentpage6.length > showCharpage6) {

			var c = contentpage6.substr(0, showCharpage6);
			var h = contentpage6.substr(showCharpage6-1, contentpage6.length - showCharpage6);

			var html = c + '<span class="moreellipses">' +
			ellipsestextpage6+ '&nbsp;</span><span class="morecontent"><span>' 
			+ h;

			$(this).html(html);
		}
	});
	$('.namePro').each(function() {
		var contentpage7 = $(this).html();

		if(contentpage7.length > showCharpage7) {

			var c = contentpage7.substr(0, showCharpage7);
			var h = contentpage7.substr(showCharpage7-1, contentpage7.length - showCharpage7);

			var html = c + '<span class="moreellipses">' +
			ellipsestextpage7+ '&nbsp;</span><span class="morecontent"><span>'
			+ h;

			$(this).html(html);
		}
	});
    $('.textOpinion').each(function() {
            var contentpage8 = $(this).html();

            if(contentpage8.length > showCharpage8) {

                var c = contentpage8.substr(0, showCharpage8);
                var h = contentpage8.substr(showCharpage8-1, contentpage8.length - showCharpage8);

                var html = c + '<span class="moreellipses">' +
                ellipsestextpage8+ '&nbsp;</span><span class="morecontent"><span>'
                + h;

                $(this).html(html);
            }
        });

});
