<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('name');
            $table->unsignedBigInteger('price')->default('0.0000');
            $table->string('contentHot',250);
            $table->string('image', 300);
            $table->string('slug', 300)->nullable();
            $table->unsignedBigInteger('sale');
            $table->longText('content')->nullable()->comment('nội dung sản phẩm');
            $table->tinyInteger('status')->default(2)->comment('trạng thái: 1:Bản nháp, 2:chờ duyệt, 3: đã xuất bản');
            $table->unsignedBigInteger('category_id')->nullable()->index();
            $table->foreign('category_id')->references('id')->on('theloai')->onDelete('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
