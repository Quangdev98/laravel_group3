<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('info', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('address',300)->nullable();
            $table->string('phone',300)->nullable();
            $table->string('email',300)->nullable();
            $table->tinyInteger('status')->default(2)->comment('1:trụ sở chính, 2:trụ sở phụ');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info');
    }
}
