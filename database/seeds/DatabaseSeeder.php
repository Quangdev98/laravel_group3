<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(TheloaiSeeder::class);
        $this->call(slideSeeder::class);
        $this->call(infoSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(postSeeder::class);
        $this->call(opinionSeeder::class);
    }
}
