<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product')->insert([
        	[
        		'id'=>1,
        		'name'=>'Xoài An Giang',
        		'price'=>'20000',
        		'contentHot'=>'Hiệu quả kinh tế của cây xoài tại An Giang đã mang lợi nhuận cao hơn từ 7 - 10 lần so với trồng lúa (tùy giá cả thị trường hàng năm và từng loại giống xoài).',
        		'image'=>'xoai.jpg',
        		'slug'=>'xoai',
        		'sale'=>'10',
        		'content'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.',
        		'status'=>2,
        		'category_id'=>1
        	],
        ]);
    }
}
