<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class infoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('info')->insert([
        	[
        		'id'=>1,
        		'address'=>'Số 1, Nguyễn Chí Thanh, Ba Đình, Hà Nội',
        		'phone'=>'0969030421',
        		'email'=>'dvq.dev@gmail.com',
        		'status'=>2
        	],
        ]);
    }
}
