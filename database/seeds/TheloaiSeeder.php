<?php

use Illuminate\Database\Seeder;

class TheloaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('theloai')->insert([
        	'id' => '1',
        	'name' =>'Cá',
        	'slug' => 'ca'
        ]);
    }
}
