<?php

use Illuminate\Database\Seeder;

class slideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slide')->insert([
        	[
        		'id'=>1,
                'image'=>'file/image1.png',
        		'title'=>'cá là 1 loài động vật có xương sống',
        		'contentHot'=>'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.',
        		'slug'=>'ca-la-1-loai-dong-vat-co-xuong-song',
        		'link'=>'fhdjfhdjgf'
        	],
        ]);
    }
}
