<?php

use Illuminate\Database\Seeder;

class opinionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('opinion')->insert([
            'id'=>1,
            'image'=>'user.png',
            'name'=>'Quang Dev',
            'content'=>'Nsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna alierqua Ut enierim ad minim veniam, quis nostrud exercitation ullamco laboris nissdi ut aliquip exeriommodo consequate Duis aute irure dolor in reprehenderit in voluptate.',
            'cate'=>'Chuyên gia Nông nghiệp'
        ]);
    }
}
