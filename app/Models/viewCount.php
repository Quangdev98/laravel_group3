<?php

namespace App\Models\Events;

use Illuminate\Database\Eloquent\Model;
use App\Models\Post;
class viewCount extends Model
{
    public function handle(Post $post)
    {
        $post->increment('view-count');
    }
}
