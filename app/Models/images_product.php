<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class images_product extends Model
{
    protected $table = "images_product";
    public function product()
    {
        return $this->belongsTo('App\Models\product','product_id','id');
    }
}
