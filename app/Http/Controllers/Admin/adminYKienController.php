<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Opinion;

class adminYKienController extends Controller
{
    public function getYKien() {
        $opinion = DB::table('opinion')->get();
        return view('q_admin.opinion.list', compact('opinion'));
    }
    public function getOpinion_add() {
        return view('q_admin.opinion.add');
    }
    public function postOpinion_add( Request $request) {
        $this->validate($request,
            [
                'name' => 'required|min:3|max:500|unique:opinion,name',

                'images_' => 'required|image|mimes:jpeg,png,jpg,gif|max:900',
            ],
            [
                'name.required' => 'tên không được để trống.<br>',
                'name.min' => 'tên không được thấp hơn 3 kí tự.<br>',
                'name.max' => 'tên không được lớn hơn 300 ký tự.<br>',
                'name.unique' => 'Tên đã tồn tại.<br>',

                'images_.required' => 'File tải lên không được để trống.<br>',
                'images_.image' => 'File tải lên không hợp lệ.<br>',
                'images_.mimes' => 'File tải lên không hợp lệ.<br>',
                'images_.max' => 'File tải lên không được quá 10MB.<br>',
            ]);
        $fileImage = 'IMAGE-opinion-'.time().$request->file('images_')->getClientOriginalName();
        $opinion = new Opinion;
        $opinion->image = $fileImage;
        $opinion->name = $request->name;
        $opinion->cate = $request->cate;
        $opinion->content = $request->content;
        $request->file('images_')->move('uploads/opinion/',$fileImage);
        $opinion->save();
        return redirect()->route('ad.ykien');
    }
    public function getOpinion_edit($id) {
        $opinion = Opinion::find($id);
        return view('q_admin.opinion.edit', compact('opinion'));
    }
    public function postOpinion_edit(Request $request,$id) {
        $this->validate($request,
            [
                'name' => 'min:3|max:500',

                'images_' => 'image|mimes:jpeg,png,jpg,gif|max:900',
            ],
            [
                'name.min' => 'tên không được thấp hơn 3 kí tự.<br>',
                'name.max' => 'tên không được lớn hơn 300 ký tự.<br>',
                'name.unique' => 'Tên đã tồn tại.<br>',
                'images_.image' => 'File tải lên không hợp lệ.<br>',
                'images_.mimes' => 'File tải lên không hợp lệ.<br>',
                'images_.max' => 'File tải lên không được quá 10MB.<br>',
            ]);
        $opinion = Opinion::find($id);
        $name_img_old = $opinion->image;
        $opinion->name = $request->name;
        $opinion->cate = $request->cate;
        $opinion->content = $request->content;
        if ($request->file('images_')) {
            $fileImage = 'IMAGE-opinion-'.time().$request->file('images_')->getClientOriginalName();
            $opinion->image=$fileImage;
            $request->file('images_')->move('uploads/opinion/',$fileImage);
            if(file_exists('uploads/opinion/'.$name_img_old)){
                unlink('uploads/opinion/'.$name_img_old);
            }
        }
        $opinion->save();
        return redirect()->route('ad.ykien');
    }
    public function getOpinionDelete($id){
        $opinion = Opinion::find($id);
        $opinion->delete();
        return redirect()->Route('ad.ykien');
    }



}
