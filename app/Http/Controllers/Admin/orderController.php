<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Customer;

class orderController extends Controller
{
    public function getOrder()
    {
        $data['customer'] = DB::table('customer')->where('status',0)->paginate(15);
        return view('q_admin.order.order',$data);
    }

    public function getOrderprocess()
    {
        $data['customer'] = DB::table('customer')->where('status',1)->paginate(15);
        return view('q_admin.order.order_process',$data);
    }
    public function activeOrder($id)
    {
        $customer = Customer::find($id);
        $customer->status = 1;
        $customer->save();
        return back();
    }
    public function getOrderView($id)
    {
        $orderView = DB::table('customer')
            ->join('order','customer.id','=','order.customer_id')
            ->select('customer.*','order.id as id_order')
            ->where('customer.id',$id)
            ->first();
        $order = DB::table('order')->where('customer_id',$id)->get();
//        dd($order);
        return view('q_admin.order.order-detail', compact('orderView','order'));
    }
}
