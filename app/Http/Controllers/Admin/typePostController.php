<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Type_post;

class typePostController extends Controller
{
    public function getTypePostList(){
    	// $typePost = Type_post::all();
        $typePost = DB::table('type_post')->get();
        // ->join('post','type_post.id','=','post.typePost_id')
        // ->select('type_post.*', DB::raw('count(post.typePost_id) as countPost'))
        // ->groupBy('type_post.id')->get();
    	return view('q_admin.Type-post.typePost_list', compact('typePost'));
    }
    public function getTypePostAdd(){
    	return view('q_admin.Type-post.typePost_add');
    }
    public function postTypePostAdd(Request $request){
    	$typePost = new Type_post;
    	$this->validate($request, 
        [
            'name' => 'required|unique:Type_post,name|min:2|max:100'
        ],
    	[
            'name.required' =>'Bạn chưa nhập tên thể loại',
            'name.unique' => 'Tên thể loại đã tồn tại',
            'name.min' =>'Tên thể loại phải có độ dài từ 3 đến 100 ký tự'
        ]);
    	$typePost->name = $request->name;
    	$typePost->save();
    	return redirect()->route('ad.typePost-list');
    }
    public function geTypetPostEdit($id){
        $typePost = Type_post::find($id);
    	return view('q_admin.Type-post.typePost_edit', compact('typePost'));
    }
    public function postTypetPostEdit(Request $request, $id){
        $typePost = Type_post::find($id);
        $this->validate($request, 
        [
            // 'name' => 'required|unique:Type_post,name|min:2|max:100'
        ], 
        [
            'name.required' =>'Bạn chưa nhập tên',
            'name.unique' => 'Tên thể loại đã tồn tại',
            'name.min' =>'Tên thể loại phải có độ dài từ 3 đến 100 ký tự'
        ]);
        $typePost->name = $request->name;
        $typePost->save();
        return redirect()->route('ad.typePost-list');
    }
    public function geTypetPostDelete(Request $request, $id){
    	$typePost = Type_post::find($id);
    	$typePost->delete();
    	return redirect()->route('ad.typePost-list');
    }
}
