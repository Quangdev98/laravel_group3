<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Users;

class admin_Author_Controller extends Controller
{
    public function getAuthorList(){
    	$users = DB::table('users')->paginate(15);
    	return view('q_admin.author.author_list',compact('users'));
    }
    public function getAuthorEdit($id){
    	$users = Users::find($id);
    	return view('q_admin.author.author_edit', compact('users'));
    }
    public function postAuthorEdit(Request $request,$id){
    	$users = Users::find($id);
    	$this->validate($request,
    	[
            // 'name' => 'required|unique:Users',
            // 'phone' => 'required|unique:Users,phone|min:10|max:15'
             'image' => 'image|mimes:jpeg,png,jpg,gif|max:10240',
        ],
    	[
            'name.required' =>'Bạn chưa nhập tên thể loại',
            'name.unique' => 'Tên thể loại đã tồn tại',
            'name.min' =>'Tên thể loại phải có độ dài từ 3 đến 100 ký tự',
            'phone.required' =>'Bạn chưa nhập số điện thoại',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'phone.min' =>'Tên thể loại phải có độ dài từ 10 đến 100 ký tự'
            // 'name.required' =>'Bạn chưa nhập tên thể loại',
            // 'name.unique' => 'Tên thể loại đã tồn tại',
            // 'name.min' =>'Tên thể loại phải có độ dài từ 3 đến 100 ký tự',
            
        ]);
        $avataOld = $users->image;
        if ($request->file('image')) {
            $avatar = 'IMAGE-SLIDE-'.time().$request->file('image')->getClientOriginalName();
            $users->image=$avatar;
            $request->file('image')->move('uploads/avatars/',$avatar);
            if(file_exists('uploads/avatars/'.$avataOld)){
                unlink('uploads/avatars/'.$avataOld);
            }
        }
		$users->name = $request->name;
		$users->phone = $request->phone;
        $users->slug = create_slug($users->name,$id);
		$users->email = $request->email;
		$users->address = $request->address;
		$users->gender = $request->gender;
		// $users->birthday = $request->birthday;
		$users->info = $request->info;
		// $users->password = bcrypt($request->password);
		$users->level = $request->level;
		$users->save();
		return redirect()->route('ad.author-list')->with('thongbao', 'thêm Tác giả thành công!');
    }
    public function getAuthorAdd(){
    	return view('q_admin.author.author_add');
    }
    public function postAuthorAdd(Request $request){
    	$this->validate($request,
    	[
            'name' => 'required|unique:Users,name|min:2|max:100',
            'phone' => 'required|unique:Users,phone|min:10|max:15',
            'email' => 'required|unique:Users,email',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:10240',
        ],
    	[
            'name.required' =>'Bạn chưa nhập tên thể loại',
            'name.unique' => 'Tên thể loại đã tồn tại',
            'name.min' =>'Tên thể loại phải có độ dài từ 3 đến 100 ký tự',
            'phone.required' =>'Bạn chưa nhập số điện thoại',
            'phone.unique' => 'Số điện thoại đã tồn tại',
            'phone.min' =>'Tên thể loại phải có độ dài từ 10 đến 100 ký tự'
            
            // 'name.required' =>'Bạn chưa nhập tên thể loại',
            // 'name.unique' => 'Tên thể loại đã tồn tại',
            // 'name.min' =>'Tên thể loại phải có độ dài từ 3 đến 100 ký tự',
            
        ]);
        $avatar = 'IMAGE-AUTHOR-'.time().$request->file('image')->getClientOriginalName();
    	$users = new Users;
		$users->image = $avatar;
        $users->name = $request->name;
		$users->phone = $request->phone;
		$users->email = $request->email;
		$users->address = $request->address;
		$users->gender = $request->gender;
		$users->birthday = $request->birthday;
		$users->info = $request->info;
		$users->password = bcrypt($request->password);
		$users->level = $request->level;
        $request->file('image')->move('uploads/avatars/',$avatar);
		$users->save();
        $users->update(['slug'=>create_slug($users->name,$users->id)]);
		return redirect()->route('ad.author-list')->with('thongbao', 'thêm Tác giả thành công!');
    }
    public function getAuthorDelete($id){
        $user = Users::find($id);
        $user->delete();
        return redirect()->Route('ad.author-list');
    }

}
