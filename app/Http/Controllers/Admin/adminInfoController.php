<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Info;

class adminInfoController extends Controller
{
    public function index(){
    	$info = DB::table('info')->get();
    	return view('q_admin.info.info_list', compact('info'));
    }
    public function getInfoAdd(){
    	return view('q_admin.info.info_add');
    }
    public function postInfoAdd(Request $request){
    	$this->validate($request,
        [
            'address' => 'required|min:10|max:300|unique:info,address',
            'phone' => 'required|min:10|max:15|unique:info,phone',
            'email' => 'required|min:9|max:30|unique:info,email',
            
        ],
        [
            'address.required' => 'Địa chỉ không được để trống.<br>',
            'title.min' => 'Địa chỉ không được thấp hơn 10 kí tự.<br>',
            'title.max' => 'Địa chỉ không được lớn hơn 300 ký tự.<br>',
            'title.unique' => 'Địa chỉ đã tồn tại.<br>',
            'phone.required' => 'Số điện thoại liên hệ không được để trống.<br>',
            'contentHot.min' => 'Số điện thoại liên hệ không được thấp hơn 10 kí tự.<br>',
            'contentHot.max' => 'Số điện thoại liên hệ không được lớn hơn 15 ký tự.<br>',
            'email.required' => 'email không được để trống.<br>',
            'email.min' => 'email không được  thấp hơn 10 kí tự.<br>',
            'email.max' => 'email không được  lớn hơn 30 kí tự.<br>',
            // 'fileImagePost.max' => 'email không được quá 10MB.<br>',            
        ]);
    	$infoNew = new Info;
    	$infoNew->address = $request->address;
    	$infoNew->phone = $request->phone;
    	$infoNew->email = $request->email;
    	$infoNew->status = $request->status;
    	$infoNew->save();
    	return redirect()->route('ad.info-list');
    }
    public function getInfoEdit($id){
    	$info = DB::table('info')->find($id);
    	return view('q_admin.info.info_edit', compact('info'));
    }
    public function postInfoEdit(Request $request,$id){
    	$this->validate($request,
        [
            'address' => 'required|min:10|max:300',
            'phone' => 'required|min:10|max:15',
            'email' => 'required|min:9|max:30',
            
        ],
        [
            'address.required' => 'Địa chỉ không được để trống.<br>',
            'title.min' => 'Địa chỉ không được thấp hơn 10 kí tự.<br>',
            'title.max' => 'Địa chỉ không được lớn hơn 300 ký tự.<br>',
            'title.unique' => 'Địa chỉ đã tồn tại.<br>',
            'phone.required' => 'Số điện thoại liên hệ không được để trống.<br>',
            'contentHot.min' => 'Số điện thoại liên hệ không được thấp hơn 10 kí tự.<br>',
            'contentHot.max' => 'Số điện thoại liên hệ không được lớn hơn 15 ký tự.<br>',
            'email.required' => 'email không được để trống.<br>',
            'email.min' => 'email không được  thấp hơn 10 kí tự.<br>',
            'email.max' => 'email không được  lớn hơn 30 kí tự.<br>',
            // 'fileImagePost.max' => 'email không được quá 10MB.<br>',            
        ]);
    	$info = Info::find($id);
    	$info->address = $request->address;
    	$info->phone = $request->phone;
    	$info->email = $request->email;
    	$info->status = $request->status;
    	$info->save();
    	return redirect()->route('ad.info-list');
    }
    public function getInfoDelete($id){
    	$infoDelete = Info::find($id);
    	$infoDelete->delete();
    	return redirect()->route('ad.info-list');
    }

}
