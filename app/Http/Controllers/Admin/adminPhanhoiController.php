<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class adminPhanhoiController extends Controller
{
    public function getPhanhoi(){
        $phanhoi = DB::table('contact')->paginate(15);
        return view('q_admin.phan-hoi.phanhoi', compact('phanhoi'));
    }
    public function getPhanhoiDelete($id){
        $phanhoi = DB::table('contact')->where('id',$id)->delete();
        return redirect()->route('ad.phanhoi');
    }
}
