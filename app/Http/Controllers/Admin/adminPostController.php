<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Post;
use App\Models\Users;
use App\Models\Type_post;

class adminPostController extends Controller
{
    public function getPostList(Request $request){
        $danhmucs = DB::table('type_post')->get();
        $authors = DB::table('users')->get();
    	$post = DB::table('users')
    	->join('post','users.id','=','post.user_id')
    	->join('type_post', 'post.typePost_id','=','type_post.id');
    	if(request('search')){
            $search = request('search');
            $post->where('post.title','LIKE',"%{$search}%");
        }
    	if(request('danhmuc')){
            $danhmuc = request('danhmuc');
            $post->where('post.typePost_id',$danhmuc);
        }
    	if(request('author')){
            $author = request('author');
            $post->where('post.user_id',$author);
        }
//    	if(request('sale')){
//            $sale = request('sale');
//            $post->where('post.sale',$sale);
//        }
    	$post = $post
    	->select('users.name as name_author','post.*','type_post.name as type_post_name')
        ->paginate(15);
    	// dd($post->all());
    	return view('q_admin.post.post_list', compact('danhmucs','authors','post'));
    }
    public function getPostAdd(){
    	$user = Users::all();
    	$type_post = Type_post::all();
    	return view('q_admin.post.post_add', compact('user','type_post'));
    }
    public function postPostAdd(Request $request){
    	$this->validate($request,
        [
            'title' => 'required|min:3|max:500|unique:post,title',
            'contentHot' => 'required|min:3|max:1000',
            'fileImagePost' => 'required|image|mimes:jpeg,png,jpg,gif|max:500',
        ],
        [
            'title.required' => 'Tiêu đề bài viết không được để trống.<br>',
            'title.min' => 'Tiêu đề slide không được thấp hơn 3 kí tự.<br>',
            'title.max' => 'Tiêu đề slide không được lớn hơn 300 ký tự.<br>',
            'title.unique' => 'Tiêu đề slide đã tồn tại.<br>',
            'contentHot.required' => 'Nội dung nổi bật slide không được để trống.<br>',
            'contentHot.min' => 'Nội dung nổi bật slide không được thấp hơn 3 kí tự.<br>',
            'contentHot.max' => 'Nội dung nổi bật slide không được lớn hơn 500 ký tự.<br>',
            // 'link.required' => 'Đường dẫn trỏ tới của slide không được để trống.<br>',
            // 'link.min' => 'Đường dẫn trỏ tới của slide không được thấp hơn 3 kí tự.<br>',
            // 'link.max' => 'Đường dẫn trỏ tới của slide không được lớn hơn 400 ký tự.<br>',
            'fileImagePost.required' => 'File tải lên không được để trống.<br>',
            'fileImagePost.image' => 'File tải lên không hợp lệ.<br>',
            'fileImagePost.mimes' => 'File tải lên không hợp lệ.<br>',
            'fileImagePost.max' => 'File tải lên không được quá 10MB.<br>',            
        ]);
        $fileImagePost = 'IMAGE-SLIDE-'.time().$request->file('fileImagePost')->getClientOriginalName();
    	$post = new Post;
    	$post->image = $fileImagePost;
    	$post->title = $request->title;
    	$post->contentHot = $request->contentHot;
    	$post->postDate = $request->postDate;
    	$post->tag = $request->tag;
    	$post->content = $request->content;
    	$post->user_id = $request->user_id;
    	$post->typePost_id = $request->typePost_id;
    	$request->file('fileImagePost')->move('uploads/posts/',$fileImagePost);
    	// dd($post->all());
    	$post->save();
        $post->update(['slug'=>create_slug($post->title,$post->id)]);
    	return redirect()->route('ad.post-list');

    }
    public function getPostEdit($id){
    	$post = Post::find($id);
    	$user = Users::all();
    	$type_post = Type_post::all();
    	return view('q_admin.post.post_edit', compact('post','user','type_post'));
    }
    public function postPostEdit(Request $request,$id){
    	$this->validate($request,
        [
            'title' => 'required|min:3|max:500',
            'contentHot' => 'required|min:3|max:1000',
            'fileImagePost' => 'image|mimes:jpeg,png,jpg,gif|max:10240',
        ],
        [
            'title.required' => 'Tiêu đề bài viết không được để trống.<br>',
            'title.min' => 'Tiêu đề slide không được thấp hơn 3 kí tự.<br>',
            'title.max' => 'Tiêu đề slide không được lớn hơn 500 ký tự.<br>',
            'title.unique' => 'Tiêu đề bài viết đã tồn tại.<br>',
            'contentHot.required' => 'Nội dung nổi bật slide không được để trống.<br>',
            'contentHot.min' => 'Nội dung nổi bật slide không được thấp hơn 3 kí tự.<br>',
            'contentHot.max' => 'Nội dung nổi bật slide không được lớn hơn 500 ký tự.<br>',
           
            'fileImagePost.image' => 'File tải lên không hợp lệ.<br>',
            'fileImagePost.mimes' => 'File tải lên không hợp lệ.<br>',
            'fileImagePost.max' => 'File tải lên không được quá 10MB.<br>',            
        ]);
        $post = Post::find($id);
        $imgOld = $post->image;
    	$post->title = $request->title;
    	$post->contentHot = $request->contentHot;
    	$post->postDate = $request->postDate;
    	$post->tag = $request->tag;
    	$post->content = $request->content;
    	$post->user_id = $request->user_id;
    	$post->typePost_id = $request->typePost_id;
    	if ($request->file('fileImagePost')) {
            $fileImagePost = 'IMAGE-SLIDE-'.time().$request->file('fileImagePost')->getClientOriginalName();
            $post->image=$fileImagePost;
            $request->file('fileImagePost')->move('uploads/posts/',$fileImagePost);
            if(file_exists('uploads/posts/'.$imgOld)){
                unlink('uploads/posts/'.$imgOld);
            }
        }
        $post->save();
        $post->update(['slug'=>create_slug($post->title,$post->id)]);
        return redirect()->route('ad.post-list');
    }
    public function getPostDelete($id){
    	$post = Post::find($id);
    	$post->delete();
    	return redirect()->Route('ad.post-list');
    }

}
