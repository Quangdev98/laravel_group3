<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Carbon;
class BangDieuKhienController extends Controller
{
    public function getAdmin(){
    	// dd('Bangdieu khien');
        $users = Users::all();
        // $countProduct = Product::all()->count();
        $year_n = Carbon::now()->format('Y');
        $month_n = Carbon::now()->format('m');
        for($i=1;$i<=$month_n;$i++)
        {
            $monthjs[$i]='tháng '.$i;
            $numberjs[$i]=customer::where('status',1)->whereMonth('updated_at',$i)->whereYear('updated_at',$year_n)->sum('total');

        }
        $data['monthjs'] = $monthjs;
        $data['numberjs'] = $numberjs;
        $data['order'] = Customer::where('status',1)->count();
        $data['order_w'] = Customer::where('status',0)->count();
    	return view('q_admin.admin', $data);
    }
}
