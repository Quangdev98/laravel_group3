<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Theloai;

class adminCategoryController extends Controller
{
   	public function getCateList(){
        // $theloai = Theloai::all();
        $countProducts = DB::table('theloai')
        ->leftJoin('product','theloai.id','=','product.category_id')
        ->select('theloai.*', DB::raw('count(product.category_id) as count_product'))
        // ->where('product.status','<',4)
        ->groupBy('theloai.id','theloai.name','theloai.slug','theloai.created_at','theloai.updated_at')->paginate(15);
        // dd($countProducts);
    	return view('q_admin.category.cate_list', compact('countProducts'));
    }
    public function getCateAdd(){    
    	return view('q_admin.category.cate_add');
    }
    public function postCateAdd(Request $request){
        $theloai = new Theloai;
        $theloai->name = $request->theloaiName;
        $theloai->save();
        $theloai->update(['slug'=>create_slug($theloai->name,$theloai->id)]);
        // dd($theloai);
        return redirect()->route('ad.category-list')->with('thongbao', 'Thêm thể loại thành công!');
    }
    public function getCateEdit($id){
        $theloai = Theloai::find($id);
    	return view('q_admin.category.cate_edit', compact('theloai'));
    } 
    public function postCateEdit(Request $request,$id){
        $theloai = Theloai::find($id);
        $this->validate($request, 
        [
            'cateName' => 'required|unique:Theloai,name|min:2|max:100'
        ], 
        [
            'cateName.required' =>'Bạn chưa nhập tên',
            'cateName.unique' => 'Tên thể loại đã tồn tại',
            'cateName.min' =>'Tên thể loại phải có độ dài từ 3 đến 100 ký tự'
        ]);
        $theloai->name = $request->cateName;
        $theloai->slug = create_slug($theloai->name,$id);
        $theloai->save();
        return redirect()->route('ad.category-list')->with('thongbao', 'Thêm thể loại thành công!');
    }
    public function getCateDelete($id){
        $theloai = Theloai::find($id);
        $theloai->delete();
        return redirect()->route('ad.category-list')->with('thongbao', 'Xóa thể loại thành công!');
    }
}
