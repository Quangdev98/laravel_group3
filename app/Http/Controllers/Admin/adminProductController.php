<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Theloai;
use App\Http\Requests\UploadRequest;
use Illuminate\Support\Str;
use App\Models\images_product;

class adminProductController extends Controller
{
   public function getProductList(Request $request){
   	// $product = Product::all();
   $danhmucs = DB::table('theloai')->get();
   	$product = DB::table('theloai')
   	->join('product','theloai.id','=','product.category_id');
    if(request('search')){
        $search = request('search');
        $product->where('product.name','LIKE', "%{$search}%");
    }
    if(request('danhmuc')){
        $danhmuc = request('danhmuc');
        $product->where('product.category_id',$danhmuc);
    }
    if(request('status')){
        $status = request('status');
        $product->where('product.status',$status);
    }
    if(request('sale')){
        $sale = request('sale');
        $product->where('product.sale','<',$sale);
    }
    $product = $product
        ->select('product.*','theloai.name as nameCate')
        ->where('status','<','4')
        ->paginate(15);
    	return view('q_admin.product.product_list', compact('danhmucs','product'));
    }
    public function getProductAdd(){
    	$theloai = Theloai::all();
    	return view('q_admin.product.product_add',compact('theloai'));
    }
    public function postProductAdd(Request $request){
    	$this->validate($request,
        [
            'name' => 'required|min:3|max:500|unique:product,name',
            // 'contentHot' => 'required|min:3|max:1000',
            // 'link' => 'required|min:3|max:400',
            'imageProduct' => 'required|image|mimes:jpeg,png,jpg,gif|max:10240',
        ],
        [
            'name.required' => 'Tiêu đề bài viết không được để trống.<br>',
            'name.min' => 'Tiêu đề slide không được thấp hơn 3 kí tự.<br>',
            'name.max' => 'Tiêu đề slide không được lớn hơn 300 ký tự.<br>',
            'name.unique' => 'Tiêu đề slide đã tồn tại.<br>',

            'imageProduct.required' => 'File tải lên không được để trống.<br>',
            'imageProduct.image' => 'File tải lên không hợp lệ.<br>',
            'imageProduct.mimes' => 'File tải lên không hợp lệ.<br>',
            'imageProduct.max' => 'File tải lên không được quá 10MB.<br>',
        ]);
        $imageProduct = 'IMAGE-PRODUCT'.time().$request->file('imageProduct')->getClientOriginalName();
    	$product = new Product;
    	$product->name = $request->name;
    	$product->price = $request->price;
    	if($request->hasFile('imageProduct'))
        {
            $filee = $request->imageProduct;
            $fileName = $filee->getClientOriginalName();
            $filee->move('uploads/products',$fileName);
            $product->image = $fileName;
        }
    	$product->sale = $request->sale;
    	$product->contentHot = $request->contentHot;
        $product->content = $request->content;
    	$product->status = $request->status;
    	$product->category_id = $request->category_id;
    	$product->save();
        $product->update(['slug'=>create_slug($product->name,$product->id)]);
        if($request->hasFile('imageDetail')){
            foreach($request->imageDetail as $file){
                $filename=$file->getClientOriginalName();
                $file->move('uploads/products/details',$filename);
                $fileModel = new images_product;
                $fileModel->link = $filename;
                $fileModel->product_id=$product->id;
                $fileModel->save();
            }
        }
        return redirect()->Route('ad.product-list')->with('thongbao','Đã thêm sản phẩm thành công');
    }
    public function getProductEdit($id){
    	$product = Product::find($id);
        $theloai = Theloai::all();
    	return view('q_admin.product.product_edit', compact('product','theloai'));
    }
    public function postProductEdit(Request $request,$id){
    	$this->validate($request,
    		[
                 'name' => 'required|min:3|max:500',
                 'imageProduct' => 'image|mimes:jpeg,png,jpg,gif|max:10240',
    		],[
                // 'name.required' => 'Tiêu đề bài viết không được để trống.<br>',
                'name.min' => 'Tiêu đề slide không được thấp hơn 3 kí tự.<br>',
                'name.max' => 'Tiêu đề slide không được lớn hơn 300 ký tự.<br>',
                'name.unique' => 'Tiêu đề slide đã tồn tại.<br>',

                // 'imageProduct.required' => 'File tải lên không được để trống.<br>',
                'imageProduct.image' => 'File tải lên không hợp lệ.<br>',
                'imageProduct.mimes' => 'File tải lên không hợp lệ.<br>',
                'imageProduct.max' => 'File tải lên không được quá 10MB.<br>',   ]
    	);
        $product = Product::find($id);
        $imageOld = $product->image;
        $product->name = $request->name;
        $product->price = $request->price;
        if ($request->file('imageProduct')) {
            $imageProduct = 'IMAGE-PRODUCT'.time().$request->file('imageProduct')->getClientOriginalName();
            $product->image = $imageProduct;
            $request->file('imageProduct')->move('uploads/products/',$imageProduct);
            if(file_exists('uploads/products/'.$imageOld)){
                unlink('uploads/products/'.$imageOld);
            }
        }
        // $product->image = $imageProduct;
        $product->sale = $request->sale;
        $product->contentHot = $request->contentHot;
        $product->content = $request->content;
        $product->status = $request->status;
        $product->category_id = $request->category_id;
        $product->save();
        $product->update(['slug'=>create_slug($product->name,$product->id)]);
        if($request->hasFile('imageDetail')){
            foreach($request->imageDetail as $file){
                $filename=$file->getClientOriginalName();
                $file->move('uploads/products/details',$filename);
                $fileModel = new images_product;
                $fileModel->link = $filename;
                $fileModel->product_id=$product->id;
                $fileModel->save();
            }
        }
        return redirect()->Route('ad.product-list');
    }
    public function getProductDeleteList(){
        $productDeleteList = DB::table('theloai')
        ->join('product','theloai.id','=','product.category_id')
        ->select('product.*','theloai.name as nameCate')
        ->where('status',4)
        ->get();
        return view('q_admin.product.product_delete',compact('productDeleteList'));
    }
    public function getProductDeleteStatus($id){
        Product::where('id', $id)->update(array('status' => 4));
        return redirect()->route('ad.product-list');
    }
    public function getProductDeleteEdit($id){
        $product = Product::find($id);
        $theloai = Theloai::all();
        return view('q_admin.product.product_edit', compact('product','theloai'));
    }
    public function postProductDeleteEdit(Request $request,$id){
        $this->validate($request,
            [
                 'name' => 'required|min:3|max:500',
                 'imageProduct' => 'image|mimes:jpeg,png,jpg,gif|max:10240',
            ],[
                'name.min' => 'Tiêu đề slide không được thấp hơn 3 kí tự.<br>',
                'name.max' => 'Tiêu đề slide không được lớn hơn 300 ký tự.<br>',
                'name.unique' => 'Tiêu đề slide đã tồn tại.<br>',
                'imageProduct.image' => 'File tải lên không hợp lệ.<br>',
                'imageProduct.mimes' => 'File tải lên không hợp lệ.<br>',
                'imageProduct.max' => 'File tải lên không được quá 10MB.<br>',   ]
        );
        $product = Product::find($id);
        $imageOld = $product->image;
        $product->name = $request->name;
        $product->price = $request->price;
        if ($request->file('imageProduct')) {
            $imageProduct = 'IMAGE-PRODUCT'.time().$request->file('imageProduct')->getClientOriginalName();
            $product->image = $imageProduct;
            $request->file('imageProduct')->move('uploads/products/',$imageProduct);
            if(file_exists('uploads/products/'.$imageOld)){
                unlink('uploads/products/'.$imageOld);
            }
        }
        // $product->image = $imageProduct;
        $product->sale = $request->sale;
        $product->contentHot = $request->contentHot;
        $product->content = $request->content;
        $product->status = $request->status;
        $product->category_id = $request->category_id;
        $product->save();
        $product->update(['slug'=>create_slug($product->name,$product->id)]);
        return redirect()->Route('ad.product-delete-list');
    }
    public function getProductDelete($id){
            $product = Product::find($id);
            $product->delete();
            return redirect()->route('ad.product-delete-list');

        }
    public function getProductChoose(Request $checked){

            $product = DB::table('product')->whereIn('product.id', $checked);
            $product->delete();


        return redirect()->route('ad.product-list');
//        if(request('checked')){
//            $checked = request('$checked');
//            $product->whereIn('product.id', $checked)->delete();
//        }
    }

}


