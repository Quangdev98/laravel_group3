<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Slide;

class adminSlideController extends Controller
{
    public function getSlideList(){
    	$slide = DB::table('slide')->paginate(15);
        return view('q_admin.slide.slide_list', compact('slide'));
    }
    public function getSlideEdit($id){
        $slide= Slide::find($id);
    	return view('q_admin.slide.slide_edit',compact('slide'));
    }
    public function postSlideEdit(Request $request,$id){
        $this->validate($request,
        [
            'title' => 'required|min:3|max:300|unique:slide,title,'.$id,
            'contentHot' => 'required|min:3|max:500',
            'link' => 'required|min:3|max:400',
            'fileImage' => 'image|mimes:jpeg,png,jpg,gif|max:10240',
        ],
        [
            'title.required' => 'Tiêu đề slide không được để trống.<br>',
            'title.min' => 'Tiêu đề slide không được thấp hơn 3 kí tự.<br>',
            'title.max' => 'Tiêu đề slide không được lớn hơn 300 ký tự.<br>',
            'title.unique' => 'Tiêu đề slide đã tồn tại.<br>',
            'contentHot.required' => 'Nội dung nổi bật slide không được để trống.<br>',
            'contentHot.min' => 'Nội dung nổi bật slide không được thấp hơn 3 kí tự.<br>',
            'contentHot.max' => 'Nội dung nổi bật slide không được lớn hơn 500 ký tự.<br>',
            'link.required' => 'Đường dẫn trỏ tới của slide không được để trống.<br>',
            'link.min' => 'Đường dẫn trỏ tới của slide không được thấp hơn 3 kí tự.<br>',
            'link.max' => 'Đường dẫn trỏ tới của slide không được lớn hơn 400 ký tự.<br>',
            'fileImage.image' => 'File tải lên không hợp lệ.<br>',
            'fileImage.mimes' => 'File tải lên không hợp lệ.<br>',
            'fileImage.max' => 'File tải lên không được quá 10MB.<br>',            
        ]);
        $slide = Slide::find($id);
        $name_img_old = $slide->image;
        $slide->title=$request->title;
        $slide->contentHot=$request->contentHot;
        $slide->link=$request->link;
        if ($request->file('fileImage')) {
            $fileImage = 'IMAGE-SLIDE-'.time().$request->file('fileImage')->getClientOriginalName();
            $slide->image=$fileImage;
            $request->file('fileImage')->move('uploads/slides/',$fileImage);
            if(file_exists('uploads/slides/'.$name_img_old)){
                unlink('uploads/slides/'.$name_img_old);
            }
        }
        $slide->save();
        $slide->update(['slug'=>create_slug($slide->title,$slide->id)]);
        return redirect()->route('ad.slide-list');
    }
    public function getSlideDelete($id){
        $slide = Slide::find($id);
        $slide->delete();
        return redirect()->route('ad.slide-list');
    }
    public function getSlideAdd(){
    	return view('q_admin.slide.slide_add');
    }
    public function postSlideAdd(Request $request){
        $this->validate($request,
        [
            'title' => 'required|min:3|max:300|unique:slide,title',
            'contentHot' => 'required|min:3|max:500',
            'link' => 'required|min:3|max:400',
            'fileImage' => 'required|image|mimes:jpeg,png,jpg,gif|max:10240',
        ],
        [
            'title.required' => 'Tiêu đề slide không được để trống.<br>',
            'title.min' => 'Tiêu đề slide không được thấp hơn 3 kí tự.<br>',
            'title.max' => 'Tiêu đề slide không được lớn hơn 300 ký tự.<br>',
            'title.unique' => 'Tiêu đề slide đã tồn tại.<br>',
            'contentHot.required' => 'Nội dung nổi bật slide không được để trống.<br>',
            'contentHot.min' => 'Nội dung nổi bật slide không được thấp hơn 3 kí tự.<br>',
            'contentHot.max' => 'Nội dung nổi bật slide không được lớn hơn 500 ký tự.<br>',
            'link.required' => 'Đường dẫn trỏ tới của slide không được để trống.<br>',
            'link.min' => 'Đường dẫn trỏ tới của slide không được thấp hơn 3 kí tự.<br>',
            'link.max' => 'Đường dẫn trỏ tới của slide không được lớn hơn 400 ký tự.<br>',
            'fileImage.required' => 'File tải lên không được để trống.<br>',
            'fileImage.image' => 'File tải lên không hợp lệ.<br>',
            'fileImage.mimes' => 'File tải lên không hợp lệ.<br>',
            'fileImage.max' => 'File tải lên không được quá 10MB.<br>',            
        ]);
        $fileImage = 'IMAGE-SLIDE-'.time().$request->file('fileImage')->getClientOriginalName();
        $slide = new Slide;
        $slide->image = $fileImage;
        $slide->title = $request->title;
        $slide->contentHot = $request->contentHot;
        $slide->link = $request->link;
        $request->file('fileImage')->move('uploads/slides/',$fileImage);
        $slide->save();
        $slide->update(['slug'=>create_slug($slide->title,$slide->id)]);
        return redirect()->route('ad.slide-list');
    }
}
