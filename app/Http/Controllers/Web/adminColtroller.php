<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Users;

class adminColtroller extends Controller
{
    public function getLogin(){
        if (Auth::check()){
            return redirect()->route('ad.admin');
        }
    	return view('q_admin.login');
    }
    public function getLogout(){
        Auth::logout();
        return redirect()->route('web.login');
    }

    public function postLogin(Request $request){
        $this->validate($request,
            [
                'email'=>'required',
                'password'=>'required|min:6|max:16'
            ], [
                'email.required'=>'Bạn chưa nhập Email',
                'password.required'=>'Bạn chưa nhập password',
                'password.min'=>'Mật khẩu không nhỏ quá 6 ký tự',
                'password.max'=>'Mật khẩu không lớn hơn 16 ký tự',
            ]);
        $arr = [
            'email'=>$request->email,
            'password'=>$request->password
        ];
        if (Auth::attempt($arr)) {
            // đăng nhập đúng
            return redirect()->route('ad.admin');
        }
        else {
            //đăng nhập sai
            return redirect()->route('web.login')->with('thongbao', 'Tài khoản hoặc mật khẩu không chính xác !');
        }

    }
}
