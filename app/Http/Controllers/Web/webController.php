<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use App\Models\Slide;
use App\Models\Post;
use App\Models\Product;
use App\Models\Contact;
use Cart;
use Dotenv\Result\Success;
use Illuminate\Support\Facades\Redirect;
use Mail;
use PDF;


class webController extends Controller
{
    public function getHome(){
        $slideHome = Slide::all();
        $productsOur = DB::table('product')->where('status','=','3')->orderBy('id','ASC')->take(6)->get();
        $productsNew = DB::table('product')->where('status','=','3')->orderBy('id','DESC')->take(6)->get();
        $phoneCategory = DB::table('info')->where('id',1)->paginate(1);
        $phoneCategory2 = DB::table('info')->where('id',2)->paginate(1);
        $author = DB::table('users')->get();
        $cate = DB::table('theloai')->get();
        $postFooter = DB::table('post')->take(6)->orderBy('id','DESC')->get();
        $opinion = DB::table('opinion')->get();
        return view('q_web.index', compact('slideHome','productsOur','productsNew','author','cate','postFooter','phoneCategory','phoneCategory2','opinion'));
    }
    public function getAbout(){
        $authorList = DB::table('users')->where('level','>','1')->where('level','<','5')->take(4)->get();
        $opinion = DB::table('opinion')->get();
        return view('q_web.about',compact('authorList','opinion'));
    }
    public function getProduct(Request $request){
        $countPro = DB::table('product')->where('product.status',3)->count();
        $countCate = DB::table('theloai')
            ->leftJoin('product','theloai.id','=','product.category_id')
            ->select('theloai.*', DB::raw('count(product.category_id) as count_product'))
            ->where('product.status',3)
            ->groupBy('theloai.id','theloai.name','theloai.slug','theloai.created_at','theloai.updated_at')
            ->get();
        $product = DB::table('product')
            ->join('theloai','product.category_id','=','theloai.id');
        if (request('slug')) {
            $product->where(DB::raw('theloai.slug'),request('slug'));
        }
        if (request('search')) {
            $search = request('search');
            $product->where('product.name','LIKE',"%{$search}%");
        }
        $product = $product
            ->select('product.*','theloai.name as nameCate')
            ->where('product.status',3)
            ->orderBy('product.id', 'DESC');
        if(isset($request->this_ajax) && isset($request->price_min) && isset($request->price_max)){
            $product = $product->where('product.price','>=',$request->price_min)
                ->where('product.price','<=',$request->price_max);
        }
        $product = $product->paginate(9);
        if (isset($request->this_ajax) && isset($request->price_min) && isset($request->price_max)){
            $returnHTML = view('q_web.products_foreach')->with('product', $product)->render();
            return response()->json(array('success' => true, 'html'=>$returnHTML));
        }else{
            return view('q_web.products', compact('countPro','countCate','product'));
        }
    }
    public function getGallery(){
        return view('q_web.gallery');
    }
    public function getBlog(Request $request){
        $postSlide = DB::table('post')->take(3)->get();
        $opinionSlideBar = DB::table('opinion')->get();
        $post = DB::table('post')
        ->join('users','post.user_id','=','users.id')
        ->join('type_post','post.typePost_id','=','type_post.id');
        if (request('id')) {
            $post->where(DB::raw('type_post.id'), request('id'));
        }
        if (request('search')) {
            $search = request('search');
            $post->where('post.title','LIKE', "%{$search}%");
        }
        $tag = DB::table('post')->select('post.tag as tag')->take(5)->orderBy('post.id','DESC')->get();
        $keyTag = [];
        foreach ($tag as $key=>$t){
            $arr_tag = explode(', ',$t->tag);
            $keyTag = array_merge($keyTag,$arr_tag);
        }
        if (request('tag')){
            $searchTag = request('tag');
            $post->where('post.tag','LIKE', "%{$searchTag}%");
        }
        $post = $post
        ->select('post.*', 'users.name as name_author', 'users.image as avatar')
        ->paginate(3);
        $countPost = DB::table('type_post')
            ->leftJoin('post','type_post.id','=','post.typePost_id')
            ->select('type_post.*', DB::raw('count(post.typePost_id) as countPostNumber'))
            ->groupBy('type_post.id','type_post.name','type_post.created_at','type_post.updated_at')
            ->get();
//        dd($keyTag);
        return view('q_web.blog', compact('postSlide','opinionSlideBar','post','keyTag','countPost'));
    }
    public function getBlog_single($slug){
        $opinionSlideBar = DB::table('opinion')->get();
        $blog = DB::table('post')
            ->join('users','post.user_id','=','users.id')
            ->select('post.*','users.name as name_author', 'users.image as avatar')
            ->where('post.slug',$slug)->first();
        $postSlide = DB::table('post')->take(3)->get();
        $countPost = DB::table('type_post')
            ->leftJoin('post','type_post.id','=','post.typePost_id')
            ->select('type_post.*', DB::raw('count(post.typePost_id) as countPostNumber'))
            ->groupBy('type_post.id','type_post.name','type_post.created_at','type_post.updated_at')
            ->get();
//        $tag = DB::table('post')
//            ->join('type_post','post.typePost_id','=','type_post.id')
//            ->select('post.tag as tag','type_post.id')
//            ->where('post.slug',$slug)
//            ->first();
        $tags = DB::table('post')->select('post.tag as tag')->take(4)->orderBy('post.id','DESC')->get();
        $keyTag = [];
        foreach ($tags as $key=>$t){
            $arr_tag = explode(', ',$t->tag);
            $keyTag = array_merge($keyTag,$arr_tag);
        }
//        dd($keyTag);
//        dd($tags);
        return view('q_web.blog_single', compact('opinionSlideBar','blog','postSlide','countPost','keyTag'));
    }
    public function getProfile(){
        return view('q_web.profile');
    }
    public function getProduct_single($slug){
        $productDetail = DB::table('product')
        ->join('theloai','product.category_id','=','theloai.id')
        ->select('product.*','theloai.name as nameCate', DB::raw('product.price - (product.price * product.sale)/100 as priceSale'))
        ->where('product.slug',$slug)->first();
        $imagepProductDetail = DB::table('product')
        ->join('images_product','product.id','=','images_product.product_id')
        ->select('product.*','images_product.link as linksImage')->where('product.slug',$slug)->get();
        // dd($productDetail);
        return view('q_web.product_single', compact('productDetail','imagepProductDetail'));
    }
    public function getCheckout(){
        $data['cart'] = Cart::content();
        $data['total'] = Cart::total(0,'','');
        return view('q_web.checkout',$data);
    }
    public function postCheckout(Request $request){
        $this->validate($request,
        [
            'name'=>'required',
            'address'=>'required|min:3|',
            'phone'=>'required|min:10|numeric',
            'email'=>'required|min:3',
        ],
        [
            'name.required'=>'Tên không được để trống',
            'address.required'=>'Địa chỉ không được để trống',
            'address.min'=>'Địa chỉ phải lớn hơn 3 kí tự',
            'phone.required'=>'Số điện thoại không được để trống',
            'phone.min'=>'Số điện thoại lớn hơn 9 số',
            'phone.numeric'=>'Số điện thoại phải là số',
            'email.required'=>'email không được để trống',
            'email.min'=>'email phải lớn hơn 3 kí tự'
        ]);

        $customer = new Customer();
        $customer->name = $request->name;
        $customer->address = $request->address;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->total = Cart::total(0,'','');
        $customer->status = 0;
        $customer->save();

            foreach(Cart::content() as $row){
                $order = new order;
                $order->name = $row->name;
                $order->price = $row->price;
                $order->quantity = $row->qty;
                $order->image = $row->options->img;
                $order->customer_id = $customer->id;
                $order->save();
           }
           foreach(cart::content() as $row =>$item)
           {
               $prdname = $item->name;
               $price = $item->price;
               $qty = $item->qty;
           }
           $data = array(
               'id' =>$customer->id,
               'name' => $request->name,
               'email'=> $request->email,
               'phone' => $request->phone,
               'address' => $request->address,
               'prdname' => $prdname,
               'price'  => $price,
               'soluong' =>$qty,
               'tongtien' =>cart::total(),
           );
//           dd(cart::content());
        Mail::send('q_web.mail', $data, function ($message) use($request) {
                $message->from('tuaphan7396@gmail.com', 'admin');
                $message->to($request->email, $request->name);
                $message->subject('Xác nhận đơn hàng');
        });
        Cart::Destroy();
        return Redirect('success');
    }
    public function getsuccess(){
        return view('q_web.success');
    }
    public function getContact(){
        $infoList = DB::table('info')->get();
        return view('q_web.contact', compact('infoList'));
    }

    public function postContact(Request $request){
        $this->validate($request,
        [
            'name'=>'required',
            'email'=>'required|min:3',
        ],
        [
            'name.required'=>'Tên không được để trống',
            'email.required'=>'email không được để trống',
            'email.min'=>'email phải lớn hơn 3 kí tự'
        ]);
        $contact= new contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->message = $request->message;
        $contact->save();

        return back()->with('thongbao',
            '$(".alert-thongbao-success").on("click",function(e){
            swal("Gửi thành công!", "Ok để thoát!", "success");
        });');
    }

    public function getService(){
        return view('q_web.service');
    }
    public function getAddcart(Request $r){
//         dd($r->all());
        $Product = Product::find($r->id_product);
        if($r->has('quantity'))
        {
            Cart::add(['id' => $Product->id,
            'name' => $Product->name,
            'price' => ($Product->price)-(($Product->price/100)*($Product->sale)),
            'qty' => $r->quantity,
            'weight' => 0,
            'options' => ['img' => $Product->image],
            ]);
        }else{
            Cart::add(['id' => $Product->id,
                'name' => $Product->name,
                'price' => ($Product->price)-(($Product->price/100)*($Product->sale)),
                'qty' => 1,
                'weight' => 0,
                'options' => ['img' => $Product->image],
            ]);
        }
//        dd(cart::content());
        return redirect('cart_single');

    }

    public function getCart_single(){
//         dd(cart::content());
        $data['cart'] = cart::content();
        $data['total'] = cart::total(0,'','');
        return view('q_web.cart_single',$data);
        // dd($data);
    }
    public function removeCart($id){
        Cart::remove($id);
        return Redirect('cart_single');

        //123
    }
    public function UpdateCart($rowId,$qty){
        Cart::update($rowId, $qty);
        return "success";
        //123
    }
    public function getsearch(Request $request){
        $product =DB::table('product')->where('name','like','%'.$request->key.'%')->get();
        return view('q_web.layout.search',compact('product'));

    }

}
