<?php

namespace App\Http\Requests\Request;

use Illuminate\Foundation\Http\FormRequest;

class AddCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'address'=>'required|min:3|',
            'phone'=>'required|min:10|numeric',
            'email'=>'required|min:3|unique:customer,id',
        ];
    }
    public function messages()
        {
            return [
                'name.required'=>'Tên không được để trống',
                'address.required'=>'Địa chỉ không được để trống',
                'address.min'=>'Địa chỉ phải lớn hơn 3 kí tự',
                'phone.required'=>'Số điện thoại không được để trống',
                'phone.min'=>'Số điện thoại lớn hơn 9 số',
                'phone.numeric'=>'Số điện thoại phải là số',
                'email.required'=>'email không được để trống',
                'email.min'=>'email phải lớn hơn 3 kí tự',
                'email.unique'=>'email đã tồn tại',
            ];
        }
}
