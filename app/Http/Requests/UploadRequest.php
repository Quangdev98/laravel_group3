<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return [
            'name' => 'required',
            'imageDetail' => 'image|mimes:jpeg,bmp,png|size:10240'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required'
        ];
        $photos = count($this->input('imageDetail'));
        foreach(range(0, $photos) as $index) {
            $rules['imageDetail.' . $index] = 'image|mimes:jpeg,bmp,png|max:100000';
        }

        return $rules;
    }
}
