<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckPoster
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->level ==3 || Auth::user()->level ==1 )
        {
            return $next($request);
        }
        else
            return Redirect()->route('ad.admin')->with('thongbao','Bạn không có quyền!!!');

    }
}
