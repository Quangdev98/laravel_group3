<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class adminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // nếu user đã đăng nhập
        // dd(Auth::check());
        if (Auth::check()){
            $user = Auth::user();
            // nếu level =1 (admin), status = 1 (actived) thì cho qua.
            if ($user->level == 1 || $user->level == 2 || $user->level == 3 ){
                // đổ tên ng đăng nhập ra trang chủ admin
                view()->share('user_admin',Auth::user());
                // 
                return $next($request);
            }else{
                Auth::logout();
                return redirect()->route('web.login');
            }
            // if($user->level == 2){
            //     view()->share('user_admin',Auth::user());
            //     return $next($request);
            // }
            //  if($user->level == 3){
            //     view()->share('user_admin',Auth::user());
            //     return $next($request);
            // }
            
        }else{
            return redirect()->route('web.login');
        }
        

    }
}
