<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Theloai;
use Cart;
use Gloudemans\Shoppingcart\Facades\Cart as FacadesCart;
use App\Http\Controllers\Web\webController;
use Gloudemans\Shoppingcart\Cart as ShoppingcartCart;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $countProduct = Product::all()->where('status','<','4')->count();
        View::share('countProduct', $countProduct);
        $countProductDelete = Product::all()->where('status',4)->count();
        View::share('countProductDelete', $countProductDelete);
        $infoHeader = DB::table('info')->where('status',1)->first();
        View::share('infoHeader', $infoHeader);
        $productItem = DB::table('product')->select('product.image','product.slug','product.name')->orderBy('id','DESC')->get();
        View::share('productItem', $productItem);
        // dd($product);
        // $countTheloai = Theloai::all()->count();
        // View::share('countTheloai', $countTheloai);
    }
}
