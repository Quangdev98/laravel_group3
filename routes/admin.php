<?php
// use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;


// ADMIN
// ADMIN
Route::get('/','BangDieuKhienController@getAdmin')->name('admin');

Route::group(['prefix'=>'category'], function(){
	Route::get('category-list', 'adminCategoryController@getCateList')->name('category-list');
	Route::get('category-add', 'adminCategoryController@getCateAdd')->name('category-add')->middleware('checkProducter');
	Route::post('category-add', 'adminCategoryController@postCateAdd')->name('post-category-add')->middleware('checkProducter');
	Route::get('category-edit/{id}', 'adminCategoryController@getCateEdit')->name('category-edit')->middleware('checkProducter');
	Route::post('category-edit/{id}', 'adminCategoryController@postCateEdit')->name('post-category-edit')->middleware('checkProducter');
	Route::get('category-delete/{id}', 'adminCategoryController@getCateDelete')->name('category-delete')->middleware('checkProducter');

});
Route::group(['prefix' => 'product'], function(){
	Route::get('product-list', 'adminProductController@getProductList')->name('product-list');
	Route::get('product-add', 'adminProductController@getProductAdd')->name('product-add')->middleware('checkProducter');
	Route::post('product-add', 'adminProductController@postProductAdd')->name('post-product-add')->middleware('checkProducter');
	Route::get('product-edit/{id}', 'adminProductController@getProductEdit')->name('product-edit')->middleware('checkProducter');
	Route::post('product-edit/{id}', 'adminProductController@postProductEdit')->name('post-product-edit')->middleware('checkProducter');
	Route::get('product-delete/{id}', 'adminProductController@getProductDelete')->name('product-delete')->middleware('checkProducter');
	Route::get('product-delete-list', 'adminProductController@getProductDeleteList')->name('product-delete-list')->middleware('checkProducter');
	Route::get('product-delete-edit/{id}', 'adminProductController@getProductDeleteEdit')->name('product-delete-edit')->middleware('checkProducter');
	Route::post('product-delete-edit/{id}', 'adminProductController@postProductDeleteEdit')->name('postProduct-delete-edit')->middleware('checkProducter');
	Route::get('product-delete-status/{id}', 'adminProductController@getProductDeleteStatus')->name('product-delete-status')->middleware('checkProducter');
	Route::get('/product-choose', 'adminProductController@getProductChoose')->name('product-choose')->middleware('checkProducter');

});
Route::group(['prefix' => 'author'], function(){
	Route::get('author-list', 'admin_Author_Controller@getAuthorList')->name('author-list');
	Route::get('author-add', 'admin_Author_Controller@getAuthorAdd')->name('author-add')->middleware('checkAdmin');
	Route::post('author-add', 'admin_Author_Controller@postAuthorAdd')->name('post-author-add')->middleware('checkAdmin');
	Route::get('author-edit/{id}', 'admin_Author_Controller@getAuthorEdit')->name('author-edit')->middleware('checkAdmin');
	Route::post('author-edit/{id}', 'admin_Author_Controller@postAuthorEdit')->name('post-author-edit')->middleware('checkAdmin');
	Route::get('author-delete/{id}', 'admin_Author_Controller@getAuthorDelete')->name('author-delete')->middleware('checkAdmin');
});
Route::group(['prefix' => 'type-post'], function(){
	Route::get('typePost-list', 'typePostController@getTypePostList')->name('typePost-list');
	Route::get('typePost-add', 'typePostController@getTypePostAdd')->name('typePost-add')->middleware('CheckPoster');
	Route::post('typePost-add', 'typePostController@postTypePostAdd')->name('post-typePost-add')->middleware('CheckPoster');
	Route::get('typePost-edit/{id}', 'typePostController@geTypetPostEdit')->name('typePost-edit')->middleware('CheckPoster');
	Route::post('typePost-edit/{id}', 'typePostController@postTypetPostEdit')->name('post-typePost-edit')->middleware('CheckPoster');
	Route::get('typePost-delete/{id}', 'typePostController@geTypetPostDelete')->name('typePost-delete')->middleware('CheckPoster');
});
Route::group(['prefix' => 'blog'], function(){
	Route::get('post-list', 'adminPostController@getPostList')->name('post-list');
	Route::get('post-add', 'adminPostController@getPostAdd')->name('post-add')->middleware('CheckPoster');
	Route::post('post-add', 'adminPostController@postPostAdd')->name('post-post-add')->middleware('CheckPoster');
	Route::get('post-edit/{id}', 'adminPostController@getPostEdit')->name('post-edit')->middleware('CheckPoster');
	Route::post('post-edit/{id}', 'adminPostController@postPostEdit')->name('post-post-edit')->middleware('CheckPoster');
	Route::get('post-delete/{id}', 'adminPostController@getPostDelete')->name('post-delete')->middleware('CheckPoster');
});
Route::group(['prefix' => 'slide'], function(){
	Route::get('slide-list', 'adminSlideController@getSlideList')->name('slide-list');
	Route::get('slide-add', 'adminSlideController@getSlideAdd')->name('slide-add')->middleware('checkAdmin');
	Route::post('slide-add', 'adminSlideController@postSlideAdd')->name('post-slide-add')->middleware('checkAdmin');
	Route::get('slide-edit/{id}', 'adminSlideController@getSlideEdit')->name('slide-edit')->middleware('checkAdmin');
	Route::post('slide-edit/{id}', 'adminSlideController@postSlideEdit')->name('post-slide-edit')->middleware('checkAdmin');
	Route::get('slide-delete/{id}', 'adminSlideController@getSlideDelete')->name('slide-delete')->middleware('checkAdmin');
});
Route::group(['prefix' => 'info'], function(){
	Route::get('info-list', 'adminInfoController@index')->name('info-list');
	Route::get('info-add', 'adminInfoController@getInfoAdd')->name('info-add')->middleware('checkAdmin');
	Route::post('info-add', 'adminInfoController@postInfoAdd')->name('post-info-add')->middleware('checkAdmin');
	Route::get('info-edit/{id}', 'adminInfoController@getInfoEdit')->name('info-edit')->middleware('checkAdmin');
	Route::post('info-edit/{id}', 'adminInfoController@postInfoEdit')->name('post-info-edit')->middleware('checkAdmin');
	Route::get('info-delete/{id}', 'adminInfoController@getInfoDelete')->name('info-delete')->middleware('checkAdmin');
});

Route::group(['prefix' => 'order'], function () {
    Route::get('order', 'orderController@getOrder')->name('order')->middleware('checkAdmin');
    Route::get('order-process', 'orderController@getOrderprocess')->name('order-process')->middleware('checkAdmin');
    Route::get('order-active/{id}', 'orderController@activeOrder')->name('order-active')->middleware('checkAdmin');
    Route::get('chi-tiet-don-hang/{id}', 'orderController@getOrderView')->name('order-view')->middleware('checkAdmin');
});

Route::group(['prefix'=> 'phanhoi'], function() {
    Route::get('phan-hoi', 'adminPhanhoiController@getPhanhoi')->name('phanhoi')->middleware('checkAdmin');
    Route::get('phan-hoi-delete/{id}', 'adminPhanhoiController@getPhanhoiDelete')->name('phanhoi-delete')->middleware('checkAdmin');
});
Route::group(['prefix'=> 'ykien','middleware'=>'checkAdmin'], function() {
    Route::get('y-kien', 'adminYKienController@getYKien')->name('ykien');
    Route::get('y-kien-them', 'adminYKienController@getOpinion_add')->name('opinion-add')->middleware('checkAdmin');
    Route::post('y-kien-them', 'adminYKienController@postOpinion_add')->name('post-opinion-add')->middleware('checkAdmin');
    Route::get('y-kien-sua/{id}', 'adminYKienController@getOpinion_edit')->name('opinion-edit')->middleware('checkAdmin');
    Route::post('y-kien-sua/{id}', 'adminYKienController@postOpinion_edit')->name('post-opinion-edit')->middleware('checkAdmin');
    Route::get('y-kien-xoa/{id}', 'adminYKienController@getOpinionDelete')->name('opinion-delete')->middleware('checkAdmin');
});
 ?>
