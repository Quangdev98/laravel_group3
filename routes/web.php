<?php

use App\Http\Controllers\cart\cartController;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;
use Gloudemans\Shoppingcart\Cart;

Route::get('logout', 'adminColtroller@getLogout')->name('logout');
Route::get('dang-nhap', 'adminColtroller@getLogin')->name('login');
Route::post('login', 'adminColtroller@postLogin')->name('postLogin');
Route::get('/', function () {
    return view('q_web.index');
});
Route::get('/', "webController@getHome")->name('index');
Route::get('ve-chung-toi', 'webController@getAbout')->name('about');
Route::get('san-pham', 'webController@getProduct')->name('products');
Route::get('san-pham/{slug}.html', 'webController@getProduct_single')->name('product_single');
Route::get('bo-suu-tap', 'webController@getGallery')->name('gallery');
Route::get('bai-viet', 'webController@getBlog')->name('blog');
Route::get('bai-viet/{slug}.html', 'webController@getBlog_single')->name('blog-single');
Route::get('profile', 'webController@getProfile')->name('profile');

Route::get('checkout', 'webController@getCheckout')->name('checkout');
Route::post('checkout/post', 'webController@postCheckout')->name('postCheckout');

Route::get('success', 'webController@getsuccess')->name('success');

Route::get('cart_single', 'webController@getCart_single')->name('cart_single');
Route::get('/cart_single/add', 'webController@getAddcart')->name('cart_add');
Route::get('/cart_single/remove/{id}', 'webController@removeCart')->name('cart_add');
Route::get('/cart_single/update/{rowId}/{qty}', 'webController@UpdateCart')->name('cart_update');

Route::get('lien-he', 'webController@getContact')->name('contact');
Route::post('contact/post', 'webController@postContact')->name('contact_post');

Route::get('dịch-vu', 'webController@getService')->name('service');

Route::get('mail', 'MailController@sendMail');
//Route::get('searchs', 'MailController@searchForm');




